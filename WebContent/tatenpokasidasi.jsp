<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="java.net.URLDecoder"%>
<%@ page import="java.util.*"%>
<%@ page import="bean.*"%>
<%@ page import="dmx.*"%>
<%
	Cookie[] cookies = request.getCookies();
	String stid = "";
	String stname = "";
	if (cookies != null) {
		for (Cookie cook : cookies) {
			if (cook.getName().equals("StId")) {
				stid = cook.getValue();
			}
			if (cook.getName().equals("StName")) {
				stname = URLDecoder.decode(cook.getValue(), "UTF-8");
			}
		}
	}
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>他店舗貸出可能一覧</title>
<link rel="stylesheet" type="text/css" href="tenpure.css">
</head>
<body>
	<jsp:useBean id="MediaListBean" class="bean.MediaListBean"
		scope="session" />
	<Div id="header">
		<h1>他店舗貸出可能一覧</h1>
	</Div>
	<table border="1" align="center">
		<tr>
			<th>店舗名</th>
			<th>カテゴリ(DVD/BD)</th>
			<th>本数</th>
		</tr>
		<%
			ArrayList<MediaTenpoRecordBean> MediaArray = MediaListBean.getMediaList();
			for (MediaTenpoRecordBean record : MediaArray) {
				String category = "DVD";//1がDVD 2BD
				int zanSu = 0;
				String pkubun = record.getMgory();
				String pid = record.getPid();
				String shName = record.getShName();
				String status = record.getStatus();
				int karisakitenID = record.getShId();
				int zitenID = record.getZitenpoID();
				int sCount = 0;
		%>
		<tr>
			<td><%=record.getShName()%></td>
			<td><%=record.getMgory()%></td>
			<td><%=record.getSu()%></td>
			<td><form action="Servlet" method="post">
					<input type="hidden" name="pid" value="<%=pid%>"> <input
						type="hidden" name="pkubun" value="<%=pkubun%>"> <input
						type="hidden" name="status" value="<%=status%>"><input
						type="hidden" name="zitenpoid" value="<%=zitenID%>"> <input
						type="submit" value="要請する">
				</form></td>
		</tr>
		<%
			}
		%>
	</table>

	<INPUT type="button" onclick="location.href='fusokuItiran.jsp'"
		value="不足在庫一覧へ戻る" name="不足在庫一覧へ戻る"
		style="position: absolute; top: 600px; left: 50px; WIDTH: 150px; HEIGHT: 50px;">
	<br>

	<ul>
		<li id="dare">社員ID：<%=stid%><br> 名前:<%=stname%><br>
		</li>
	</ul>

</body>
</html>