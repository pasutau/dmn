<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="java.net.URLDecoder" %>
<%
	Cookie[] cookies = request.getCookies();
	String stid = "";
	String stname = "";
	if(cookies != null  ){
		for(Cookie cook : cookies){
			if(cook.getName().equals("StId")){
				stid = cook.getValue();
			}
			if(cook.getName().equals("StName")){
				stname = URLDecoder.decode(cook.getValue(),"UTF-8");
			}
		}
	}
%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>貸出申請情報入力確認</title>
	<link rel="stylesheet" type="text/css" href="tenpure.css">
</head>
<body>
	<Div id ="header"><h1>貸出申請情報入力確認</h1></Div>

	<div align="center"><time><h3>申請日:2017年7月20日</h3></time><h3>ワイルドスピード NODAMISSION<span style="margin-right: 40px;"></span>DVD:2本<span style="margin-right: 40px;"></span>BD:1本</h3></div>

	<!-- 表 -->
	<div style="height:330px; width:870px; text-align:center;">
	<table border="1px" cellpadding="4px" align="right" style="border-collapse: collapse;">
	<colgroup>
		<!-- 1列目のグループ -->
		<col span="1" style="background-color: #ffffff;" />
		<!-- 2列目のグループ -->
		<col span="1" style="background-color: #ffffff;" />
		<!-- 3列目のグループ -->
		<col span="1" style="background-color: #ffffff;" />
		<!-- 4列目のグループ -->
		<col span="1" style="background-color: #ffffff;" />
	</colgroup>
	<tr>
		<th  align="center" bgcolor="#C0C0C0"><span style="margin-right: 40px;"></span>店舗<span style="margin-right: 40px;"></span></th>
		<th  align="center"  bgcolor="#C0C0C0">DVD申請本数</th>
		<th  align="center"  bgcolor="#C0C0C0">BD申請本数</th>
		<th  align="center"  bgcolor="#C0C0C0">申請日数</th>
	</tr>
		<tr>
		<td align="center">○○店</td>
		<td align="center">1本</td>
		<td align="center">2本</td>
		<td align="center">2週間</td>
	</tr>
	<tr>
		<td align="center">××店</td>
		<td align="center"></td>
		<td align="center"></td>
		<td align="center"></td>
	</tr>
	<tr>
		<td align="center">□□店</td>
		<td align="center"></td>
		<td align="center"></td>
		<td align="center"></td>
	</tr>
	<tr>
		<td align="center">△△店</td>
		<td align="center"></td>
		<td align="center"></td>
		<td align="center"></td>
	</tr>
	<tr>
		<td align="center">●●店</td>
		<td align="center"></td>
		<td align="center"></td>
		<td align="center"></td>
	</tr>
	<tr>
		<td align="center">■■店</td>
		<td align="center"></td>
		<td align="center"></td>
		<td align="center"></td>
	</tr>
	<tr>
		<td align="center">▲▲店</td>
		<td align="center"></td>
		<td align="center"></td>
		<td align="center"></td>
	</tr>
	<tr>
		<td align="center">▼▼店</td>
		<td align="center"></td>
		<td align="center"></td>
		<td align="center"></td>
	</tr>
	<tr>
		<td align="center">▽▽店</td>
		<td align="center"></td>
		<td align="center"></td>
		<td align="center"></td>
	</tr>
</table>
</div>
	<div align = "center"><h3>この内容で申請しますか?</h3></div>
	<input type="button"  onclick="location.href='kasidasisinsei.jsp'" value="戻る" name="戻る" style="position:absolute;top:580px;left:460px; WIDTH:200px; HEIGHT: 50px;" >
	<input type="submit"  onclick="location.href='kasidasisinseikanryo.jsp'" value="貸出要請" name="貸出要請" style="position:absolute;top:580px;right:460px; WIDTH:200px; HEIGHT: 50px;" >

	<ul>
	<li id = "dare">社員ID：<%=stid %><br>
    	名前:<%=stname %><br>
    	</li>
	</ul>
</body>
</html>