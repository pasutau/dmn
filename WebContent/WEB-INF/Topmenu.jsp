<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="java.net.URLDecoder" %>
<%
	Cookie[] cookies = request.getCookies();
	String stid = "";
	String stname = "";
	if(cookies != null){
		for(Cookie cook : cookies){
			if(cook.getName().equals("StId")){
				stid = cook.getValue();
			}
			if(cook.getName().equals("StName")){
				stname = URLDecoder.decode(cook.getValue(),"UTF-8");
			}
		}
	}
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>TOPメニュー画面</title>
<link rel="stylesheet" type="text/css" href="tenpure.css">
</head>
<body>
	<Div id ="header"><h1>業務選択</h1></Div>
	<Div Align="right">
		<a href="#" class="square_btn2">ログアウト</a>
	</Div>

	<!-- メニューボタン -->
	<a href="tujyogyomu.html"><img src="./img/tujyo.PNG" alt="通常業務" style="position:absolute;top:230px;left:250px;"></a>
	<a href="settei.html"><img src="./img/setei.PNG" alt="設定" style="position:absolute;top:230px;right:250px;"></a>

	<ul>
	<li id = "dare">社員ID：<%=stid %><br>
    	名前:<%=stname %><br>
    	</li>
	</ul>
</body>
</html>