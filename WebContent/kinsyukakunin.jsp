<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="java.net.URLDecoder" %>
<%
Cookie[] cookies = request.getCookies();
    String stid = "";
    String stname = "";
    if(cookies != null){
        for(Cookie cook : cookies){
            if(cook.getName().equals("StId")){
                stid = cook.getValue();
            }
            if(cook.getName().equals("StName")){
                stname = URLDecoder.decode(cook.getValue(),"UTF-8");
            }
        }
    }
%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>金種入力確認</title>
    <link rel="stylesheet" type="text/css" href="tenpure.css">
</head>
<body>
    <Div id ="header"><h1>金種入力確認</h1></Div>
    <Div Align="right">
        <a href="GoodBye" class="square_btn2">ログアウト</a>
    </Div>

<form method ="post" action="kinsyukannryo">
    <!-- 表 -->
    <table border="1px" cellpadding="3px" align="center" style="border-collapse: collapse;">
    <colgroup>
        <!-- 1列目のグループ -->
        <col span="1" style="background-color: #ffffff;" />
        <!-- 2列目のグループ -->
        <col span="1" style="background-color: #ffffff;" />
        <!-- 3 ~ 4列目のグループ -->
        <col span="2" style="background-color: #ffffff;" />
    </colgroup>
    <tr bgcolor="#C0C0C0">
        <th  align="center"><span style="margin-right: 100px;"></span>金種<span style="margin-right: 100px;"></span></th>
        <th  align="center"><span style="margin-right: 75px;"></span>枚数<span style="margin-right: 75px;"></span></th>
        <th  align="center"><span style="margin-right: 40px;"></span>合計金額<span style="margin-right: 40px;"></span></th>
    </tr>
    <tr>
        <td align="right">10000</td>
        <td align="right"></td>
        <td align="right"></td>

    </tr>
    <tr>
        <td align="right">5000</td>
        <td align="right"></td>
        <td align="right"></td>
    </tr>
    <tr>
        <td align="right">2000</td>
        <td align="right"></td>
        <td align="right"></td>
    </tr>
    <tr>
        <td align="right">1000</td>
        <td align="right"></td>
        <td align="right"></td>
    </tr><tr>
        <td align="right">500</td>
        <td align="right"></td>
        <td align="right"></td>
    </tr>
    <tr>
        <td align="right">100</td>
        <td align="right"></td>
        <td align="right"></td>
    </tr>
    <tr>
        <td align="right">50</td>
        <td align="right"></td>
        <td align="right"></td>
    </tr>
    <tr>
        <td align="right">10</td>
        <td align="right"></td>
        <td align="right"></td>
    </tr>
    <tr>
        <td align="right">5</td>
        <td align="right"></td>
        <td align="right"></td>
    </tr>
    <tr>
        <td align="right">1</td>
        <td align="right"></td>
        <td align="right"></td>
    </tr>
    <tr>
        <td colspan="2" align="center">当日の売上金</td>
        <td align="right"></td>
    </tr>
    <tr>
        <td colspan="2" align="center">総合計</td>
        <td align="right"></td>
    </tr>
    <tr>
        <td colspan="2" align="center">差異</td>
        <td align="right"></td>
    </tr>
</table>
</form>
<Div  Align="center"><h1><font color="red">この内容で登録しますか</font></h1></Div>

    <input type="button"  onclick="location.href='kinsyukanryou.jsp'" value="登録" name="登録" style="position:absolute;top:540px;left:400px; WIDTH:200px; HEIGHT: 50px;" ><br>

    <INPUT type="button" onclick="location.href='rezisime.jsp'" value="レジ締めへ戻る" name="レジ締めへ戻る" style="position:absolute;top:600px;left:50px; WIDTH:150px; HEIGHT: 50px;" ><br>

        <ul>
    <li id = "dare">社員ID：<%=stid %><br>
        名前:<%=stname %><br>

        </li>
    </ul>
</body>
</html>