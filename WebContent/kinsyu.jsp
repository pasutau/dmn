<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="java.net.URLDecoder" %>
<%
	Cookie[] cookies = request.getCookies();
	String stid = "";
	String stname = "";
    String regiid = "";
	if(cookies != null){
		for(Cookie cook : cookies){
			if(cook.getName().equals("StId")){
				stid = cook.getValue();
			}
			if(cook.getName().equals("StName")){
				stname = URLDecoder.decode(cook.getValue(),"UTF-8");
			}
			if(cook.getName().equals("RegiId")){
                regiid = URLDecoder.decode(cook.getValue(),"UTF-8");
            }
		}
}

%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>金種入力</title>
	<link rel="stylesheet" type="text/css" href="tenpure.css">
</head>
<body>
	<Div id ="header"><h1>金種入力</h1></Div>
	<Div Align="right">
		<a href="GoodBye" class="square_btn2">ログアウト</a>
	</Div>
<form method ="post" action="RegisterKinsyu">
<input type="hidden" name="rid" value="<%= regiid%>">

	<!-- 表 -->
	<table border="1px" cellpadding="3px" align="center" style="border-collapse: collapse;">
	<colgroup>
		<!-- 1列目のグループ -->
		<col span="1" style="background-color: #ffffff;" />
		<!-- 2列目のグループ -->
		<col span="1" style="background-color: #ffffff;" />
		<!-- 3 ~ 4列目のグループ -->
		<col span="2" style="background-color: #ffffff;" />
	</colgroup>
	<Div align="center"><h2>レジ内金額を入力</h2></Div>
	<tr bgcolor="#C0C0C0">
		<th  align="center"><span style="margin-right: 100px;"></span>金種<span style="margin-right: 100px;"></span></th>
		<th  align="center"><span style="margin-right: 75px;"></span>枚数<span style="margin-right: 75px;"></span></th>
	</tr>
	<tr>
		<td align="right">10000</td>
		<td align="right"><input type="text" name="man"style="text-align: right;"></td>
	</tr>
	<tr>
		<td align="right">5000</td>
		<td align="right"><input type="text" name="gosen"style="text-align: right;"></td>
	</tr>
	<tr>
		<td align="right">2000</td>
		<td align="right"><input type="text" name="nisen"style="text-align: right;"></td>
	</tr>
	<tr>
		<td align="right">1000</td>
		<td align="right"><input type="text" name="sen"style="text-align: right;"></td>
		</tr>
		<tr>
		<td align="right">500</td>
		<td align="right"><input type="text" name="gohyaku"style="text-align: right;"></td>
	</tr>
	<tr>
		<td align="right">100</td>
		<td align="right"><input type="text" name="hyaku"style="text-align: right;"></td>
	</tr>
	<tr>
		<td align="right">50</td>
		<td align="right"><input type="text" name="gozyu"style="text-align: right;"></td>
	</tr>
	<tr>
		<td align="right">10</td>
		<td align="right"><input type="text" name="zyu"style="text-align: right;"></td>
	</tr>
	<tr>
		<td align="right">5</td>
		<td align="right"><input type="text" name="go"style="text-align: right;"></td>
	</tr>
	<tr>
		<td align="right">1</td>
		<td align="right"><input type="text" name="iti"style="text-align: right;"></td>
	</tr>

</table>
<input type="submit" value="登録" name="登録" style="position:absolute;top:600px;left:400px; WIDTH:200px; HEIGHT: 50px;" ><br>
</form>

	<INPUT type="button" onclick="location.href='rezisime.jsp'" value="レジ締めへ戻る" name="レジ締めへ戻る" style="position:absolute;top:600px;left:50px; WIDTH:150px; HEIGHT: 50px;" ><br>

		<ul>
	<li id = "dare">社員ID：<%=stid %><br>
    	名前:<%=stname %><br>
    	</li>
	</ul>
</body>
</html>