<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="java.net.URLDecoder" %>
<%@ page import="java.util.*"%>
<%@ page import="bean.*" %>
<%
	Cookie[] cookies = request.getCookies();
	String stid = "";
	String stname = "";
	if(cookies != null){
		for(Cookie cook : cookies){
			if(cook.getName().equals("StId")){
				stid = cook.getValue();
			}
			if(cook.getName().equals("StName")){
				stname = URLDecoder.decode(cook.getValue(),"UTF-8");
			}
		}
	}
%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>メディア貸出履歴一覧</title>
	<link rel="stylesheet" type="text/css" href="tenpure.css">
</head>
<body>
<jsp:useBean id="kasilist" class="bean.KasiRirekiListBean" scope="session"/>
	<Div id ="header"><h1>メディア貸出履歴一覧</h1></Div>
<table border ="1">
<tr><th>会員ＩＤ</th><th>氏名</th><th>日付</th></tr>
    <%
    ArrayList<KasiRirekiBean> kasiArray =  kasilist.getKasiRireki();
    for(KasiRirekiBean record : kasiArray){
    %>
    <tr>
        <td><%= record.getuId() %></td>
        <td><%= record.getuName() %></td>
        <td><%= record.getTime() %></td>
    </tr>
    <% } %>
</table>
	<INPUT type="button" onclick="location.href='kasidasikanri.jsp'" value="貸出へ戻る" name="貸出へ戻る" style="position:absolute;top:600px;left:50px; WIDTH:150px; HEIGHT: 50px;" ><br>

	<ul>
	<li id = "dare">社員ID：<%=stid %><br>
    	名前:<%=stname %><br>
    	</li>
	</ul>
</body>
</html>