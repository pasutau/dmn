<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="java.net.URLDecoder" %>
<%
	Cookie[] cookies = request.getCookies();
	String stid = "";
	String stname = "";
	if(cookies != null){
		for(Cookie cook : cookies){
			if(cook.getName().equals("StId")){
				stid = cook.getValue();
			}
			if(cook.getName().equals("StName")){
				stname = URLDecoder.decode(cook.getValue(),"UTF-8");
			}
		}
	}
%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>社員情報登録</title>
	<link rel="stylesheet" type="text/css" href="tenpure.css">
</head>
<body>

	<Div id ="header"><h1>社員情報登録</h1></Div><br>

	<form action = "SyainAddServlet" method = "post">
	<div Align="center"><h1>社員名:<input type="text" name="sName"></h1></div><br>
	<div Align="center"><h1>社員名(かな):<input type="text" name="sNameKana" ></h1></div><br>
	<div Align="center"><h1>電話番号:<input type="text" name="sTel" ></h1></div><br>
	<div Align="center"><h1>住所:<input type="text" name="sAddress" ></h1></div><br>
	<div Align="center"><h1>職位:<select name="sPossion">
									   <option value="店長"selected>店長</option>
									   <option value="社員" >社員</option>
									   <option value="アルバイト">アルバイト</option>
									   <option value="奴隷">奴隷</option>
									   </select></h1></div><br>
	<div Align="center"><h1>店舗:<select name="sTenpo">
									   <option value="1"selected>○○店</option>
									   <option value="2" >△△店</option>
									   <option value="3">□□店</option>
									   <option value="4">××店</option>
									   <option value="5">●●店</option>
									   <option value="6">◎◎店</option>
									   <option value="7">▲▲店</option>
									   <option value="8">■■店</option>
									   <option value="9">▼▼店</option>
									   <option value="10">▽▽店</option>
									   </select>
	</h1></div>

	<input type="submit" value="登録する" style="position:absolute;top:560px;left:580px; WIDTH:200px; HEIGHT: 50px;" ><br>
</form>

	<INPUT type="button" onclick="location.href='syainkanri.jsp'" value="社員管理へ戻る" name="社員管理へ戻る" style="position:absolute;top:600px;left:50px; WIDTH:150px; HEIGHT: 50px;" ><br>

	<ul>
	<li id = "dare">社員ID：<%=stid %><br>
    	名前:<%=stname %><br>
    	</li>
	</ul>
</body>
</html>