<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="java.net.URLDecoder" %>
<%@ page import="java.util.*" %>
<%@ page import = "dmx.*" %>
<%@ page import = "db.*" %>
<%@ page import="bean.*" %>
<%
	Cookie[] cookies = request.getCookies();
	String stid = "";
	String stname = "";
	if(cookies != null){
		for(Cookie cook : cookies){
			if(cook.getName().equals("StId")){
				stid = cook.getValue();
			}
			if(cook.getName().equals("StName")){
				stname = URLDecoder.decode(cook.getValue(),"UTF-8");
			}
		}
	}
%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>社員情報権限登録</title>
	<link rel="stylesheet" type="text/css" href="tenpure.css">
</head>
<body>
<jsp:useBean id="SyainInfoBean" class="bean.SyainInfoBean" scope="session" />
	<Div id ="header"><h1>社員情報権限登録</h1></Div>

<form action = "SyainkengenServlet" method = "post">
<%
	ArrayList<SyainBean> semInfoArray = SyainInfoBean.getSyainArray();
	for(SyainBean rc : semInfoArray) {
 %>
	<input type="hidden" name="sId" value=<%= rc.getsId() %>>
	<div Align="center"><h1>社員ID:<%= rc.getsId() %> <%= rc.getsName() %></h1></div>
	<div class="float">
		<input type="checkbox" name="kanri" value="1" checked="checked">貸出管理<br>
		<input type="checkbox" name="kanri" value="2">会員管理<br>
		<input type="checkbox" name="kanri" value="3">在庫管理<br>
		<input type="checkbox" name="kanri" value="4">社員管理<br>
		<input type="checkbox" name="kanri" value="5">レジ締め<br>
		<input type="checkbox" name="kanri" value="6">メディア管理<br>
		<input type="checkbox" name="kanri" value="7">ブラック管理<br>
	</div>

	<input type="submit" value="登録" name="登録" style="position:absolute;top:560px;left:570px; WIDTH:200px; HEIGHT: 50px;" ><br>

<%
	}
%>
</form>


	<INPUT type="button" onclick="location.href='syainkengen.jsp'" value="社員検索へ戻る" name="社員検索へ戻る" style="position:absolute;top:600px;left:50px; WIDTH:150px; HEIGHT: 50px;" ><br>

	<ul>
	<li id = "dare">社員ID：<%=stid %><br>
    	名前:<%=stname %><br>
    	</li>
	</ul>
</body>
</html>