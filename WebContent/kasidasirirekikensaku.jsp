<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="java.net.URLDecoder" %>
<%
	Cookie[] cookies = request.getCookies();
	String stid = "";
	String stname = "";
	if(cookies != null){
		for(Cookie cook : cookies){
			if(cook.getName().equals("StId")){
				stid = cook.getValue();
			}
			if(cook.getName().equals("StName")){
				stname = URLDecoder.decode(cook.getValue(),"UTF-8");
			}
		}
	}
%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>貸出履歴検索</title>
	<link rel="stylesheet" type="text/css" href="tenpure.css">
</head>
<body>
<!-- ユーザーが何借りたか表示する機能 -->
	 <Div id ="header"><h1>貸出履歴検索</h1></Div><br>

	 <form action ="UserHistory" method = get>
        <div Align="center"><input type="text" name="uid"  placeholder="会員ID" style="position: absolute; left:600px; top:250px;"></div><br>
        <input type="submit" value="検索" name="検索" style="position:absolute;top:450px;left:580px; WIDTH:200px; HEIGHT: 50px;" ><br>
    </form>

	<INPUT type="button" onclick="location.href='kasidasikanri.jsp'" value="貸出へ戻る" name="貸出へ戻る" style="position:absolute;top:600px;left:50px; WIDTH:150px; HEIGHT: 50px;" ><br>

	<li id = "dare" style="position:absolute;top:570px;left:1200px; WIDTH:150px; HEIGHT: 50px;">
              社員ID:<%=stid %><br>
        <br>
              名前:<%=stname %>
    </li>
</body>
</html>