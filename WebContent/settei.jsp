<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="java.net.URLDecoder" %>
<%
	Cookie[] cookies = request.getCookies();
	String stid = "";
	String stname = "";
	if(cookies != null){
		for(Cookie cook : cookies){
			if(cook.getName().equals("StId")){
				stid = cook.getValue();
				//判定する？
				//だめならおかえりください
			}
			if(cook.getName().equals("StName")){
				stname = URLDecoder.decode(cook.getValue(),"UTF-8");
			}
		}
	}
%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>設定</title>
	<link rel="stylesheet" type="text/css" href="tenpure.css">
</head>
<body>
	<Div id ="header"><h1>設定一覧</h1></Div>
	<Div Align="right">
		<a href="GoodBye" class="square_btn2">ログアウト</a>
	</Div>

	<!-- メニューボタン -->
	<a href="GetRanking"><img src="./img/kakusyu.PNG" alt="各種分析" style="position:absolute;top:400px;left:200px;"></a>
	<a href="mediaSerch.jsp"><img src="./img/media.PNG" alt="メディア一覧" style="position:absolute;top:150px;left:200px;"></a>
	<a href="syainkanri.jsp"><img src="./img/syain.PNG" alt="社員管理" style="position:absolute;top:150px;right:350px;"></a>

	<a href="rezisime.jsp"><img src="./img/rezisime.PNG" alt="レジ締め" style="position:absolute;top:400px;right:350px;"></a>

	<INPUT type="button" onclick="location.href='Topmenu.jsp'" value="業務選択へ戻る" name="業務選択へ戻る" style="position:absolute;top:600px;left:50px; WIDTH:150px; HEIGHT: 50px;" ><br>

	<ul>
	<li id = "dare">社員ID：<%=stid %><br>
    	名前:<%=stname %><br>
    	</li>
	</ul>
</body>
</html>