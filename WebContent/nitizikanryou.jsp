<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="java.net.URLDecoder" %>
<%
	   Cookie[] cookies = request.getCookies();
    String stid = "";
    String stname = "";
    if(cookies != null){
        for(Cookie cook : cookies){
            if(cook.getName().equals("StId")){
                stid = cook.getValue();
            }
            if(cook.getName().equals("StName")){
                stname = URLDecoder.decode(cook.getValue(),"UTF-8");
            }
        }
    }
%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>日時処理</title>
    <link rel="stylesheet" type="text/css" href="tenpure.css">
</head>
<body>
    <Div id ="header"><h1>日時処理</h1></Div>
    <Div Align="right">
        <a href="" class="square_btn2">ログアウト</a>
    </Div>

<Div  Align="center"><h1><font color="red">日時処理完了しました</font></h1></Div>
<INPUT type="button" onclick="location.href='rezisime.jsp'" value="レジ締めへ戻る" name="レジ締めへ戻る" style="position:absolute;top:200px;left:370px; WIDTH:200px; HEIGHT: 50px;" ><br>

<ul>
    <li id = "dare">社員ID：<%=stid %><br>
        名前:<%=stname %><br>
        </li>
    </ul>
</body>
</html>