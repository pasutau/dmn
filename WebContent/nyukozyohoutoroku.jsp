<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="java.net.URLDecoder" %>
<%
	Cookie[] cookies = request.getCookies();
	String stid = "";
	String stname = "";
	if(cookies != null){
		for(Cookie cook : cookies){
			if(cook.getName().equals("StId")){
				stid = cook.getValue();
			}
			if(cook.getName().equals("StName")){
				stname = URLDecoder.decode(cook.getValue(),"UTF-8");
			}
		}
	}
%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>入庫登録</title>
<link rel="stylesheet" type="text/css" href="tenpure.css">
</head>
<body>
	<Div id ="header"><h1>入庫登録</h1></Div>
	<Div Align="right">
		<a href="GoodBye" class="square_btn2">ログアウト</a>
	</Div>

	<!-- メニューボタン -->
	<a href="titleTuika.jsp"><img src="./img/taitorutuika.PNG" alt="タイトル追加" style="position:absolute;top:230px;left:250px;"></a>
	<a href="mediatuika.jsp"><img src="./img/mdeiatuika.PNG" alt="メディア追加" style="position:absolute;top:230px;right:250px;"></a>

	<ul>
    <li id = "dare">社員ID：<%=stid %><br>
    	名前:<%=stname %><br>
    </li>
	</ul>


</body>
</html>