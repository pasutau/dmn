<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="java.net.URLDecoder" %>
<%@ page import="dmx.LoanRegistration" %>
<%
	Cookie[] cookies = request.getCookies();
	String stid = "";
	String stname = "";
	if(cookies != null){
		for(Cookie cook : cookies){
			if(cook.getName().equals("StId")){
				stid = cook.getValue();
			}
			if(cook.getName().equals("StName")){
				stname = URLDecoder.decode(cook.getValue(),"UTF-8");
			}
		}
	}
%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>メディア貸出登録完了</title>
	<link rel="stylesheet" type="text/css" href="tenpure.css">
</head>
<body>
	 <Div id ="header"><h1>メディア貸出登録完了</h1></Div>
    <jsp:useBean id="mediaMei" class="dmx.LoanRegistration" scope="session" />
    <jsp:useBean id="ryokin" class="dmx.LoanRegistration" scope="session" />

    <Div Align="center">
    <table border="1" >
    <tr><th>タイトル</th><th>料金</th>

<%
String[] mMei = (String[])session.getAttribute("mediaMei[]");
int[]    ryo  = (int[])session.getAttribute("ryokin[]");
int   siyousu = (int)session.getAttribute("siyousu");
int     gokei = (int)session.getAttribute("gokei");
    int soeji = 0;
    while(soeji < 20) {
    	if(mMei[soeji] != ""){
    	    out.println("<tr><td>" + mMei[soeji] + "</td><td>" + ryo[soeji] + "</td><tr>");
    	}
        soeji+=1;
    }
    session.removeAttribute("mediaMei");
    session.removeAttribute("ryokin");
    session.removeAttribute("siyousu");
    session.removeAttribute("gokei");
%>
</tr>
</table>
</div>
<div>スタンプ使用数:<%= siyousu %></div>
<div><h2>合計金額:<%= gokei %></h2></div>
	<input type="button"  onclick="location.href='mediakasidasi.jsp'" value="OK" name="OK" style="position:absolute;top:540px;left:570px; WIDTH:200px; HEIGHT: 50px;" ><br>

<ul>
	<li id = "dare">社員ID：<%=stid %><br>
    	名前:<%=stname %><br>
    	</li>
	</ul>
</body>
</html>