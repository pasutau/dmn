<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="java.net.URLDecoder" %>
<%
	Cookie[] cookies = request.getCookies();
	String stid = "";
	String stname = "";
	if(cookies != null){
		for(Cookie cook : cookies){
			if(cook.getName().equals("StId")){
				stid = cook.getValue();
			}
			if(cook.getName().equals("StName")){
				stname = URLDecoder.decode(cook.getValue(),"UTF-8");
			}
		}
	}
%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>社員情報登録確認</title>
	<link rel="stylesheet" type="text/css" href="tenpure.css">
</head>
<body>

	<Div id ="header"><h1>社員情報登録確認</h1></Div><br>
	<jsp:useBean id ="sem" class ="dmx.SyainAddServlet" scope = "session" />

	<div Align="center"><h1>社員名:<jsp:getProperty name = "sem" property = "sName"/></h1></div><br>
	<div Align="center"><h1>社員名(かな):<jsp:getProperty name = "sem" property = "sNameKana"/></h1></div><br>
	<div Align="center"><h1>店舗:<jsp:getProperty name = "sem" property = "sTenpo"/></h1></div>

	<Div Align="center" style="position:absolute;top:480px;left:570px;">
		<h3>この内容で登録しますか？</h3>
	</Div>

	<input type="button"  onclick="location.href='syainzyouhou.jsp'" value="戻る" name="戻る" style="position:absolute;top:560px;left:460px; WIDTH:200px; HEIGHT: 50px;" ><br>
	<input type="submit"  onclick="location.href='syainzyouhoutorokukanryo.jsp'" value="登録する" name="登録する" style="position:absolute;top:560px;right:460px; WIDTH:200px; HEIGHT: 50px;" ><br>

	<ul>
	<li id = "dare">社員ID:<%=stid %><br>
    	名前:<%=stname %><br></li>
	</ul>
</body>
</html>