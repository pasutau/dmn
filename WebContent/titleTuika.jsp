<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="java.net.URLDecoder" %>
<%
    Cookie[] cookies = request.getCookies();
    String stid = "";
    String stname = "";
    if(cookies != null  ){
        for(Cookie cook : cookies){
            if(cook.getName().equals("StId")){
                stid = cook.getValue();
            }
            if(cook.getName().equals("StName")){
                stname = URLDecoder.decode(cook.getValue(),"UTF-8");
            }
        }
    }
%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>新タイトル追加</title>
    <link rel="stylesheet" type="text/css" href="tenpure.css">
</head>
<body>
    <form action=TitleTuika>



<SPAN style="position:relative;left:310px">
    	<font size="5">

                タイトル名:<input type="text" name="pName" placeholder="タイトル名"><br>
                タイトルかな:<input type="text" name="pNameKana" placeholder="タイトルかな"><br>

                収録時間:<input type="text" name="pTime" placeholder="収録時間"><br>
                レンタル開始日:<select id="room" name="room"><br>

        <%
          for(int i=1900; i<2018; i++)    {
             out.print("<option value="+i+">"+ i +"年</option>");
          }
        %>
        </select> <select id="tuki" name="tuki">
        <%
          for(int i=1; i<13; i++) {
            out.print("<option value="+i+">"+ i +"月</option>");
          }
        %>
        </select> <select id="hi" name="hi">
        <%
          for(int i=1; i<32; i++) {
            out.print("<option value="+i+">"+ i +"日</option>");
          }
        %>
        </select><br>
         区分:<select name="kubun">
            <option value="3">旧作</option>
            <option value="2">準新作</option>
            <option value="1">新作</option>
        </select><br>
        主演:<input type="text" name="pShuen" placeholder="主演"><br>
        主演かな:<input type="text" name="pShuenKana" placeholder="主演かな"><br>
        監督:<input type="text" name="pKantoku" placeholder="監督"><br>
        監督かな<input type="text" name="pKantokuKana" placeholder="監督かな"><br>
       	</SPAN>
       <br>
       <br>
    <SPAN style="position:relative;left:380px"><input type="reset" value = "削除">
   	</SPAN>

       <SPAN style="position:relative;left:460px"><input type="submit" value ="確定">
	</SPAN>
      </font>
    </form>

    <INPUT type="button" onclick="location.href='zaikokanri.jsp'" value="在庫管理へ戻る" name="在庫管理へ戻る" style="position:absolute;top:600px;left:50px; WIDTH:150px; HEIGHT: 50px;" ><br>

    <ul>
    <li id = "dare">社員ID：<%=stid %><br>
        名前:<%=stname %><br>
        </li>
    </ul>

</body>
</html>