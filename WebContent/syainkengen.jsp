<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="java.net.URLDecoder" %>
<%@ page import = "dmx.*" %>
<%@ page import = "db.*" %>
<%
	Cookie[] cookies = request.getCookies();
	String stid = "";
	String stname = "";
	if(cookies != null){
		for(Cookie cook : cookies){
			if(cook.getName().equals("StId")){
				stid = cook.getValue();
			}
			if(cook.getName().equals("StName")){
				stname = URLDecoder.decode(cook.getValue(),"UTF-8");
			}
		}
	}
	Authority auth = new Authority();
	boolean kyokaFlg = auth.AuthorityCheck(stid,4);
	if(kyokaFlg == false){
	response.sendRedirect("error.jsp");
	}
%>

<html>
<head>
	<meta charset="UTF-8">
	<title>社員検索</title>
	<link rel="stylesheet" type="text/css" href="tenpure.css">
</head>
<body>
	<Div id ="header"><h1>社員検索</h1></Div>

	<form method ="post" action="SyainkensakuServlet2">
		<div style="text-align:center"><h2>必要項目に入力してください</h2></div><br>
		<div style="text-align:center"> <span style="font-size:large">社員ID:</span><input type="text" name="sid">  </div><br>
		<div align="center"><input type="submit" value="検索"></div>
		</form>

	<INPUT type="button" onclick="location.href='syainkanri.jsp'" value="社員管理へ戻る" name="社員管理へ戻る" style="position:absolute;top:600px;left:50px; WIDTH:150px; HEIGHT: 50px;" ><br>

	<ul>
	<li id = "dare">社員ID：<%=stid %><br>
    	名前:<%=stname %><br>
    	</li>
	</ul>
</body>
</html>