<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="java.net.URLDecoder" %>
<%
	Cookie[] cookies = request.getCookies();
	String stid = "";
	String stname = "";
	if(cookies != null){
		for(Cookie cook : cookies){
			if(cook.getName().equals("StId")){
				stid = cook.getValue();
			}
			if(cook.getName().equals("StName")){
				stname = URLDecoder.decode(cook.getValue(),"UTF-8");
			}
		}
	}
%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>社員管理</title>
	<link rel="stylesheet" type="text/css" href="tenpure.css">
</head>
<body>
	<Div id ="header"><h1>社員管理</h1></Div>
    <Div Align="right">
        <a href="GoodBye" class="square_btn2">ログアウト</a>
    </Div>

	<a href="syainzyouhou.jsp"><img src="./img/syainzyouhou.PNG" alt="社員情報登録" style="position:absolute;top:100px;left:580px;"></a>
	<a href="syainkensaku.jsp"><img src="./img/syainzyouhou2.PNG" alt="社員情報変更" style="position:absolute;top:250px;left:580px;"></a>
	<a href="syainkengen.jsp"><img src="./img/syainkengen.PNG" alt="社員権限設定" style="position:absolute;top:400px;left:580px;"></a>
	<a href="syainkengen2.jsp"><img src="./img/syainkengensakujyo.PNG" alt="社員権限削除" style="position:absolute;top:550px;left:580px;"></a>

	<INPUT type="button" onclick="location.href='settei.jsp'" value="設定へ戻る" name="設定へ戻る" style="position:absolute;top:600px;left:50px; WIDTH:150px; HEIGHT: 50px;" ><br>

	<ul>
	<li id = "dare">社員ID:<%=stid %><br>
    	名前:<%=stname %><br>
    	</li>
	</ul>
</body>
</html>