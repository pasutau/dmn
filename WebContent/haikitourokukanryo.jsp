<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="java.net.URLDecoder" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="bean.MediaBean" %>
<%
	Cookie[] cookies = request.getCookies();
	String stid = "";
	String stname = "";
	if(cookies != null  ){
		for(Cookie cook : cookies){
			if(cook.getName().equals("StId")){
				stid = cook.getValue();
			}
			if(cook.getName().equals("StName")){
				stname = URLDecoder.decode(cook.getValue(),"UTF-8");
			}
		}
	}
%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>廃棄登録完了</title>
	<link rel="stylesheet" type="text/css" href="tenpure.css">
</head>
<body>
<jsp:useBean id="mdinfoBean" class="bean.MediadisposalinfoBean" scope="session" />
	<Div id ="header"><h1>廃棄登録完了</h1></Div><br>


	<%----------------------------------- 廃棄メディア表を表示しています----------------------------------%>
<Div Align="center">
<table border="1" >
<tr><th>タイトル</th><th>メディアＩＤ</th>


<%
ArrayList<MediaBean> mdRecordArray
	= mdinfoBean.getMediaRecordArray();

for(MediaBean rcd : mdRecordArray)	{
	out.println("<tr><td>" + rcd.getMedianame() + "</td><td>"
			+ rcd.getmId() + "</td><tr>");
}
%>

</table>
<!-- 日付を表示する !-->
<script>
var hiduke=new Date();

//年・月・日・曜日を取得する
var year = hiduke.getFullYear();
var month = hiduke.getMonth()+1;
var week = hiduke.getDay();
var day = hiduke.getDate();

var yobi= new Array("日","月","火","水","木","金","土");

var haikiday =(year+"年"+month+"月"+day+"日 ");//廃棄日
document.open();
document.write(haikiday);
document.close();
</script>
</div>

	<div align="center"><h3>この内容で確定しました</h3></div>

	<INPUT type="button" onclick="location.href='zaikokanri.jsp'" value="在庫管理へ戻る" name="在庫管理へ戻る" style="position:absolute;top:600px;left:50px; WIDTH:150px; HEIGHT: 50px;" ><br>

	<ul>
	<li id = "dare">社員ID：<%=stid %><br>
    	名前:<%=stname %><br>
    	</li>
	</ul>

</body>
</html>