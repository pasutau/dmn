<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="java.net.URLDecoder" %>
<%
	Cookie[] cookies = request.getCookies();
	String stid = "";
	String stname = "";
	if(cookies != null){
		for(Cookie cook : cookies){
			if(cook.getName().equals("StId")){
				stid = cook.getValue();
			}
			if(cook.getName().equals("StName")){
				stname = URLDecoder.decode(cook.getValue(),"UTF-8");
			}
		}
	}
%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>在庫管理</title>
	<link rel="stylesheet" type="text/css" href="tenpure.css">
</head>
<body>
	<Div id ="header"><h1>在庫管理</h1></Div>
	<Div Align="right">
		<a href="GoodBye" class="square_btn2">ログアウト</a>
	</Div>

	<!-- メニューボタン -->
	<a href="nyukozyohoutoroku.jsp"><img src="./img/nyukotoroku.PNG" alt="入庫登録" style="position:absolute;top:150px;left:250px;"></a>
	<a href="haikitoroku.jsp"><img src="./img/haikitoroku.PNG" alt="廃棄登録" style="position:absolute;top:150px;right:250px;"></a>
	<a href="tenpokanzaiko.jsp"><img src="./img/tenpokanzaiko.PNG" alt="店舗間在庫" style="position:absolute;top:400px;left:250px;"></a>
	<a href="MediaFusokuItiranServlet"><img src="./img/husokuzaiko.PNG" alt="不足在庫一覧" style="position:absolute;top:400px;right:250px;"></a>

	<INPUT type="button" onclick="location.href='tujogyomu.jsp'" value="通常業務へ戻る" name="通常業務へ戻る" style="position:absolute;top:600px;left:50px; WIDTH:150px; HEIGHT: 50px;" ><br>

	<ul>
	<li id = "dare">社員ID：<%=stid %><br>
    	名前:<%=stname %><br>
    	</li>
	</ul>

</body>
</html>