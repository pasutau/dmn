<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="java.net.URLDecoder" %>
<%
	Cookie[] cookies = request.getCookies();
	String stid = "";
	String stname = "";
	if(cookies != null){
		for(Cookie cook : cookies){
			if(cook.getName().equals("StId")){
				stid = cook.getValue();
			}
			if(cook.getName().equals("StName")){
				stname = URLDecoder.decode(cook.getValue(),"UTF-8");
			}
		}
	}
%>

<!DOCTYPE html>

<html>
<head>
	<meta charset="UTF-8">
	<title>メディア貸出登録</title>
	<link rel="stylesheet" type="text/css" href="tenpure.css">
</head>
<body>
	<Div id ="header"><h1>メディア貸出登録</h1></Div>

<div style="text-align : center">
<form action = LoanRegistration method=post>
	<input type="text" name="uid" placeholder="会員ID">
	<br>
	<br>
	<input type="text" name="mediaid1" placeholder="メディアID1">
	<input type="text" name="hi1" placeholder="貸出日数1">
	<input type="text" name="mediaid2" placeholder="メディアID2">
	<input type="text" name="hi2" placeholder="貸出日数2">
<br>
	<input type="text" name="mediaid3" placeholder="メディアID3">
	<input type="text" name="hi3" placeholder="貸出日数3">
	<input type="text" name="mediaid4" placeholder="メディアID4">
	<input type="text" name="hi4" placeholder="貸出日数4">
<br>
	<input type="text" name="mediaid5" placeholder="メディアID5">
	<input type="text" name="hi5" placeholder="貸出日数5">
	<input type="text" name="mediaid6" placeholder="メディアID6">
	<input type="text" name="hi6" placeholder="貸出日数6">
<br>
	<input type="text" name="mediaid7" placeholder="メディアID7">
	<input type="text" name="hi7" placeholder="貸出日数7">
	<input type="text" name="mediaid8" placeholder="メディアID8">
	<input type="text" name="hi8" placeholder="貸出日数8">
<br>
	<input type="text" name="mediaid9" placeholder="メディアID9">
	<input type="text" name="hi9" placeholder="貸出日数9">
	<input type="text" name="mediaid10" placeholder="メディアID10">
	<input type="text" name="hi10" placeholder="貸出日数10">
<br>
	<input type="text" name="mediaid11" placeholder="メディアID11">
	<input type="text" name="hi11" placeholder="貸出日数11">
	<input type="text" name="mediaid12" placeholder="メディアID12">
	<input type="text" name="hi12" placeholder="貸出日数12">
<br>
	<input type="text" name="mediaid13" placeholder="メディアID13">
	<input type="text" name="hi13" placeholder="貸出日数13">
	<input type="text" name="mediaid14" placeholder="メディアID14">
	<input type="text" name="hi14" placeholder="貸出日数14">
<br>
	<input type="text" name="mediaid15" placeholder="メディアID15">
	<input type="text" name="hi15" placeholder="貸出日数15">
	<input type="text" name="mediaid16" placeholder="メディアID16">
	<input type="text" name="hi16" placeholder="貸出日数16">
<br>
	<input type="text" name="mediaid17" placeholder="メディアID17">
	<input type="text" name="hi17" placeholder="貸出日数17">
	<input type="text" name="mediaid18" placeholder="メディアID18">
	<input type="text" name="hi18" placeholder="貸出日数18">
<br>
	<input type="text" name="mediaid19" placeholder="メディアID19">
	<input type="text" name="hi19" placeholder="貸出日数19">
	<input type="text" name="mediaid20" placeholder="メディアID20">
	<input type="text" name="hi20" placeholder="貸出日数20">
	<br><br>

	<div Align="center"><h3>スタンプ使用数:<select name="siyousu">
	                                   <option value="0"selected>0</option>
									   <option value="1">1</option>
									   <option value="2">2</option>
									   <option value="3">3</option>
									   <option value="4">4</option>
									   <option value="5">5</option>
									   <option value="6">6</option>
									   <option value="7">7</option>
									   <option value="8">8</option>
									   <option value="9">9</option>
									   <option value="10">10</option>
									   <option value="11">11</option>
									   <option value="12">12</option>
									   <option value="13">13</option>
									   <option value="14">14</option>
									   <option value="15">15</option>
									   <option value="16">16</option>
									   <option value="17">17</option>
									   <option value="18">18</option>
									   <option value="19">19</option>
									   <option value="20">20</option>
									   </select>
	</h3></div>
    <input type="reset" value = "削除">
    <input type="submit" value ="確定">

   </form>
   </div>

	<input type="button" onclick="location.href='kasidasikanri.jsp'" value="貸出へ戻る" name="貸出へ戻る"
	                    style="position:absolute;top:600px;left:50px; WIDTH:150px; HEIGHT: 50px;" ><br>
	<ul>
	<li id = "dare">社員ID:<%=stid %><br>
    	名前:<%=stname %><br></li>
	</ul>
</body>
</html>