<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="java.net.URLDecoder" %>
<%
	Cookie[] cookies = request.getCookies();
	String stid = "";
	String stname = "";
	if(cookies != null){
		for(Cookie cook : cookies){
			if(cook.getName().equals("StId")){
				stid = cook.getValue();
			}
			if(cook.getName().equals("StName")){
				stname = URLDecoder.decode(cook.getValue(),"UTF-8");
			}
		}
	}
%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>メディア検索結果一覧</title>
	<link rel = "stylesheet" type = "text/css" href = "tenpure.css" > <!-- CSS定義 -->
</head>
<body>
<jsp:useBean id="productBean" class="bean.ProductBean" scope="session" />

	<Div id = "header"><h1>メディア検索結果一覧</h1></Div>
    <Div Align="right">
        <a href="GoodBye" class="square_btn2">ログアウト</a>
    </Div>

	<Div id = "header"><h2>スキャンされたタイトル名</h2></Div><br>

<!-- 表 -->
	<div  style="height:250px; width:500px;  align:center;">
	<table border="1px" cellpadding="6px" align="right" style="border-collapse: collapse;">
	<colgroup>
		<!-- 1列目のグループ -->
		<col span="1" style="background-color: #ffffff;" />
		<!-- 2列目のグループ -->
		<col span="1" style="background-color: #ffffff;" />
	</colgroup>
	<tr bgcolor="#C0C0C0">

	</tr>

	<tr>
		<td align="center"><%= productBean.getpName() %></td>
	</tr>
</table>
</div>

    <form method="post" action="MediaupdateServlet">
    <input type="hidden" name="pid" value="<%= productBean.getpId()%>">
    <Div Align="center"><h2>
        準新作<input type="radio" name="radio1" value="2">
        旧作<input type="radio" name="radio1" value="3"></h2></Div><!-- ラジオボタン -->

    <INPUT type="button" onclick="location.href='mediaSerch.jsp'" value="メディア検索へ戻る" name="メディア検索へ戻る"
        style="position:absolute;top:600px;left:50px; WIDTH:150px; HEIGHT: 50px;" ><br>

        <Div Align="center"><input type="submit" value="登録" name="登録"
            style="position:absolute;top:500px;left:390px; WIDTH:150px; HEIGHT: 50px;" ><br></Div></form>

<ul>
		<li id = "dare" style="position:absolute;top:570px;left:730px; WIDTH:150px; HEIGHT: 50px;">
		社員ID:<%=stid %><br>
		<br>
		名前:<%=stname %></li></ul>

</body>
</html>