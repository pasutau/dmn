<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="java.net.URLDecoder" %>
<%@ page import="java.util.*" %>
<%@ page import="bean.*" %>
<%
	Cookie[] cookies = request.getCookies();
	String stid = "";
	String stname = "";
	if(cookies != null){
		for(Cookie cook : cookies){
			if(cook.getName().equals("StId")){
				stid = cook.getValue();
			}
			if(cook.getName().equals("StName")){
				stname = URLDecoder.decode(cook.getValue(),"UTF-8");
			}
		}
	}
%>

<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>延滞者一覧</title>
	<link rel = "stylesheet" type = "text/css" href = "tenpure.css" > <!-- CSS定義 -->

</head>
<body>
<jsp:useBean id="enInfoBean" class="bean.EntaiInfoBean" scope="session" />
	<Div  Align="center"><h1>延滞者一覧</h1></Div>
    <Div Align="right">
        <a href="GoodBye" class="square_btn2">ログアウト</a>
    </Div>
		<br>
		<br>
		<br>
		<br>
	</Div>
<form method="post" action="EntaiUpdateServlet">
<Div  Align="center">
<!-- 表 -->
<table border="1">

	<colgroup>
		<!-- 1列目のグループ -->
		<col span="1" style="background-color: #ffffff;" />
		<!-- 2列目のグループ -->
		<col span="1" style="background-color: #ffffff;" />
		<!-- 3列目のグループ -->
		<col span="1" style="background-color: #ffffff;" />
		<!-- 4列目のグループ -->
		<col span="1" style="background-color: #ffffff;" />
		<col span="1" style="background-color: #ffffff;" />
		<col span="1" style="background-color: #ffffff;" />
	</colgroup>

	<tr bgcolor="#C0C0C0">
		<th  align="center"><span style="margin-right: 10px;"></span>貸出番号<span style="margin-right: 10px;"></span></th>
		<th  align="center"><span style="margin-right: 10px;"></span>会員ID<span style="margin-right: 10px;"></span></th>
		<th  align="center"><span style="margin-right: 10px;"></span>会員名<span style="margin-right: 10px;"></span></th>
		<th  align="center"><span style="margin-right: 10px;"></span>電話番号<span style="margin-right: 10px;"></span></th>
		<th  align="center"><span style="margin-right: 10px;"></span>連絡者<span style="margin-right: 10px;"></span></th>
		<th  align="center"><span style="margin-right: 10px;"></span>連絡状態<span style="margin-right: 10px;"></span></th>
	</tr>

	<%
	int count = 1;
	ArrayList<EntaiBean> enInfoArray = enInfoBean.getEntaiArray();
	session.setAttribute("enInfoArray", enInfoArray);
	for(EntaiBean rc : enInfoArray) {
		int status = Integer.parseInt(rc.getDcflag());
 %>
	<tr>
		<td align="center"><%= rc.getLid() %></td>
		<td align="center"><%= rc.getUname() %></td>
		<td align="center"><%= rc.getUid() %></td>
		<td align="center"><%= rc.getUname() %></td>
		<td align="center"><%= rc.getuTel() %></td>
		<td align="center"><p>
		<%if(rc.getDcflag().equals("1")){ %>
		<select name="status<%= count %>">
			<option value="1">保留</option>
			<option value="2">連絡がついた</option>
			</select><br>
		<%} else { %>
		連絡済です
		<input type="hidden" name="status<%= count %>" value="2">
		<%} count++;%>
		<br></td>


	</tr>
<%
	}
%>

</table>
</Div>
	<INPUT type="button" onclick="location.href='kainkensakumenu.jsp'" value="会員検索へ戻る" name="会員検索へ戻る"
	 style="position:absolute;top:500px;left:100px; WIDTH:200px; HEIGHT: 50px;" ><br>
	<Div Align="center"><input type="submit" value="更新" name="更新"
	style="position:absolute;top:500px;left:700px; WIDTH:200px; HEIGHT: 50px;" ><br></Div></form>
	<ul>
	<li id = "dare">社員ID：<%=stid %><br>
    	名前:<%=stname %><br></li>
	</ul>
</body>
</html>