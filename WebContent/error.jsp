<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="java.net.URLDecoder" %>
<%
    Cookie[] cookies = request.getCookies();
    String stid = "";
    String stname = "";
    if(cookies != null  ){
        for(Cookie cook : cookies){
            if(cook.getName().equals("StId")){
                stid = cook.getValue();
            }
            if(cook.getName().equals("StName")){
                stname = URLDecoder.decode(cook.getValue(),"UTF-8");
            }
        }
    }
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>エラー</title>
    <link rel="stylesheet" type="text/css" href="tenpure.css">
</head>
<body>
    <br><br><br>
    <div><h1 align="center"><font color="red">権限がありません</font></h1></div>
    <input type="button" onclick="location.href='Topmenu.jsp'" value="戻る" name="back" style="position:absolute;top:600px;left:50px; WIDTH:150px; HEIGHT: 50px;" ><br>
    <li id = "dare">社員ID:<%= stid %><br>
        名前:<%= stname %><br></li>
    </ul>
</body>
</html>