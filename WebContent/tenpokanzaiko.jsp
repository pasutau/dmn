<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="java.net.URLDecoder" %>
<%
	Cookie[] cookies = request.getCookies();
	String stid = "";
	String stname = "";
	if(cookies != null  ){
		for(Cookie cook : cookies){
			if(cook.getName().equals("StId")){
				stid = cook.getValue();
			}
			if(cook.getName().equals("StName")){
				stname = URLDecoder.decode(cook.getValue(),"UTF-8");
			}
		}
	}
%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>店舗間在庫管理</title>
	<link rel="stylesheet" type="text/css" href="tenpure.css">
</head>
<body>
	<Div id ="header"><h1>店舗間在庫管理</h1></Div>
	<Div Align="right">
		<a href="#" class="square_btn2">ログアウト</a>
	</Div>

	<!-- メニューボタン -->

	<a href="LoanServlet"><img src="./img/sinseiitiran.PNG" alt="申請一覧" style="position:absolute;top:150px;left:580px;"></a>
	<a href="TkasidasiServlet"><img src="./img/kasidasityuitiran.PNG" alt="貸出中一覧" style="position:absolute;top:300px;left:580px;"></a>
	<a href="BorrowingServlet"><img src="./img/kariiretyuitiran.PNG" alt="借入中一覧" style="position:absolute;top:450px;left:580px;"></a>

	<INPUT type="button" onclick="location.href='zaikokanri.jsp'" value="在庫管理へ戻る" name="在庫管理へ戻る" style="position:absolute;top:600px;left:50px; WIDTH:150px; HEIGHT: 50px;" ><br>

	<ul>
	<li id = "dare">社員ID：<%=stid %><br>
    	名前:<%=stname %><br>
    	</li>
	</ul>
</body>
</html>