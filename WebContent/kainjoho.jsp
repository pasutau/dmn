<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="java.net.URLDecoder" %>
<%
	Cookie[] cookies = request.getCookies();
	String stid = "";
	String stname = "";
	String regiid = "";
	if(cookies != null){
		for(Cookie cook : cookies){
			if(cook.getName().equals("StId")){
				stid = cook.getValue();
			}
			if(cook.getName().equals("StName")){
				stname = URLDecoder.decode(cook.getValue(),"UTF-8");
			}
            if(cook.getName().equals("RegiId")){
                regiid = URLDecoder.decode(cook.getValue(),"UTF-8");
            }
		}
	}
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet type=text/css" href="tenpure.css">
</head>
<body>
	<div id="header">
		<h1>会員情報登録</h1>
	</div>
	<form action="MemberAddServlet" method="post">
	<input type="hidden" name="rid" value="<%= regiid%>">
	<p>

	<label><span style="color: #cd5b00; font-size: large; margin-left:50px">お名前</span>

		<span style="font-size: large; margin-left:170px"></span> <input type="text" name="name"></label>
	</p>

	<label><span style="color: #cd5b00; font-size: large ;margin-left:50px">お名前(かな)</span>


		<span style="font-size: large; margin-left:122px"></span> <input type="text" name="oname"></label>
	<p>
		<label for="room" style="color: #cd5b00;margin-left:50px ">生年月日</label>

		<SPAN style="position:relative;left:160px"><select id="room" name="room" >
			<%

				for(int i=1900; i<2018; i++)	{
					out.print("<option value="+i+">"+ i +"年</option>");
				}
				%>

		</select> <select id="tuki" name="tuki">
			<%
				for(int i=1; i<13; i++)	{
					out.print("<option value="+i+">"+ i +"月</option>");
				}
				%>
		</select> <select id="hi" name="hi">
			<%
				for(int i=1; i<32; i++)	{
					out.print("<option value="+i+">"+ i +"日</option>");
				}
				%>

		</select>
</SPAN>
	</p>

	<label><span style="color: #cd5b00; font-size: large;;margin-left:50px ">性別</span></label>


	<SPAN style="position:relative;left:180px"><input type="radio" name="radio1" id="male" value="1"> <label for="male">男性</label>
	<input type="radio" name="radio1" id="female"value="2"> <label for="female">女性</label>
</SPAN>

	<br>
	<br>
	<label><span style="color: #cd5b00; font-size: large; margin-left:50px ">ご就業</span></label>

	<SPAN style="position:relative;left:163px">
	<input type="radio" name="radio2" id="koumu"value="1"> <label for="koumu">会社員・公務員</label>
	<input type="radio" name="radio2" id="fbaito"value="2"> <label for="fbaito">派遺・アルバイト</label>
	<input type="radio" name="radio2" id="ziei"value="3"> <label for="ziei">自営業</label>
	<input type="radio" name="radio2" id="sengyou"value="4"><label for="sengyou">専業主婦</label>
	<input type="radio" name="radio2" id="gakusei"value="5"> <label for="gakusei">学生</label>
	<br>
	<br>
</SPAN>


<label><span style="color: #cd5b00; font-size: large; margin-left:50px ">ご住所</span></label>



			<label><span style="color: #cd5b00; font-size: large; margin-left:130px ">〒</span></label>
			<label><span style="font-size: large; margin-left:15px"></span> <input type="text" name="yuubin"></label>
<br>


					<label><span style="color: #cd5b00; font-size: large; margin-left:264px ">(都道府県)</span></label>
<br>

		<SPAN style="position:relative;left:270px"><select name="ken">
			<option selected="selected">都道府県</option>
			<option value="北海道">北海道</option>
			<option value="青森県">青森県</option>
			<option value="岩手県">岩手県</option>
			<option value="宮城県">宮城県</option>
			<option value="秋田県">秋田県</option>
			<option value="山形県">山形県</option>
			<option value="福島県">福島県</option>
			<option value="茨城県">茨城県</option>
			<option value="栃木県">栃木県</option>
			<option value="群馬県">群馬県</option>
			<option value="埼玉県">埼玉県</option>
			<option value="千葉県">千葉県</option>
			<option value="東京都">東京都</option>
			<option value="神奈川県">神奈川県</option>
			<option value="新潟県">新潟県</option>
			<option value="富山県">富山県</option>
			<option value="石川県">石川県</option>
			<option value="福井県">福井県</option>
			<option value="山梨県">山梨県</option>
			<option value="長野県">長野県</option>
			<option value="岐阜県">岐阜県</option>
			<option value="静岡県">静岡県</option>
			<option value="愛知県">愛知県</option>
			<option value="三重県">三重県</option>
			<option value="滋賀県">滋賀県</option>
			<option value="京都府">京都府</option>
			<option value="大阪府">大阪府</option>
			<option value="兵庫県">兵庫県</option>
			<option value="奈良県">奈良県</option>
			<option value="和歌山県">和歌山県</option>
			<option value="鳥取県">鳥取県</option>
			<option value="島根県">島根県</option>
			<option value="岡山県">岡山県</option>
			<option value="広島県">広島県</option>
			<option value="山口県">山口県</option>
			<option value="徳島県">徳島県</option>
			<option value="香川県">香川県</option>
			<option value="愛媛県">愛媛県</option>
			<option value="高知県">高知県</option>
			<option value="福岡県">福岡県</option>
			<option value="佐賀県">佐賀県</option>
			<option value="長崎県">長崎県</option>
			<option value="熊本県">熊本県</option>
			<option value="大分県">大分県</option>
			<option value="宮崎県">宮崎県</option>
			<option value="鹿児島県">鹿児島県</option>
			<option value="沖縄県">沖縄県</option>
	</select>
	</SPAN>
		<div>
			<br>
		</div> <span style="font-size: small; color: #cd5b00; text-align: right;margin-left:270px ">
		(都市区町村)</span>
		<br>

		<SPAN style="position:relative;left:270px"><input type="text" name="tosi" size="50"> <br> <br>
</SPAN>
		<span style="color: #cd5b00; font-size: large;margin-left:50px">電話番号</span>
		<span style="font-size: large;margin-left:144px"></span>

		<input type="text" name="denwa"> <br> <br>
		<span style="color: #cd5b00; font-size: large;margin-left:50px">連絡用mailアドレス</span>
		<span style="font-size: large"></span>

		<SPAN style="position:relative;left:70px">	<input type="text" name="mail"> <br> <br></SPAN>
		<INPUT type="button"  onClick="history.back()" value="会員管理へ戻る" style="position: absolute; top: 650px; left: 10px; WIDTH: 150px; HEIGHT: 50px;"><br>
		<INPUT type="submit" value="会員情報保存" style="position: absolute; top: 650px; right: 10px; WIDTH: 150px; HEIGHT: 50px;"><br></form>
		<ul>
	<li id = "dare">社員ID：<%=stid %><br>
    	名前:<%=stname %><br>
    	</li>
	</ul>

</body>
</html>