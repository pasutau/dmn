<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="java.net.URLDecoder" %>
<%
	Cookie[] cookies = request.getCookies();
	String stid = "";
	String stname = "";
	if(cookies != null){
		for(Cookie cook : cookies){
			if(cook.getName().equals("StId")){
				stid = cook.getValue();
			}
			if(cook.getName().equals("StName")){
				stname = URLDecoder.decode(cook.getValue(),"UTF-8");
			}
		}
	}
%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>社員情報権限登録確認</title>
	<link rel="stylesheet" type="text/css" href="tenpure.css">
</head>
<body>

	<Div id ="header"><h1>社員情報権限登録確認</h1></Div><br>

	<div Align="center"><h1>社員ID:0002 名前:野田和希</h1></div><br>
	<div Align="center"><h2>職位:アルバイト<span style="margin-right: 40px;"></span>権限:貸出管理</h2></div>

	<Div Align="center" style="position:absolute;top:480px;left:570px;">
		<h3>この内容で登録しますか?</h3>
	</Div>

	<input type="button"  onclick="location.href='syainkengentoroku.jsp'" value="戻る" name="戻る" style="position:absolute;top:560px;left:450px; WIDTH:200px; HEIGHT: 50px;" ><br>
	<input type="button"  onclick="location.href='syainkengentorokukanryo.jsp'" value="登録" name="登録" style="position:absolute;top:560px;right:450px; WIDTH:200px; HEIGHT: 50px;" ><br>

	<ul>
	<li id = "dare">社員ID：<%=stid %><br>
    	名前:<%=stname %><br>
    	</li>
	</ul>
</body>
</html>