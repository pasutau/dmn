<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="java.net.URLDecoder"%>
<%@ page import="java.util.*"%>
<%@ page import="bean.*"%>
<%@ page import="dmx.*"%>
<%
	Cookie[] cookies = request.getCookies();
	String stid = "";
	String stname = "";
	if (cookies != null) {
		for (Cookie cook : cookies) {
			if (cook.getName().equals("StId")) {
				stid = cook.getValue();
			}
			if (cook.getName().equals("StName")) {
				stname = URLDecoder.decode(cook.getValue(), "UTF-8");
			}
		}
	}
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>不足在庫一覧</title>
<link rel="stylesheet" type="text/css" href="tenpure.css">
</head>
<body>
	<jsp:useBean id="fusokulist" class="bean.FusokuListBean"
		scope="session" />
	<Div id="header">
		<h1>不足在庫一覧</h1>
	</Div>
	<table border="1" align="center">
		<tr>
			<th>メディア名</th>
			<th>カテゴリ(DVD/BD)</th>
			<!-- <th>区分</th> -->
			<th>本数</th>
		</tr>
		<%
			ArrayList<FusokuRecordBean> fusokuArray = fusokulist.getFusokuList();
			for (FusokuRecordBean record : fusokuArray) {
				String category = "DVD";//1がDVD 2BD
				String kubun = "貸出中";//テスト用
				int zanSu = 0;
				int pkubun = record.getKubun();
				String pid = record.getpId();
				int su = 0;

				if (record.getmCategory().equals("2")) {
					category = "BD";
				}
				if (pkubun == 2) {
					kubun = "貸出可";
				}
				if (pkubun == 2 && su < 2) {//貸出可能なものが1本しかないもののみ表示
		%>
		<tr>
			<td><%=record.getpName()%></td>
			<td><%=category%></td>
			<!--  <td><%=kubun%></td> -->
			<td><%=su%></td>
			<td><form action="MediaTenpoServlet" method="post">
					<input type="hidden" name="pid" value="<%=pid%>"> <input
						type="hidden" name="pkubun" value="<%=pkubun%>"> <input
						type="submit" value="要請する">
				</form></td>
		</tr>
		<%
			}
			}
		%>
	</table>

	<INPUT type="button" onclick="location.href='zaikokanri.jsp'"
		value="在庫管理へ戻る" name="在庫管理へ戻る"
		style="position: absolute; top: 600px; left: 50px; WIDTH: 150px; HEIGHT: 50px;">
	<br>

	<ul>
		<li id="dare">社員ID：<%=stid%><br> 名前:<%=stname%><br>
		</li>
	</ul>
</body>
</html>