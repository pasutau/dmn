<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="java.net.URLDecoder" %>
<%
	Cookie[] cookies = request.getCookies();
	String stid = "";
	String stname = "";
	if(cookies != null){
		for(Cookie cook : cookies){
			if(cook.getName().equals("StId")){
				stid = cook.getValue();
			}
			if(cook.getName().equals("StName")){
				stname = URLDecoder.decode(cook.getValue(),"UTF-8");
			}
		}
	}
%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>社員情報権限登録完了</title>
	<link rel="stylesheet" type="text/css" href="tenpure.css">
</head>
<body>

	<Div id ="header"><h1>社員情報権限登録完了</h1></Div><br>

	<Div  Align="center"><h1><font color="red">社員情報権限登録完了しました</font></h1></Div>

	<input type="button"  onclick="location.href='syainkengen.jsp'" value="OK" name="OK" style="position:absolute;top:560px;left:570px; WIDTH:200px; HEIGHT: 50px;" ><br>

	<ul>
	<li id = "dare">社員ID：<%=stid %><br>
    	名前:<%=stname %><br>
    	</li>
	</ul>
</body>
</html>