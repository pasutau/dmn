<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="java.net.URLDecoder" %>
<%@ page import="java.util.*" %>
<%@ page import="bean.*" %>
<%
	Cookie[] cookies = request.getCookies();
	String stid = "";
	String stname = "";
	String regiid = "";
	if(cookies != null){
		for(Cookie cook : cookies){
			if(cook.getName().equals("StId")){
				stid = cook.getValue();
			}
			if(cook.getName().equals("StName")){
				stname = URLDecoder.decode(cook.getValue(),"UTF-8");
			}
            if(cook.getName().equals("RegiId")){
                regiid = URLDecoder.decode(cook.getValue(),"UTF-8");
            }
		}
	}
%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>会員情報更新</title>
	<link rel = "stylesheet" type = "text/css" href = "tenpure.css" > <!-- CSS定義 -->
</head>
<body>
<jsp:useBean id="memInfoBean" class="bean.MemberInfoBean" scope="session" />
<Div id ="header"><h1>会員情報更新</h1></Div>
<form action="MemberUpdateServlet" method="post">
<input type="hidden" name="rid" value="<%= regiid%>">
<%
	int job;
	int gen;
	ArrayList<MemberBean> memInfoArray = memInfoBean.getMemberArray();
	for(MemberBean rc : memInfoArray) {
		job=0;
		gen=0;
		if(!(rc.getuJob()==null)){
		job = Integer.parseInt(rc.getuJob());
		}
		if(!(rc.getuGender()==null)){
		gen = Integer.parseInt(rc.getuGender());
		}
 %>
	<input type="hidden" name="uid" value=<%= rc.getuId() %>>
	<p>

	<label><span style="color: #cd5b00; font-size: large; margin-left:50px">お名前</span>

		<span style="font-size: large; margin-left:170px"></span> <input type="text" name="name" value=<%= rc.getuName() %>></label>

	</p>
	<label><span style="color: #cd5b00; font-size: large">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;お名前(かな)</span>
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		<span style="font-size: large"></span> <input type="text" name="oname" value=<%= rc.getuNameKana() %>></label><br> <br>


	<p>
		<label for="room" style="color: #cd5b00;margin-left:50px ">生年月日</label>

		<SPAN style="position:relative;left:160px"><select id="room" name="room" >
			<%

				for(int i=1900; i<2018; i++)	{
					out.print("<option value="+i+">"+ i +"年</option>");
				}
				%>

		</select> <select id="tuki" name="tuki">
			<%
				for(int i=1; i<13; i++)	{
					out.print("<option value="+i+">"+ i +"月</option>");
				}
				%>
		</select> <select id="hi" name="hi">
			<%
				for(int i=1; i<32; i++)	{
					out.print("<option value="+i+">"+ i +"日</option>");
				}
				%>

		</select>
</SPAN>
	</p>


	<label><span style="color: #cd5b00; font-size: large">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;性別</span></label>
	&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	<input type="radio" name="radio1" id="male" value="1"<%if(gen==1){ %> checked <%} %>> <label for="male">男性</label>
	<input type="radio" name="radio1" id="female"value="2"<%if(gen==2){ %> checked <%} %>> <label for="female">女性</label>
	<br>
	<br>
	<label><span style="color: #cd5b00; font-size: large">&nbsp;&nbsp; &nbsp; ご就業</span></label>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	<input type="radio" name="radio2" id="koumu"value="1" <%if(job==1){ %> checked <%} %>> <label for="koumu">会社員・公務員</label>
	<input type="radio" name="radio2" id="fbaito"value="2" <%if(job==2){ %> checked <%} %>> <label for="fbaito">派遺・アルバイト</label>
	<input type="radio" name="radio2" id="ziei"value="3"<%if(job==3){ %> checked <%} %>> <label for="ziei">自営業</label>
	<input type="radio" name="radio2" id="sengyou"value="4"<%if(job==4){ %> checked <%} %>><label for="sengyou">専業主婦</label>
	<input type="radio" name="radio2" id="gakusei"value="5"<%if(job==5){ %> checked <%} %>> <label for="gakusei">学生</label>
	<br>
	<br>
	<label><span style="color: #cd5b00; font-size: large">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			ご住所&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			〒</span><span
		style="font-size: large"></span> &nbsp;&nbsp;&nbsp;&nbsp;
		<input type="text" name="example2" size="10" value=<%= rc.getuPostal() %>></label>
		<span style="color: #cd5b00; font-size: large">住所&nbsp;&nbsp;&nbsp;</span>
		<input type="text" name="tosi" size="50" value=<%= rc.getuAddress() %>> <br> <br>
		<span style="color: #cd5b00; font-size: large">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;電話番号</span>
		<span style="font-size: large" ></span>
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		&nbsp; &nbsp;<input type="text" name="denwa"value=<%= rc.getuTel() %>> <br> <br>
		<span style="color: #cd5b00; font-size: large">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;連絡用mailアドレス</span>
		<span style="font-size: large" ></span>
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		<input type="text" name="mail"value=<%= rc.getuMail() %>> <br> <br>

	<div align="center"><p>
		<select name="flag">
		<option value="1" selected>会員情報更新</option>
		<option value="2">この会員を退会させる</option>
		<option value="3">会員IDを更新する</option>
		</select>
	</p></div>
<%
	}
%>
<br>
		<INPUT type="button"  onClick="location.href='kaiinkanri.jsp'" value="会員管理画面へ戻る" style="position: absolute; top: 650px; left: 10px; WIDTH: 150px; HEIGHT: 50px;"><br>
		<INPUT type="submit" value="会員情報更新" style="position: absolute; top: 650px; right: 10px; WIDTH: 150px; HEIGHT: 50px;"><br></form>
		<ul>
		<li id = "dare" style="position:absolute;top:570px;left:1200px; WIDTH:150px; HEIGHT: 50px;">
		社員ID:<%=stid %><br>
		<br>
		名前:<%=stname %></li></ul>

</body>
</html>