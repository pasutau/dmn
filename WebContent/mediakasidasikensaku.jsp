<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="java.net.URLDecoder" %>
<%
	Cookie[] cookies = request.getCookies();
	String stid = "";
	String stname = "";
	if(cookies != null){
		for(Cookie cook : cookies){
			if(cook.getName().equals("StId")){
				stid = cook.getValue();
			}
			if(cook.getName().equals("StName")){
				stname = URLDecoder.decode(cook.getValue(),"UTF-8");
			}
		}
	}
%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>メディア貸出履歴検索</title>
	<link rel="stylesheet" type="text/css" href="tenpure.css">
</head>
<body>

	<Div id ="header"><h1>メディア貸出履歴検索</h1></Div><br>

	<form action ="LoanHistory" method = get  align="center">
		<div Align="center"><input type="text" name="mediaId"  placeholder="メディアID" style="position: absolute; left:600px; top:250px;"></div><br>
		<input type="submit" value="検索" name="検索" style="position:absolute;top:450px;left:580px; WIDTH:200px; HEIGHT: 50px;" ><br>
	</form>


	<input type="button" onclick="location.href='kasidasikanri.jsp'" value="貸出へ戻る" name="貸出へ戻る" style="position:absolute;top:600px;left:50px; WIDTH:150px; HEIGHT: 50px;" ><br>
	<ul>
	<li id = "dare">社員ID：<%=stid %><br>
    	名前:<%=stname %><br>
    	</li>
	</ul>
</body>
</html>