<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="java.net.URLDecoder" %>
<%
	Cookie[] cookies = request.getCookies();
	String stid = "";
	String stname = "";
	if(cookies != null){
		for(Cookie cook : cookies){
			if(cook.getName().equals("StId")){
				stid = cook.getValue();
			}
			if(cook.getName().equals("StName")){
				stname = URLDecoder.decode(cook.getValue(),"UTF-8");
			}
		}
	}
%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>返金完了</title>
	<link rel="stylesheet" type="text/css" href="tenpure.css">
</head>
<body>
	 <Div id ="header"><h1>返金完了</h1></Div>

	<!-- 表 -->
	<div style="height:325px; width:1200px; overflow-y:scroll; text-align:center;">
	<table border="1px" cellpadding="6px" align="right" style="border-collapse: collapse;">
	<colgroup>
		<!-- 1列目のグループ -->
		<col span="1" style="background-color: #ffffff;" />
		<!-- 2列目のグループ -->
		<col span="1" style="background-color: #ffffff;" />
		<!-- 3列目のグループ-->
		<col span="1" style="background-color: #ffffff;" />
		<!-- 4列目のグループ-->
		<col span="1" style="background-color: #ffffff;" />
		<!-- 5列目のグループ-->
		<col span="1" style="background-color: #ffffff;" />
	</colgroup>
	<tr bgcolor="#C0C0C0">
		<th  align="center"><span style="margin-right: 150px;"></span>タイトル名<span style="margin-right: 150px;"></span></th>
		<th  align="center"><span style="margin-right: 40px;"></span>貸出日付<span style="margin-right: 40px;"></span></th>
		<th  align="center"><span style="margin-right: 40px;"></span>返却予定日<span style="margin-right: 40px;"></span></th>
		<th  align="center"><span style="margin-right: 40px;"></span>返金日付<span style="margin-right: 40px;"></span></th>
		<th  align="center"><span style="margin-right: 40px;"></span>返金料金<span style="margin-right: 40px;"></span></th>
	</tr>
	<tr>
		<td align="center">ワイルドスピード・NODAMISSION</td>
		<td align="center">7月19日</td>
		<td align="center">7月20日</td>
		<td align="center">7月19日</td>
		<td align="center">260円</td>
	</tr>
	<tr>
		<td align="center"></td>
		<td align="center"></td>
		<td align="center"></td>
		<td align="center"></td>
		<td align="center"></td>
	</tr>
	<tr>
		<td align="center"></td>
		<td align="center"></td>
		<td align="center"></td>
		<td align="center"></td>
		<td align="center"></td>
	</tr>
	<tr>
		<td align="center"></td>
		<td align="center"></td>
		<td align="center"></td>
		<td align="center"></td>
		<td align="center"></td>
	</tr><tr>
		<td align="center"></td>
		<td align="center"></td>
		<td align="center"></td>
		<td align="center"></td>
		<td align="center"></td>
	</tr>
	<tr>
		<td align="center"></td>
		<td align="center"></td>
		<td align="center"></td>
		<td align="center"></td>
		<td align="center"></td>
	</tr>
		<tr>
		<td align="center"></td>
		<td align="center"></td>
		<td align="center"></td>
		<td align="center"></td>
		<td align="center"></td>
	</tr>
		<tr>
		<td align="center"></td>
		<td align="center"></td>
		<td align="center"></td>
		<td align="center"></td>
		<td align="center"></td>
	</tr>
		<tr>
		<td align="center"></td>
		<td align="center"></td>
		<td align="center"></td>
		<td align="center"></td>
		<td align="center"></td>
	</tr>
		<tr>
		<td align="center"></td>
		<td align="center"></td>
		<td align="center"></td>
		<td align="center"></td>
		<td align="center"></td>
	</tr>
	<tr>
		<td align="center"></td>
		<td align="center"></td>
		<td align="center"></td>
		<td align="center"></td>
		<td align="center"></td>
	</tr>
	<tr>
		<td align="center"></td>
		<td align="center"></td>
		<td align="center"></td>
		<td align="center"></td>
		<td align="center"></td>
	</tr>
	<tr>
		<td align="center"></td>
		<td align="center"></td>
		<td align="center"></td>
		<td align="center"></td>
		<td align="center"></td>
	</tr>
	<tr>
		<td align="center"></td>
		<td align="center"></td>
		<td align="center"></td>
		<td align="center"></td>
		<td align="center"></td>
	</tr>
	<tr>
		<td align="center"></td>
		<td align="center"></td>
		<td align="center"></td>
		<td align="center"></td>
		<td align="center"></td>
	</tr>
	<tr>
		<td align="center"></td>
		<td align="center"></td>
		<td align="center"></td>
		<td align="center"></td>
		<td align="center"></td>
	</tr>
	<tr>
		<td align="center"></td>
		<td align="center"></td>
		<td align="center"></td>
		<td align="center"></td>
		<td align="center"></td>
	</tr>
	<tr>
		<td align="center"></td>
		<td align="center"></td>
		<td align="center"></td>
		<td align="center"></td>
		<td align="center"></td>
	</tr>
	<tr>
		<td align="center"></td>
		<td align="center"></td>
		<td align="center"></td>
		<td align="center"></td>
		<td align="center"></td>
	</tr>
	<tr>
		<td align="center"></td>
		<td align="center"></td>
		<td align="center"></td>
		<td align="center"></td>
		<td align="center"></td>
	</tr>
</table>
</div>

<div style="height:20px; width:1180px; text-align:center;">
	<table border="2px" cellpadding="7px" align="right" style="border-collapse: collapse;">
	<colgroup>
		<!-- 1列目のグループ -->
		<col span="1" style="background-color: #ffffff;" />
		<!-- 2列目のグループ -->
		<col span="1" style="background-color: #ffffff;" />
		<tr>
		<td align="center">返金合計金額</td>
		<td align="center">160円</td>
		</tr>
</table>
</div>

	<div align="center"><h3><span style="margin-right: 360px;"></span>この内容で確定しました</h3></div>
	<input type="button"  onclick="location.href='kasidasirirekikensaku.html'" value="OK" name="OK" style="position:absolute;top:540px;left:580px; WIDTH:200px; HEIGHT: 50px;" ><br>

	<ul>
	<li id = "dare">社員ID：<%=stid %><br>
    	名前:<%=stname %><br>
    	</li>
	</ul>
</body>
</html>