<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="java.net.URLDecoder"%>
<%
	Cookie[] cookies = request.getCookies();
	String stid = "";
	String stname = "";
	if (cookies != null) {
		for (Cookie cook : cookies) {
			if (cook.getName().equals("StId")) {
				stid = cook.getValue();
			}
			if (cook.getName().equals("StName")) {
				stname = URLDecoder.decode(cook.getValue(), "UTF-8");
			}
		}
	}
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>貸出メディア受取完了</title>
<link rel="stylesheet" type="text/css" href="tenpure.css">
</head>
<body>
	<Div Align="center">
		<h1>貸出メディア受取完了</h1>
	</Div>
	<Div Align="right">
		<a href="GoodBye" class="square_btn2">ログアウト</a>
	</Div>
	<br>
	<br>
	<br>
	<br>
	<Div Align="center">
		<h1>
			<font color="red">貸出メディアを受け取りました</font>
		</h1>
	</Div>
	<input type="button" onclick="location.href='tenpokanzaiko.jsp'"
		value="店舗間在庫管理へ戻る" name="店舗間在庫管理へ戻る"
		style="position: absolute; top: 600px; left: 50px; WIDTH: 150px; HEIGHT: 50px;">
	<br>

	<ul>
		<li id="dare">社員ID：<%=stid%><br> 名前:<%=stname%><br>
		</li>
	</ul>
</body>
</html>