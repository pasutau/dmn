<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="java.net.URLDecoder" %>
<%@ page import="java.util.*"%>
<%@ page import="bean.*" %>
<%
	Cookie[] cookies = request.getCookies();
	String stid = "";
	String stname = "";
	if(cookies != null){
		for(Cookie cook : cookies){
			if(cook.getName().equals("StId")){
				stid = cook.getValue();
			}
			if(cook.getName().equals("StName")){
				stname = URLDecoder.decode(cook.getValue(),"UTF-8");
			}
		}
	}
%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>貸出履歴一覧</title>
	<link rel="stylesheet" type="text/css" href="tenpure.css">
</head>
<body>
<Div id ="header"><h1>貸出履歴一覧</h1></Div>
<jsp:useBean id="uslist" class="bean.UserRirekiListBean" scope="session"/>
<table border ="1" align="center">
<tr><th>貸出日付</th><th>返却日付</th><th>タイトル</th></tr>
    <%
    ArrayList<UserRirekiRecordBean> ukasiArray =  uslist.getUserRireki();
    for(UserRirekiRecordBean record : ukasiArray){
    %>
    <tr>
        <td><%= record.getKasidasi() %></td>
        <td><%= record.getHenkyaku() %></td>
        <td><%= record.getTitle() %></td>
    </tr>
    <% } %>
</table>

	<INPUT type="button" onclick="location.href='kasidasikanri.jsp'" value="貸出へ戻る" name="貸出へ戻る" style="position:absolute;top:600px;left:50px; WIDTH:150px; HEIGHT: 50px;" ><br>

	<ul>
	<li id = "dare">社員ID：<%=stid %><br>
    	名前:<%=stname %><br>
    	</li>
	</ul>
</body>
</html>