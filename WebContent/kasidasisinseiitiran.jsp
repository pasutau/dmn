<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="java.net.URLDecoder"%>
<%@ page import="bean.LoanBean"%>
<%@ page import="db.DBConnectUtil"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="db.MediaDAO"%>
<%@ page import="bean.LoanapplicationListBean"%>
<%@ page import="java.sql.Connection"%>
<%
	Cookie[] cookies = request.getCookies();
	String stid = "";
	String stname = "";
	if (cookies != null) {
		for (Cookie cook : cookies) {
			if (cook.getName().equals("StId")) {
				stid = cook.getValue();
			}
			if (cook.getName().equals("StName")) {
				stname = URLDecoder.decode(cook.getValue(), "UTF-8");
			}
		}
	}
%>



<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>貸出申請一覧</title>
<link rel="stylesheet" type="text/css" href="tenpure.css">
</head>
<body>
	<%----------------------------------- 申請一覧表を表示しています----------------------------------%>
	<jsp:useBean id="lListbean" class="bean.LoanapplicationListBean"
		scope="session" />
	<Div Align="center">
		<table border="1">
			<tr>
				<th>店舗名</th>
				<th>タイトル名</th>
			</tr>

			<%
				ArrayList<LoanBean> LoanRecordArray = lListbean.getapplicationArray();
				for (LoanBean rcd : LoanRecordArray) {
					String shopname = rcd.getShopname();
					String shouhinname = rcd.getProductname();
					String pid = rcd.getPid();//商品ＩＤ
					int shid = rcd.getShid();//店舗ＩＤ
					int mytenid = rcd.getMytenpoid();//自分の店舗ＩＤをぶっこむ
					int slid = rcd.getSlid();//店舗貸出ＩＤ
			%>
			<tr>
				<td><%=shopname%></td>
				<td><%=shouhinname%></td>
				<td><form action="LoandetailsServlet">
						<input type="hidden" name="slid" value="<%=slid%>">
						<input type="hidden" name="pid" value="<%=pid%>">
						<input type="hidden" name="shid" value="<%=shid%>">
						<input type="hidden" name="mytenid" value="<%=mytenid%>">
						<input type="hidden" name="shopname" value="<%=shopname%>">
						<input type="hidden" name="shouhinname" value="<%=shouhinname%>">
						<input type="submit" value="貸出する">
					</form></td>
			</tr>
			<%
				}
			%>

		</table>
	</Div>


	<INPUT type="button" onclick="location.href='tenpokanzaiko.jsp'"
		value="店舗間在庫管理へ戻る" name="店舗間在庫管理へ戻る"
		style="position: absolute; top: 600px; left: 50px; WIDTH: 150px; HEIGHT: 50px;">
	<br>

	<ul>
		<li id="dare">社員ID：<%=stid%><br> 名前:<%=stname%><br>
		</li>
	</ul>
</body>
</html>