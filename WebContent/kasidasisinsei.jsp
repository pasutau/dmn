<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="java.net.URLDecoder" %>
<%
	Cookie[] cookies = request.getCookies();
	String stid = "";
	String stname = "";
	if(cookies != null  ){
		for(Cookie cook : cookies){
			if(cook.getName().equals("StId")){
				stid = cook.getValue();
			}
			if(cook.getName().equals("StName")){
				stname = URLDecoder.decode(cook.getValue(),"UTF-8");
			}
		}
	}
%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>貸出申請情報入力</title>
	<link rel="stylesheet" type="text/css" href="tenpure.css">
</head>
<body>
	<Div id ="header"><h1>貸出申請情報入力</h1></Div>

	<div align="center"><h3><span style="margin-right: 40px;"></span>DVD:1本<span style="margin-right: 40px;"></span>BD:1本</h3></div>

	<!-- 表 -->
	<div style="height:430px; width:870px; text-align:center;">
	<table border="1px" cellpadding="4px" align="right" style="border-collapse: collapse;">
	<colgroup>
		<!-- 1列目のグループ -->
		<col span="1" style="background-color: #ffffff;" />
		<!-- 2列目のグループ -->
		<col span="1" style="background-color: #ffffff;" />
		<!-- 3列目のグループ -->
		<col span="1" style="background-color: #ffffff;" />
		<!-- 4列目のグループ -->
		<col span="1" style="background-color: #ffffff;" />
	</colgroup>
	<tr>
		<th  align="center" bgcolor="#C0C0C0"><span style="margin-right: 40px;"></span>店舗<span style="margin-right: 40px;"></span></th>
		<th  align="center"  bgcolor="#C0C0C0">DVD申請本数</th>
		<th  align="center"  bgcolor="#C0C0C0">BD申請本数</th>
		<th  align="center"  bgcolor="#C0C0C0">申請日数</th>
	</tr>
		<tr>
		<td align="center">HCS店</td>
		<td align="center">1本</td>
		<td align="center">1本</td>
		<td align="center">2週間</td>
	</tr>
	<tr>
		<td align="center">屯田店</td>
		<td align="center">1本</td>
		<td align="center">1本</td>
		<td align="center">2週間</td>
	</tr>
	<tr>
		<td align="center">新琴似店</td>
		<td align="center">1本</td>
		<td align="center">1本</td>
		<td align="center">2週間</td>
	</tr>
	<tr>
		<td align="center">菊水店</td>
		<td align="center">1本</td>
		<td align="center">1本</td>
		<td align="center">2週間</td>
	</tr>
	<tr>
		<td align="center">東札幌店</td>
		<td align="center">1本</td>
		<td align="center">1本</td>
		<td align="center">2週間</td>
	</tr>
	<tr>
		<td align="center">札幌店</td>
		<td align="center">1本</td>
		<td align="center">1本</td>
		<td align="center">2週間</td>
	</tr>
	<tr>
		<td align="center">大通り店</td>
		<td align="center">1本</td>
		<td align="center">1本</td>
		<td align="center">2週間</td>
	</tr>
	<tr>
		<td align="center">すすきの店</td>
		<td align="center">1本</td>
		<td align="center">1本</td>
		<td align="center">2週間</td>
	</tr>
	<tr>
		<td align="center">白石店</td>
		<td align="center">1本</td>
		<td align="center">1本</td>
		<td align="center">2週間</td>
	</tr>
		<tr>
		<td align="center">豊平店</td>
		<td align="center">1本</td>
		<td align="center">1本</td>
		<td align="center">2週間</td>
	</tr>
</table>
</div>


	<input type="submit"  onclick="location.href='kasidasisinseikakunin.html'" value="貸出要請" name="貸出要請" style="position:absolute;top:580px;left:580px; WIDTH:200px; HEIGHT: 50px;" >

	<INPUT type="button" onclick="location.href='tatenpokasidasi.html'" value="他店舗貸出可能一覧へ戻る" name="他店舗貸出可能一覧へ戻る" style="position:absolute;top:600px;left:50px; WIDTH:175px; HEIGHT: 50px;" ><br>

	<ul>
	<li id = "dare">社員ID：<%=stid %><br>
    	名前:<%=stname %><br>
    	</li>
	</ul>

</body>
</html>