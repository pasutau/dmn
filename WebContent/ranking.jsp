<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="java.net.URLDecoder" %>
<%@ page import = "db.*"%>
<%@ page import ="bean.*" %>
<%@ page import ="java.sql.Connection" %>
<%@ page import ="java.util.*" %>
<%
	Cookie[] cookies = request.getCookies();
	String stid = "";
	String stname = "";
	if(cookies != null){
		for(Cookie cook : cookies){
			if(cook.getName().equals("StId")){
				stid = cook.getValue();
			}
			if(cook.getName().equals("StName")){
				stname = URLDecoder.decode(cook.getValue(),"UTF-8");
			}
		}
	}
%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>ランキング</title>
	<link rel="stylesheet" type="text/css" href="tenpure.css">
</head>
<body>
<jsp:useBean id="ranklist" class="bean.RankingListBean" scope="session"/>
	<Div id ="header"><h1>ランキング</h1></Div>
	<Div Align="right">
		<a href="GoodBye" class="square_btn2">ログアウト</a>
	</Div>
	<!-- 表 -->
<table border ="1" align="center">
<tr><th>順位</th><th>タイトル</th><th>件数</th></tr>
    <%
    ArrayList<RankingRecordBean> rankArray = ranklist.getRanking();
    int cnt = 1;
    for(RankingRecordBean record : rankArray){
    %>
    <tr>
        <td><%= cnt %></td>
        <td><%= record.getTitle() %></td>
        <td><%= record.getKensu() %></td>
        <% cnt++; %>
        <% if(cnt>10){break;} %>
    </tr>
    <% } %>
</table>
	<INPUT type="button" onclick="location.href='settei.jsp'" value="設定へ戻る" name="設定へ戻る" style="position:absolute;top:600px;left:50px; WIDTH:150px; HEIGHT: 50px;" ><br>

	<ul>
	<li id = "dare">社員ID：<%=stid %><br>
    	名前:<%=stname %><br>
    	</li>
	</ul>
</body>
</html>