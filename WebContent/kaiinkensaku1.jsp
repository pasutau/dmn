<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="java.net.URLDecoder" %>
<%
	Cookie[] cookies = request.getCookies();
	String stid = "";
	String stname = "";
	if(cookies != null){
		for(Cookie cook : cookies){
			if(cook.getName().equals("StId")){
				stid = cook.getValue();
			}
			if(cook.getName().equals("StName")){
				stname = URLDecoder.decode(cook.getValue(),"UTF-8");
			}
		}
	}
%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>会員検索入力画面</title>
	<link rel="stylesheet" type="text/css" href="tenpure.css">
</head>
<body>
	<Div id ="header"><h1>会員検索</h1></Div>
	<Div Align="right"><a href="GoodBye" class="square_btn2">ログアウト</a>
	</Div>

	<br>
	<br>
	<br>
	<br>

		<form method ="post" action="MemberKensakuServlet">
		<div style="text-align:center"><h2>会員情報を入力してください</h2></div><br>
		<Div Align="center">
		<table style="border-style: none;" border="1">
		<tr><th style="border-style: none;">会員ID</th><th style="border-style: none;"><input type="text" name="uid"></th></tr>
		<tr><th style="border-style: none;">おなまえ（かな）</th><th style="border-style: none;"><input type="text" name="uname"></th></tr>
		<tr><th style="border-style: none;">生年月日</th><th style="border-style: none;"><input type="text" name="ubirth" placeholder="例：1990-01-01"></th></tr>
		<tr><th style="border-style: none;">電話番号</th><th style="border-style: none;"><input type="text" name="utel" placeholder="ハイフンなし"></th></tr>
		</table>
		</Div>
		<div align="center"><input type="submit" value="検索"></div>
		</form>

		<INPUT type="button" onclick="location.href='tujogyomu.jsp'" value="会員管理へ戻る" name="会員管理へ戻る" style="position:absolute;left:50px;HEIGHT: 50px;"><br>
	<ul>
	<li id = "dare">社員ID：<%=stid %><br>
    	名前:<%=stname %><br>
    	</li>
	</ul>
</body>
</html>