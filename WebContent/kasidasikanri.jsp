<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="java.net.URLDecoder" %>
<%@ page import = "db.*"  %>
<%@ page import = "bean.*" %>
<%@ page import = "dmx.*" %>

<%
	Cookie[] cookies = request.getCookies();
	String stid = "";
	String stname = "";
	if(cookies != null){
		for(Cookie cook : cookies){
			if(cook.getName().equals("StId")){
				stid = cook.getValue();
			}
			if(cook.getName().equals("StName")){
				stname = URLDecoder.decode(cook.getValue(),"UTF-8");
			}
		}
	}
	Authority auth = new Authority();
    boolean kyokaFlg = auth.AuthorityCheck(stid,1/*機能ID*/);//true 使用権限あり false 使用権限なし
    if(kyokaFlg == false){
    	response.sendRedirect("error.jsp");
    }

%>

<%
	KaikeiBean kai = new KaikeiBean();

	kai.setSoeji(0);
%>

<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>貸出管理</title>
	<link rel="stylesheet" type="text/css" href="tenpure.css">
</head>
<body>
	<Div id ="header"><h1>貸出管理</h1></Div>
	<Div Align="right">
		<a href="GoodBye" class="square_btn2">ログアウト</a>
	</Div>

	<!-- メニューボタン -->
	<a href="mediakasidasi.jsp"><img src="./img/kasidasitouroku.PNG" alt="貸出情報登録" style="position:absolute;top:150px;left:580px;"></a>
	<a href="kasidasirirekikensaku.jsp"><img src="./img/kasidasirireki.PNG" alt="貸出履歴検索" style="position:absolute;top:300px;left:580px;"></a>
	<a href="mediakasidasikensaku.jsp"><img src="./img/mediarireki.PNG" alt="メディア履歴表示" style="position:absolute;top:450px;left:580px;"></a>

	<INPUT type="button" onclick="location.href='tujogyomu.jsp'" value="通常業務へ戻る" name="通常業務へ戻る" style="position:absolute;top:600px;left:50px; WIDTH:150px; HEIGHT: 50px;" ><br>

		<li id = "dare" style="position:absolute;top:570px;left:1200px; WIDTH:150px; HEIGHT: 50px;">
		      社員ID:<%=stid %><br>
		<br>
		      名前:<%=stname %>
		</li>
</body>
</html>