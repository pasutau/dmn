<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="java.net.URLDecoder" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="bean.MediaBean" %>
<%
	Cookie[] cookies = request.getCookies();
	String stid = "";
	String stname = "";
	if(cookies != null  ){
		for(Cookie cook : cookies){
			if(cook.getName().equals("StId")){
				stid = cook.getValue();
			}
			if(cook.getName().equals("StName")){
				stname = URLDecoder.decode(cook.getValue(),"UTF-8");
			}
		}
	}
%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>廃棄登録確認</title>
	<link rel="stylesheet" type="text/css" href="tenpure.css">
</head>
<body>
<jsp:useBean id="mdinfoBean" class="bean.MediadisposalinfoBean" scope="session" />

	<Div id ="header"><h1>廃棄登録確認</h1></Div><br>
	<input type="text" name=mediaid placeholder="廃棄日" style="position: absolute; left:600px; top:100px;"><br><br>
<Div Align="center">
<table border="1" >
<tr><th>タイトル</th><th>メディアＩＤ</th>


<%
ArrayList<MediaBean> mdRecordArray
	= mdinfoBean.getMediaRecordArray();

for(MediaBean rcd : mdRecordArray)	{
	out.println("<tr><td>" + rcd.getMedianame() + "</td><td>"
			+ rcd.getmId() + "</td><tr>");
}
%>

</table>
</div>

	<div align="center"><h3>この内容で確定しますか?</h3></div>
	<input type="submit"  onclick="location.href='haikitoroku.jsp'" value="戻る" name="戻る" style="position:absolute;top:560px;left:460px; WIDTH:200px; HEIGHT: 50px;" >
	<input type="submit"  onclick="location.href='haikitourokukanryo.jsp'" value="登録" name="登録" style="position:absolute;top:560px;right:460px; WIDTH:200px; HEIGHT: 50px;" ><br>

	<INPUT type="button" onclick="location.href='zaikokanri.jsp'" value="在庫管理へ戻る" name="在庫管理へ戻る" style="position:absolute;top:600px;left:50px; WIDTH:150px; HEIGHT: 50px;" ><br>

	<ul>
	<li id = "dare">社員ID：<%=stid %><br>
    	名前:<%=stname %><br>
    	</li>
	</ul>

</body>
</html>