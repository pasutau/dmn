<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="java.net.URLDecoder"%>
<%@ page import="bean.LoanBean"%>
<%@ page import="db.DBConnectUtil"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="db.MediaDAO"%>
<%@ page import="bean.LoanapplicationListBean"%>
<%@ page import="java.sql.Connection"%>
<%
	Cookie[] cookies = request.getCookies();
	String stid = "";
	String stname = "";
	if (cookies != null) {
		for (Cookie cook : cookies) {
			if (cook.getName().equals("StId")) {
				stid = cook.getValue();
			}
			if (cook.getName().equals("StName")) {
				stname = URLDecoder.decode(cook.getValue(), "UTF-8");
			}
		}
	}
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>貸出詳細</title>
<link rel="stylesheet" type="text/css" href="tenpure.css">
</head>
<body>
	<Div id="header">
		<h1>貸出詳細</h1>
	</Div>
	<jsp:useBean id="lListbean" class="bean.LoanapplicationListBean"
		scope="session" />
	<Div Align="center">
		<table border="1">
			<tr>
				<th>店舗名</th>
				<th>タイトル名</th>
				<th>メディアＩＤ</th>
			</tr>

			<%
				ArrayList<LoanBean> LoanRecordArray = lListbean.getapplicationArray();
				for (LoanBean rcd : LoanRecordArray) {
					String shopname = rcd.getShopname();
					String shouhinname = rcd.getProductname();
					String pid = rcd.getPid();//商品ＩＤ
					int shid = rcd.getShid();//店舗ＩＤ
					int mytenid = rcd.getMytenpoid();//自分の店舗ＩＤをぶっこむ
					int slid = rcd.getSlid();//貸出申請ＩＤをぶっこむ
			%>
			<tr>
				<td><%=shopname%></td>
				<td><%=shouhinname%></td>
				<td><input type="text" name="mediaid" placeholder="メディアID"></td>
				<td><form action="TenpolendingServlet">
						<input type="hidden" name="slid" value="<%=slid%>"> <input
							type="hidden" name="pid" value="<%=pid%>"> <input
							type="hidden" name="shid" value="<%=shid%>"> <input
							type="hidden" name="mytenid" value="<%=mytenid%>"> <input
							type="hidden" name="shopname" value="<%=shopname%>"> <input
							type="hidden" name="shouhinname" value="<%=shouhinname%>">
						<input type="submit" value="貸出する">
					</form></td>
			</tr>
			<%
				}
			%>

		</table>
	</Div>





	<ul>
		<li id="dare">社員ID：<%=stid%><br> 名前:<%=stname%><br>
		</li>
	</ul>
</body>
</html>