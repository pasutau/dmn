<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="java.net.URLDecoder" %>
<%@ page import="java.util.*" %>
<%@ page import="bean.*" %>
<%
	Cookie[] cookies = request.getCookies();
	String stid = "";
	String stname = "";
	if(cookies != null){
		for(Cookie cook : cookies){
			if(cook.getName().equals("StId")){
				stid = cook.getValue();
			}
			if(cook.getName().equals("StName")){
				stname = URLDecoder.decode(cook.getValue(),"UTF-8");
			}
		}
	}
%>

<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>ブラック会員予備軍一覧</title>
	<link rel = "stylesheet" type = "text/css" href = "tenpure.css" > <!-- CSS定義 -->
	</head>
	<body>
<jsp:useBean id="buraInfoBean" class="bean.BurakkurisutoInfoBean" scope="session" />

	<Div  Align="center"><h1>ブラックリスト会員更新確認</h1></Div>
	<Div Align = "right"><a href = "#" class = "square_btn">ログアウト</a></Div>

<form method="post" action="BurakkurisutoUpdateServlet">
<!-- 表 -->
<Div  Align="center">
<table border="1">

	<colgroup>
		<!-- 1列目のグループ -->
		<col span="1" style="background-color: #ffffff;" />
		<!-- 2列目のグループ -->
		<col span="1" style="background-color: #ffffff;" />
		<!-- 3列目のグループ -->
		<col span="1" style="background-color: #ffffff;" />
		<!-- 4列目のグループ -->
		<col span="1" style="background-color: #ffffff;" />
		<!-- 5列目のグループ -->
		<col span="1" style="background-color: #ffffff;" />
		<!-- 6列目のグループ -->
		<col span="1" style="background-color: #ffffff;" />
	</colgroup>

	<tr bgcolor="#C0C0C0">
		<th  align="center"><span style="margin-right: 10px;"></span>会員ID<span style="margin-right: 10px;"></span></th>
		<th  align="center"><span style="margin-right: 10px;"></span>名前<span style="margin-right: 10px;"></span></th>
		<th  align="center"><span style="margin-right: 10px;"></span>入力者<span style="margin-right: 10px;"></span></th>
		<th  align="center"><span style="margin-right: 10px;"></span>入力日<span style="margin-right: 10px;"></span></th>
		<th  align="center"><span style="margin-right: 10px;"></span>理由<span style="margin-right: 10px;"></span></th>
		<th  align="center"><span style="margin-right: 40px;"></span>状態<span style="margin-right: 40px;"></span></th>
	</tr>
<%
	int count = 1;
	ArrayList<BurakkurisutoBean> buraInfoArray = buraInfoBean.getBurakkuArray();
	session.setAttribute("buraInfoArray", buraInfoArray);
	for(BurakkurisutoBean rc : buraInfoArray) {
		int status = Integer.parseInt(rc.getBstatus());
 %>
	<tr>
		<td align="center"><%= rc.getUid() %></td>
		<td align="center"><% if(status==3) { %> <font color="red"> ☠ <% }%> <%= rc.getUname() %></font></td>
		<td align="center"><%= rc.getStname() %></td>
		<td align="center"><%= rc.getUtime() %></td>
		<td align="center"><font color="red"><%= rc.getReason() %></font>
		<td align="center"><p>
		<select name="status<%= count %>">
		<% if(status==2) { %>
			<option value="2">保留</option>
			<option value="3">追加</option>
			<option value="1">削除</option>
		<% } else {%>
			<option value="2">保留</option>
			<option value="1">削除</option>
		<% }
		count++;%>
		</select><br></td>

	</tr>
<%
	}
%>
</table>
</div>
	<Div Align="center"><input type="button"  onclick="location.href='kainkensakumenu.jsp'" value="会員検索メニューへ" name="会員検索へ戻る"
	style="position:absolute;top:500px;left:100px; WIDTH:200px; HEIGHT: 50px;" ><br></Div>

	<Div Align="center"><input type="button"  onclick="location.href='burakkurisutotouroku.jsp'" value="ブラック会員追加" name="ブラック会員追加"
	style="position:absolute;top:500px;left:400px; WIDTH:200px; HEIGHT: 50px;" ><br></Div>

	<Div Align="center"><input type="submit" value="更新" name="更新"
	style="position:absolute;top:500px;left:700px; WIDTH:200px; HEIGHT: 50px;" ><br></Div></form>
		<ul>
	<li id = "dare">社員ID：<%=stid %><br>
    	名前:<%=stname %><br>
    	</li>
	</ul>
</body>
</html>