<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="java.net.URLDecoder" %>
<%@ page import="java.util.*" %>
<%@ page import="bean.*" %>
<%
	Cookie[] cookies = request.getCookies();
	String stid = "";
	String stname = "";
	if(cookies != null){
		for(Cookie cook : cookies){
			if(cook.getName().equals("StId")){
				stid = cook.getValue();
			}
			if(cook.getName().equals("StName")){
				stname = URLDecoder.decode(cook.getValue(),"UTF-8");
			}
		}
	}
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>会員情報更新</title>
<link rel="stylesheet type=text/css" href="tenpure.css">
</head>
<body>
<jsp:useBean id="memInfoBean" class="bean.MemberInfoBean" scope="session" />
	<Div  Align="center"><h1>会員情報詳細</h1></Div><br>
	<Div  Align="center">
	<table border="1" >
	<%
	ArrayList<MemberBean> memInfoArray = memInfoBean.getMemberArray();
	for(MemberBean rc : memInfoArray) {
		String birth = rc.getuBirthday();
		String[] sps1 = birth.split("-"); /* 2000-01-01 の - で区切る*/
		birth = sps1[0] + "年" + sps1[1]+"月"+sps1[2]+"日";

 	%>
	<%String name = rc.getuName();%>


	<tr><th>会員ID</th><th><%= rc.getuId() %></th></tr>
	<tr><th>会員名</th><th><%= name %></th></tr>
	<tr><th>会員名(かな)</th><th><%= rc.getuNameKana() %></th></tr>
	<tr><th>生年月日</th><th><%= birth  %></th></tr>
	<tr><th>性別</th><th><%=rc.getuGender()  %></th></tr>
	<tr><th>職業</th><th><%=rc.getuJob()%></th></tr>
	<tr><th>住所</th><th><%=rc.getuAddress()%></th></tr>
	<tr><th>電話番号</th><th><%=rc.getuTel()%></th></tr>
	<tr><th>Mail</th><th><%=rc.getuMail()%></th></tr>

<%
	}
%>
</table>
	</Div>
	<br>
	<INPUT type="button" onclick="location.href='kaiinkanri.jsp'" value="会員管理へ戻る" name="会員管理へ戻る" style="position:absolute;left:50px;HEIGHT: 50px;"><br>
	<input type="button"  onclick="location.href='kaiinkousin1.jsp'" value="会員情報更新"
		style="position:absolute;top:400px;left:600px; WIDTH:200px; HEIGHT: 50px;" ><br>
	<ul>
	<li id = "dare">社員ID：<%=stid %><br>
    	名前:<%=stname %><br>
    	</li>
	</ul>

</body>
</html>