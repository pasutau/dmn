<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="java.net.URLDecoder" %>
<%@ page import="java.util.*" %>
<%@ page import = "dmx.*" %>
<%@ page import = "db.*" %>
<%@ page import="bean.*" %>
<%
	Cookie[] cookies = request.getCookies();
	String stid = "";
	String stname = "";
	if(cookies != null){
		for(Cookie cook : cookies){
			if(cook.getName().equals("StId")){
				stid = cook.getValue();
			}
			if(cook.getName().equals("StName")){
				stname = URLDecoder.decode(cook.getValue(),"UTF-8");
			}
		}
	}
	Authority auth = new Authority();
	boolean kyokaFlg = auth.AuthorityCheck(stid,4);
	if(kyokaFlg == false){
		response.sendRedirect("error.jsp");
	}
%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>社員情報更新</title>
	<link rel="stylesheet" type="text/css" href="tenpure.css">
</head>
<body>
<jsp:useBean id="SyainInfoBean" class="bean.SyainInfoBean" scope="session" />
	<Div id ="header"><h1>社員情報更新</h1></Div><br>

<form action = "SyainUpdateServlet" method = "post">
<%
	ArrayList<SyainBean> semInfoArray = SyainInfoBean.getSyainArray();
	for(SyainBean rc : semInfoArray) {
 %>
 	<input type="hidden" name="sId" value=<%= rc.getsId() %>>
	<div Align="center"><h1>社員名:<input type="text" name="sName" value="<%= rc.getsName() %>"></h1></div><br>
	<div Align="center"><h1>社員名(かな):<input type="text" name="sNameKana" value="<%= rc.getsNameKana() %>"></h1></div><br>
	<div Align="center"><h1>電話番号:<input type="text" name="sTel" value="<%=rc.getsTel()%>"></h1></div><br>
	<div Align="center"><h1>住所:<input type="text" name="sAddress" value="<%=rc.getsAddress()%>"></h1></div><br>
	<div Align="center"><h1>職位:<%=rc.getsPossion() %>
									   <select name="sPossion">
									   <option value="店長"selected>店長</option>
									   <option value="社員" >社員</option>
									   <option value="アルバイト">アルバイト</option>
									   </select></h1></div><br>
	<div Align="center"><h1>店舗:<%=rc.getShName()%>
								<select name="sTenpo">
									   <option value="1"selected>HCS店</option>
									   <option value="2" >豊平店</option>
									   <option value="3">屯田店</option>
									   <option value="4">新琴似店</option>
									   <option value="5">菊水店</option>
									   <option value="6">東札幌店</option>
									   <option value="7">札幌店</option>
									   <option value="8">大通り店</option>
									   <option value="9">すすきの店</option>
									   <option value="10">白石店</option>
								</select>
	</h1></div>
	<input type="submit" value="登録する" style="position:absolute;top:560px;left:580px; WIDTH:200px; HEIGHT: 50px;" ><br>
<%
	}
%>

</form>


	<INPUT type="button" onclick="location.href='syainkanri.jsp'" value="社員管理へ戻る" name="社員管理へ戻る" style="position:absolute;top:600px;left:50px; WIDTH:150px; HEIGHT: 50px;" ><br>

	<ul>
	<li id = "dare">社員ID：<%=stid %><br>
    	名前:<%=stname %><br>
    	</li>
	</ul>
</body>
</html>