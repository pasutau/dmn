<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="java.net.URLDecoder" %>
<%@ page import="java.util.*" %>
<%@ page import="bean.*" %>
<%
	Cookie[] cookies = request.getCookies();
	String stid = "";
	String stname = "";
	if(cookies != null){
		for(Cookie cook : cookies){
			if(cook.getName().equals("StId")){
				stid = cook.getValue();
			}
			if(cook.getName().equals("StName")){
				stname = URLDecoder.decode(cook.getValue(),"UTF-8");
			}
		}
	}
%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>会員検索結果一覧</title>
	<link rel = "stylesheet" type = "text/css" href = "tenpure.css" > <!-- CSS定義 -->
</head>
<body>
<jsp:useBean id="memInfoBean" class="bean.MemberInfoBean" scope="session" />

<Div  Align="center"><h1>会員検索結果一覧</h1></Div><br>
<div align="center"><h3><font color="blue">青：失効中の会員  </font><font color="red">赤：退会した会員</font></h3></div>
<Div  Align="center">
<table border="1" >


	<tr>
		<th>会員ID</th>
		<th>会員名</th>
		<th>性別</th>
		<th>生年月日</th>
		<th>メールアドレス</th>
		<th>電話番号</th>
		<th>住所</th>
	</tr>

<%
	ArrayList<MemberBean> memInfoArray = memInfoBean.getMemberArray();
	for(MemberBean rc : memInfoArray) {
		int id = rc.getuId();
		int status = Integer.parseInt(rc.getuStatus());
 %>

	<tr>
		<td><a href="MemberKojinServlet?id=<%= id %>"><%= id %></a></td>
		<td><% if(status==2) { %> <font color="blue">  <% }else if(status==3){ %>
								  <font color="red">  <% }%> <%= rc.getuNameKana() %></font></td>
		<td><%= rc.getuGender() %></td>
		<td><%= rc.getuBirthday() %></td>
		<td><%= rc.getuMail() %></td>
		<td><%= rc.getuTel() %></td>
		<td>〒<%= rc.getuPostal() %>　　<%= rc.getuAddress() %></td>
	</tr>
<%
	}
%>
</table>
</Div>
<br>
	<INPUT type="button" onclick="location.href='tujogyomu.jsp'" value="会員管理へ戻る" name="会員管理へ戻る" style="position:absolute;left:50px;HEIGHT: 50px;"><br>

		<ul>
		<li id = "dare" style="position:absolute;top:570px;left:1200px; WIDTH:150px; HEIGHT: 50px;">
		社員ID:<%=stid %><br>
		<br>
		名前:<%=stname %></li></ul>

</body>
</html>