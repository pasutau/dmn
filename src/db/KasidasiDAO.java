package db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;

import bean.KasiRirekiBean;
import bean.KasiRirekiListBean;
import bean.UserRirekiListBean;
import bean.UserRirekiRecordBean;

public class KasidasiDAO {
	PreparedStatement pst = null;
	DBConnectUtil dbc = new DBConnectUtil();

	// メディア名取得
	public String mediaMeiShutoku(Connection con, String mediaId) throws SQLException {
		String mediaMei = "";
		System.out.println("かしだしＤＡＯのmediaId=" + mediaId);
		ResultSet rs = null;
		try {
			pst = con.prepareStatement("SELECT p_name FROM product,media"
									+ " WHERE product.p_id = media.p_id"
									+ " AND media.m_id = '" + mediaId + "'");
			rs = pst.executeQuery();
			while (rs.next()) {
				mediaMei = rs.getString("p_name");
				System.out.println("メディア名取得mediaMei = " + mediaMei);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		rs.close();
		return mediaMei;
	}

	// 料金取得
	public int ryokinShutoku(Connection con, String mediaId, String hi) throws SQLException {
		int ryokin = 0;
		System.out.println("かしだしＤＡＯ料金のmediaId=" + mediaId);
		System.out.println("かしだしＤＡＯ料金のhi=" + hi);
		ResultSet rs = null;
		try {
			pst = con.prepareStatement(
					"SELECT rf_amount" + " FROM product,media,newness,rental_fee" + " WHERE product.p_id = media.p_id"
							+ " AND product.n_id = newness.n_id" + " AND newness.n_id = rental_fee.n_id"
							+ " AND media.m_id = " + "'" + mediaId + "'" + " AND rf_days = " + "'" + hi + "'");

			rs = pst.executeQuery();
			while (rs.next()) {
				ryokin = rs.getInt("rf_amount");
				System.out.println("syntaxの部分のryokin=" + ryokin);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		rs.close();
		System.out.println(mediaId + "のryokin=" + ryokin);
		return ryokin;
	}

	// 貸出情報登録
	// 型ばらばらだから注意
	public void lendTouroku(Connection con, int uId, String nowdate, int stid, int rid, int gokei, int siyousu) {
		int num = 0;
		try {
			Statement stmt = con.createStatement();
			String sql = "INSERT INTO lend VALUES( L_ID_SEQ.NEXTVAL ," + uId + " , TO_DATE('" + nowdate
					+ "','yyyy/MM/dd HH24:mi:ss'), " + stid + " , " + rid + " , " + gokei + " , " + siyousu + " )";
			System.out
					.println(uId + "," + nowdate + "," + stid + "," + rid + "," + gokei + "," + siyousu + "を入れてるはず...");
			num = stmt.executeUpdate(sql);
			stmt.close();
			// まだコミットしてないからね
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println(num + "件追加");
	}

	// 貸出情報登録(詳細)
	public void lendInfTouroku(Connection con, String mediaId, String newdate) throws SQLException {
		int num = 0;
		int LId = 0;

		ResultSet rs = null;
		// l_idをlendから取得する
		try {
			pst = con.prepareStatement("SELECT * FROM lend WHERE l_id IN (SELECT MAX(l_id) FROM lend)");
			rs = pst.executeQuery();
			while (rs.next()) {
				LId = rs.getInt("l_id");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		// l_infoにinsertする
		try {
			Statement stmt = con.createStatement();
			String sql = "INSERT INTO l_information VALUES(" + LId + ",'" + mediaId + "',TO_DATE('" + newdate
					+ "','yyyy/MM/dd HH24:mi:ss')," + null + ")";
			num = stmt.executeUpdate(sql);
			System.out.println(LId + "," + mediaId + "," + newdate + "が入ってるはず");
			stmt.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		// mediaの更新(貸出状態へ
		try {
			Statement stmt = con.createStatement();
			// m_frag 1:貸出中 2:貸出可
			String sql = "UPDATE media SET m_flag = 1 WHERE m_id='" + mediaId + "'";
			num = stmt.executeUpdate(sql);
			stmt.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/* 返却処理  false=延滞*/
	public int henkyaku(Connection con, String mediaId, String sqlDate) throws SQLException {
		int entaicnt = 0;
		int num = 0;// 更新件数もっとくだけのゴミ箱
		// l_infoの貸出状態更新

		//延滞者かどうか調べる
		try{
			ResultSet rs = null;
			entaicnt = 0;
			Date niti = null;//日付入れる

			Statement stmt = con.createStatement();
			String sql = "SELECT l_id,li_plan_time FROM l_information"
					  + " WHERE m_id = '" + mediaId + "'"
					  + " AND LI_TIME IS NULL AND (LI_PLAN_TIME < TO_DATE('"+ sqlDate +"','yyyy/MM/dd HH24:mi:ss'))";
			//SQLおかしい？
			rs = stmt.executeQuery(sql);
			while(rs.next()){
				niti = rs.getDate(2);
				/**
				 * 2つの日付の差分日数を算出するプログラムです。
				 */
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
				Date dateTo = sdf.parse(sqlDate);
				Date dateFrom = niti;

				// 日付をlong値に変換します。
				long dateTimeTo = dateTo.getTime();
				long dateTimeFrom = dateFrom.getTime();

				// 差分の日数を算出します。
				long dayDiff = ( dateTimeTo - dateTimeFrom  ) / (1000 * 60 * 60 * 24 );
				int entainissu = (int)dayDiff;

				System.out.println( "日数(FROM今日) : " + sdf.format(dateFrom) );
				System.out.println( "日数(TO返すはずの日) : " + sdf.format(dateTo) );
				System.out.println( "差分日数(延滞日数) : " + dayDiff );

				entaicnt += entainissu;
			}


			if(entaicnt > 0){
				System.out.println("お前は延滞者だ");
			}
			stmt.close();
		}catch(Exception e){
			e.printStackTrace();
		}

		try {
			Statement stmt = con.createStatement();
			// 最新のデータに日付入れたい
			String sql = "UPDATE l_information SET li_time = TO_DATE('" + sqlDate + "','yyyy/MM/dd HH24:mi:ss')"
					+ " WHERE li_time IS null" + " AND m_id = '" + mediaId + "'";
			num = stmt.executeUpdate(sql);
			stmt.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		// mediaの更新(貸出可能状態へ
		try {
			Statement stmt = con.createStatement();
			// m_frag 1:貸出中 2:貸出可
			String sql = "UPDATE media SET m_flag = 2 WHERE m_id='" + mediaId + "'";
			num = stmt.executeUpdate(sql);
			stmt.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return entaicnt;
	}

	/* メディア貸出履歴 */
	public KasiRirekiListBean mediaKasiRireki(Connection con, String mediaId) throws SQLException {

		KasiRirekiListBean kasilist = new KasiRirekiListBean();

		ResultSet rs = null;
		try {
			System.out.println("KasidasiDao.mediaId=" + mediaId);
			pst = con.prepareStatement("SELECT users.U_ID, users.U_NAME, li_plan_time"
					+ " FROM l_information, lend, users" + " WHERE l_information.m_id ='" + mediaId + "'"
					+ " AND l_information.l_id = lend.l_id" + " AND lend.U_ID = users.U_ID" + " ORDER BY 3 DESC");
			rs = pst.executeQuery();

			while (rs.next()) {
				KasiRirekiBean rireki = new KasiRirekiBean();
				rireki.setuId(rs.getInt("u_id"));
				rireki.setuName(rs.getString("u_name"));
				rireki.setTime(rs.getDate("li_plan_time"));
				kasilist.addKasiRireki(rireki);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		rs.close();
		return kasilist;
	}

	/* 貸出履歴(誰が何借りたかの履歴) */
	public UserRirekiListBean userKasiRireki(Connection con, int userId) throws SQLException {

		UserRirekiListBean ukasilist = new UserRirekiListBean();

		ResultSet rs = null;
		try {
			System.out.println("KasidasiDao.userId=" + userId);
			pst = con.prepareStatement("SELECT lend_time, li_time, p_name"
					+ " FROM l_information, lend, users, media, product" + " WHERE lend.u_id = " + userId
					+ " AND l_information.l_id = lend.l_id" + " AND l_information.m_id = media.m_id"
					+ " AND media.p_id = product.p_id" + " AND lend.u_id = users.u_id" + " ORDER BY 1 DESC");
			rs = pst.executeQuery();

			// デバッグ用
			// while(rs.next()){
			// System.out.println(rs.getDate("lend_time"));
			// System.out.println(rs.getDate("li_time"));
			// System.out.println(rs.getString("p_name"));
			// }

			while (rs.next()) {
				UserRirekiRecordBean rireki = new UserRirekiRecordBean();
				rireki.setKasidasi(rs.getDate("lend_time"));
				rireki.setHenkyaku(rs.getDate("li_time"));
				rireki.setTitle(rs.getString("p_name"));
				ukasilist.addUserRireki(rireki);
			}

			// デバッグ用
			// ArrayList<UserRirekiRecordBean> ukasiArray =
			// ukasilist.getUserRireki();
			// for(UserRirekiRecordBean record : ukasiArray){
			// System.out.println(record.getKasidasi());
			// System.out.println(record.getHenkyaku());
			// System.out.println(record.getTitle());
			// }

		} catch (Exception e) {
			e.printStackTrace();
		}
		return ukasilist;
	}

}