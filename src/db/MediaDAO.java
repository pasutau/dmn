package db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;

import bean.LoanBean;
import bean.LoanapplicationListBean;
import bean.MediaBean;
import bean.MediadisposalinfoBean;

public class MediaDAO {
	PreparedStatement pst = null;
	DBConnectUtil dbc = new DBConnectUtil();
	MediaBean mdBean = new MediaBean();
	LoanBean loanBean = new LoanBean();
	MediadisposalinfoBean mdinfoBean = new MediadisposalinfoBean();

	/////////////////////////////////////////////////////////////////////////////////////////////////////////
	// 廃棄するDVDの情報をとってくる//
	public String haikimediaPull(Connection con, String mediaId) throws SQLException {
		String medianame = "";
		ResultSet rs = null;
		try {
			pst = con.prepareStatement("SELECT P_NAME FROM PRODUCT,MEDIA " + " WHERE PRODUCT.P_ID = MEDIA.P_ID"
					+ " AND MEDIA.M_ID = " + "'" + mediaId + "'");
			rs = pst.executeQuery();
			while (rs.next()) {
				medianame = rs.getString("p_name");
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return medianame;
	}

	// 廃棄するDVDの情報を表に追加していく//
	public void haikiInsert(Connection con, java.sql.Date nowtime, String mediaid) {
		int num = 0;
		// try {
		// Statement stmt = con.createStatement();
		// String sql = "INSERT INTO disposal VALUES('" + mediaid + "','" +
		// nowtime + "')";
		//
		// // 店舗別メディア数を変更する
		//
		// num = stmt.executeUpdate(sql);
		// System.out.println(num + "件追加");
		// stmt.close();
		// } catch (Exception e) {
		// e.printStackTrace();
		// }
		try {
			Statement stmt = con.createStatement();
			String sql = "UPDATE media SET m_flag = 3 WHERE m_id= '" + mediaid + "'";
			num = stmt.executeUpdate(sql);
			System.out.println(num + "件あぷでしたよ");
			stmt.close();
			con.commit();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////////
	/* 店舗間在庫管理(貸出申請一覧) */
	/* 自店舗ＩＤをもってきている */
	public int tenpoidPull(Connection con, int syainID) throws SQLException {
		int mytenpoid = 0;
		ResultSet rs = null;
		try {
			String sql = "SELECT sh_id FROM staff WHERE " + syainID + "=st_id";// 自分の所属している店舗ＩＤをとってくるよ
			pst = con.prepareStatement(sql);
			rs = pst.executeQuery();
			while (rs.next())
				mytenpoid = rs.getInt("sh_id");// 自分の店舗ＩＤをmytenpoidにいれてる
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mytenpoid;
	}

	/* 自店舗名をもってきている */
	public String tenponamePull(Connection con, int tenpoid) throws SQLException {
		String mytenponame = null;
		ResultSet rs = null;
		try {
			String sql = "SELECT sh_name FROM shop WHERE " + tenpoid + "=sh_id";// 自分の所属している店舗名をとってくるよ
			pst = con.prepareStatement(sql);
			rs = pst.executeQuery();
			while (rs.next())
				mytenponame = rs.getString("sh_name");// 自分の店舗名をmytenponameにいれてる
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mytenponame;
	}

	/* 商品名をもってきている */
	public String productnamePull(Connection con, String p_id) throws SQLException {
		String product = null;
		ResultSet rs = null;
		try {
			String sql = "SELECT p_name FROM product WHERE " + "'" + p_id + "'" + "=p_id";// 商品名をとってきているよ
			pst = con.prepareStatement(sql);
			rs = pst.executeQuery();
			product = rs.getString("p_name");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return product;
	}

	/* 貸出申請一覧表示するために。。。 （修正必要ゴミ） ぼくに貸してっていってきてる一覧 */// こいつの中がみすってる
	public LoanapplicationListBean getLoanList(Connection con, int mytenid) {
		ResultSet rs = null;
		ResultSet rs2 = null;
		ResultSet rs3 = null;
		LoanapplicationListBean lList = new LoanapplicationListBean();

		try {
			// どの商品をもってきてるのおおおお
			String shohinID = "";
			int tenpoID = 0;
			// 貸してってきてる申請表の全部をもってくる
			String sql = "SELECT * FROM s_lending WHERE sl_sh_id = " + mytenid + "AND sl_status = 1";
			pst = con.prepareStatement(sql);
			rs = pst.executeQuery();
			while (rs.next()) {
				LoanBean lbean = new LoanBean();
				lbean.setSlid(rs.getInt("sl_id"));// 店舗貸出ＩＤをセット
				shohinID = rs.getString("p_id");// 変数に要請がきてる商品ＩＤをセット
				tenpoID = rs.getInt("sh_id");// 変数に要請がきてる店舗ＩＤをセット

				lbean.setMytenpoid(mytenid);
				lbean.setShid(rs.getInt("sh_id"));// 貸してってきてる店舗ＩＤをセット

				// うちに貸してってきてる商品の名前をセレクト
				String sql2 = "SELECT p_name FROM product WHERE p_id = " + "'" + shohinID + "'";
				pst = con.prepareStatement(sql2);
				rs2 = pst.executeQuery();
				while (rs2.next()) {// セレクトした結果は一件のはず
					lbean.setPid(shohinID);// やり取りするようの商品ＩＤも一応セットしとく
					lbean.setProductname(rs2.getString("p_name"));// 商品名をbeanにセット

				}
				// うちに貸してってきてる店舗名をセレクト
				String sql3 = "SELECT sh_name FROM shop WHERE sh_id = " + tenpoID;
				pst = con.prepareStatement(sql3);
				rs3 = pst.executeQuery();
				while (rs3.next()) {// セレクトした結果は一件のはず
					lbean.setShid(tenpoID);// やり取りするようの店舗ＩＤをセット
					lbean.setShopname(rs3.getString("sh_name"));// 貸してってきてる店舗名をもってきてる

				}
				lList.addMediainfo(lbean);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			dbc.disconnect(con);
		}
		return lList;
	}

	/*
	 * 店舗間在庫管理、貸出申請一覧からの処理 貸出終了予定日、貸出開始日、ステータス をアップデート
	 */
	public void slendingUpdate(LoanBean loanbean) {
		Connection con = DBConnectUtil.connect();
		try {

			String sql = "UPDATE s_lending SET sl_time = TO_DATE(?,'yyyy/MM/dd HH24:mi:ss'), "
					+ "sl_plan_time = TO_DATE(?,'yyyy/MM/dd HH24:mi:ss'),sl_status = ? "
					+ "WHERE sl_status = '1' AND sl_id = ? ";

			pst = con.prepareStatement(sql);
			pst.setString(1, loanbean.getLenstartday());
			pst.setString(2, loanbean.getPlanTime());
			pst.setString(3, loanbean.getStatus());
			pst.setInt(4, loanbean.getSlid());
			pst.executeUpdate();
			pst.close();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			dbc.commit(con);
			dbc.disconnect(con);
		}
		return;
	}

	/* 店舗間在庫管理（自店舗貸出中一覧を表示） */

	public LoanapplicationListBean zitenpoLoanpull(Connection con, int mytenid) {
		SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		ResultSet rs = null;
		ResultSet rs2 = null;
		ResultSet rs3 = null;
		LoanapplicationListBean lList = new LoanapplicationListBean();
		try {

			String shohinID = "";
			int taisyotenpoID = 0;

			String sql = "SELECT * FROM s_lending WHERE sl_sh_id =" + mytenid
					+ "AND (sl_status = '4' OR sl_status = '5')";
			pst = con.prepareStatement(sql);
			rs = pst.executeQuery();
			while (rs.next()) {
				LoanBean lbean = new LoanBean();
				lbean.setStatus(rs.getString("sl_status"));// ステータスをせっと
				int hansta = Integer.parseInt(lbean.getStatus());// ステータスをセット（４：貸出中、５：返却中)

				// 貸出中 OR 返送中のメッセージをセット
				if (hansta == 4) {
					lbean.setStatusname("貸出中");
				} else
					lbean.setStatusname("返却中");

				lbean.setLenstartday(format.format(rs.getDate("sl_time")));// 貸出開始日をセット
				lbean.setSlid(rs.getInt("sl_id"));// 店舗貸出ＩＤをセット
				shohinID = rs.getString("p_id");// 変数に貸出中の商品ＩＤ
				taisyotenpoID = rs.getInt("sh_id");// 変数に貸出中の店舗ＩＤ
				lbean.setMytenpoid(mytenid);// 自分の店舗ＩＤをセット
				lbean.setShid(rs.getInt("sh_id"));// 貸出中の店舗ＩＤをセット

				// 他店舗に貸出中の商品名をセレクト
				String sql2 = "SELECT p_name FROM product WHERE p_id = " + "'" + shohinID + "'";
				pst = con.prepareStatement(sql2);
				rs2 = pst.executeQuery();
				while (rs2.next()) {// セレクトした結果は一件のはず
					lbean.setPid(shohinID);// やり取りするようの商品ＩＤも一応セットしとく
					lbean.setProductname(rs2.getString("p_name"));// 商品名をbeanにセット

				}
				// 他店舗に貸出中の店舗名をセレクト
				String sql3 = "SELECT sh_name FROM shop WHERE sh_id = " + taisyotenpoID;
				pst = con.prepareStatement(sql3);
				rs3 = pst.executeQuery();
				while (rs3.next()) {// セレクトした結果は一件のはず
					lbean.setShid(taisyotenpoID);// やり取りするようの貸出中の店舗ＩＤをセット
					lbean.setShopname(rs3.getString("sh_name"));// 貸出中の店舗名をもってきてる

				}
				lList.addMediainfo(lbean);
				System.out.println(lList.getArraySize());
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			dbc.disconnect(con);
		}
		return lList;
	}

	/* 店舗間在庫管理貸出中一覧にて。。。(貸出申請詳細にて、貸出終了予定日、ステータスが4のもの をアップデート */
	public void slenendUpdate(LoanBean loanbean) {
		Connection con = DBConnectUtil.connect();
		/*
		 * System.out.println(loanbean.getLenendTime());
		 * System.out.println(loanbean.getStatus());
		 * System.out.println(loanbean.getMediaid());
		 */// 値は三つとってこれてる
		try {
			String sql = "UPDATE s_lending SET sl_end_time = TO_DATE(?,'yyyy/MM/dd HH24:mi:ss'),sl_status = ? WHERE sl_status = '5' AND sl_id IN (SELECT sl_id FROM sl_detail WHERE m_id = ? )";
			pst = con.prepareStatement(sql);
			pst.setString(1, loanbean.getLenendTime());
			pst.setString(2, loanbean.getStatus());
			pst.setString(3, loanbean.getMediaid());
			pst.executeUpdate();
			pst.close();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			dbc.commit(con);
			dbc.disconnect(con);
		}
		return;
	}

	// 自店舗借入中一覧をとってきている//
	public LoanapplicationListBean zitenpoBorrowing(Connection con, int mytenid) {
		SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		ResultSet rs = null;
		ResultSet rs2 = null;
		ResultSet rs3 = null;
		LoanapplicationListBean lList = new LoanapplicationListBean();
		try {

			String shohinID = "";
			int zibuntenpoID = 0;

			String sql = "SELECT * FROM s_lending WHERE sh_id =" + mytenid + "AND sl_status = '4'";
			pst = con.prepareStatement(sql);
			rs = pst.executeQuery();
			while (rs.next()) {
				LoanBean lbean = new LoanBean();
				lbean.setLenstartday(format.format(rs.getDate("sl_time")));// 借入開始日をセット
				lbean.setSlid(rs.getInt("sl_id"));// 店舗貸出ＩＤをセット
				shohinID = rs.getString("p_id");// 変数に借入中の商品ＩＤ
				zibuntenpoID = rs.getInt("sh_id");// 変数に自分の店舗ＩＤ
				lbean.setMytenpoid(mytenid);// 自分の店舗ＩＤをセット
				lbean.setShid(rs.getInt("sl_sh_id"));// 貸してもらってる相手の店舗ＩＤをセット

				// 他店舗に借入中の商品名をセレクト
				String sql2 = "SELECT p_name FROM product WHERE p_id = " + "'" + shohinID + "'";
				pst = con.prepareStatement(sql2);
				rs2 = pst.executeQuery();
				while (rs2.next()) {// セレクトした結果は一件のはず
					lbean.setPid(shohinID);// やり取りするようの商品ＩＤも一応セットしとく
					lbean.setProductname(rs2.getString("p_name"));// 商品名をbeanにセット

				}
				// 他店舗に借入中の店舗名をセレクト
				String sql3 = "SELECT sh_name FROM shop WHERE sh_id = " + zibuntenpoID;
				pst = con.prepareStatement(sql3);
				rs3 = pst.executeQuery();
				while (rs3.next()) {// セレクトした結果は一件のはず
					lbean.setShid(zibuntenpoID);// やり取りするようの貸出中の店舗ＩＤをセット
					lbean.setShopname(rs3.getString("sh_name"));// 借入中の店舗名をもってきてる

				}
				lList.addMediainfo(lbean);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			dbc.disconnect(con);
		}
		return lList;
	}

	/* 店舗間在庫管理借入中一覧にて。。。(貸出申請詳細にて、貸出終了予定日、ステータスが4のもの をアップデート */
	public void s_borrowingUpdate(LoanBean loanbean) {
		Connection con = DBConnectUtil.connect();

		try {
			String sql = "UPDATE s_lending SET sl_status = ? WHERE sl_status = '4' AND sl_id IN (SELECT sl_id FROM sl_detail WHERE m_id = ? )";
			pst = con.prepareStatement(sql);
			System.out.println(loanbean.getStatus());
			pst.setString(1, loanbean.getStatus());
			pst.setString(2, loanbean.getMediaid());
			pst.executeUpdate();
			pst.close();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			dbc.commit(con);
			dbc.disconnect(con);
		}
		return;
	}
}
