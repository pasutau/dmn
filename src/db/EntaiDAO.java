package db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import bean.EntaiBean;
import bean.EntaiInfoBean;

public class EntaiDAO {
	DBConnectUtil dbc = new DBConnectUtil();
	private Statement stmt;
	PreparedStatement ps= null;
	public void EntaiAdd(int lid, String sDate) {
		// TODO 自動生成されたメソッド・スタブ
		Connection con = DBConnectUtil.connect();
		try{
			String sql = "INSERT INTO D_CONTACT VALUES(?,TO_DATE(?,'yyyy/MM/dd HH24:mi:ss'),?,?)";
			ps = con.prepareStatement(sql);
			ps.setInt(1,lid);
			ps.setString(2,sDate);
			ps.setString(3,"");
			ps.setString(4,"1");
			ps.executeUpdate();
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			dbc.commit(con);
			dbc.disconnect(con);
		}
		return;
		}


	public EntaiInfoBean getNewEntaiHuman(String strNextDate) {
		// TODO 自動生成されたメソッド・スタブ
		Connection con = DBConnectUtil.connect();
		EntaiInfoBean eib = new EntaiInfoBean();
		ResultSet rs = null;
		String sql = "SELECT L_ID FROM L_INFORMATION "
				+ " WHERE LI_TIME IS NULL AND (LI_PLAN_TIME < TO_DATE('"+strNextDate+"'))  "
				+ " AND L_ID NOT IN(SELECT L_ID FROM D_CONTACT) ORDER BY 1";
		try{
			stmt = con.createStatement();
			rs = stmt.executeQuery(sql);
			while(rs.next()){
				EntaiBean eb = new EntaiBean();
				eb.setLid(rs.getInt("L_ID"));
				eib.addEntaiRecord(eb);
			}
		}catch(SQLException e){
			e.printStackTrace();
		}finally{
			dbc.disconnect(con);
		}
		return eib;
	}


	public EntaiInfoBean getEntaiInfoBean() {
		// TODO 自動生成されたメソッド・スタブ
		Connection con = DBConnectUtil.connect();
		EntaiInfoBean eib = new EntaiInfoBean();
		ResultSet rs = null;
		String sql = "SELECT D_CONTACT.L_ID, USERS.U_ID, USERS.U_NAME, USERS.U_TEL, D_CONTACT.DC_FLAG "
				+ " FROM LEND, D_CONTACT, USERS"
				+ " WHERE D_CONTACT.L_ID = LEND.L_ID AND LEND.U_ID = USERS.U_ID AND DC_FLAG <> 3 "
				+ " ORDER BY 1,5";
		try{
			stmt = con.createStatement();
			rs = stmt.executeQuery(sql);
			while(rs.next()){
				EntaiBean eb = new EntaiBean();
				eb.setLid(rs.getInt(1));
				eb.setUid(rs.getInt(2));
				eb.setUname(rs.getString(3));
				eb.setuTel(rs.getString(4));
				eb.setDcflag(rs.getString(5));
				eib.addEntaiRecord(eb);
			}
		}catch(SQLException e){
			e.printStackTrace();
		}finally{
			dbc.disconnect(con);
		}
		return eib;
	}


	public void EntaiUpdate(int lid, String flag,String stid, int uid) {
		// TODO 自動生成されたメソッド・スタブ
			// TODO 自動生成されたメソッド・スタブ
			Connection con = DBConnectUtil.connect();
			try{
				String sql = "UPDATE (SELECT d.L_ID, d.ST_ID, d.DC_FLAG FROM D_CONTACT d, LEND l WHERE d.L_ID = l.L_ID AND l.U_ID = ?)"
						+ " SET DC_FLAG = ?, ST_ID = ? ";
				//String sql = "UPDATE D_CONTACT d SET  DC_FLAG = ?, ST_ID = ? WHERE L_ID = ? AND U_ID";
				ps = con.prepareStatement(sql);
				ps.setInt(1, uid);
				ps.setString(2,flag);
				ps.setInt(3,Integer.parseInt(stid));
				//ps.setInt(4,lid);
				ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			dbc.commit(con);
			dbc.disconnect(con);
		}
		return ;
	}


	public void EntaiEndUpdate(int lid) {
		// TODO 自動生成されたメソッド・スタブ
		Connection con = DBConnectUtil.connect();
		try{
			String sql = "UPDATE D_CONTACT SET DC_FLAG = ? WHERE L_ID = ?";
			ps = con.prepareStatement(sql);
			ps.setString(1,"3");
			ps.setInt(2, lid);
			ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			dbc.commit(con);
			dbc.disconnect(con);
		}
		return ;
	}


	public EntaiInfoBean getEntaiUpdateHuman() {
		// TODO 自動生成されたメソッド・スタブ
		Connection con = DBConnectUtil.connect();
		EntaiInfoBean eib = new EntaiInfoBean();
		ResultSet rs = null;
		String sql = "SELECT D_CONTACT.L_ID  FROM D_CONTACT, L_INFORMATION"
				+ " WHERE D_CONTACT.L_ID = L_INFORMATION.L_ID AND L_INFORMATION.LI_TIME IS NOT NULL AND DC_FLAG <> 3 "
				+ " AND L_INFORMATION.L_ID NOT IN(SELECT L_ID FROM L_INFORMATION WHERE LI_TIME IS NULL) "
				+ " ORDER BY 1";
		try{
			stmt = con.createStatement();
			rs = stmt.executeQuery(sql);
			while(rs.next()){
				EntaiBean eb = new EntaiBean();
				eb.setLid(rs.getInt(1));
				eib.addEntaiRecord(eb);
			}
		}catch(SQLException e){
			e.printStackTrace();
		}finally{
			dbc.disconnect(con);
		}
		return eib;
	}

}
