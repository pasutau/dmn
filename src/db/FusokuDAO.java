package db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import bean.FusokuListBean;
import bean.FusokuRecordBean;
import bean.MediaListBean;
import bean.MediaTenpoRecordBean;

public class FusokuDAO {
	PreparedStatement pst = null;
	DBConnectUtil dbc = new DBConnectUtil();
	MediaDAO mddao = new MediaDAO();

	// 不足数取得
	// できてねーぞこれ
	public FusokuListBean FusokuList(Connection con, int stId) throws SQLException {

		FusokuListBean fusokulist = new FusokuListBean();
		try {
			ResultSet rs = null;
			// 今もってる在庫数出してjavaで判定しよう
			pst = con.prepareStatement("SELECT product.p_id,p_name,m_category,m_flag,COUNT(*)" + " FROM media,product"
					+ " WHERE media.P_ID = product.p_id" + " AND sh_id = " + stId
					+ " GROUP BY product.p_id,p_name,m_category,m_flag");
			rs = pst.executeQuery();
			while (rs.next()) {
				FusokuRecordBean rc = new FusokuRecordBean();
				rc.setpId(rs.getString("p_id"));
				rc.setpName(rs.getString("p_name"));
				rc.setmCategory(rs.getString("m_category"));
				rc.setKubun(rs.getInt("m_flag"));
				rc.setSu(rs.getInt("COUNT(*)"));
				fusokulist.addFusokuList(rc);
			}
			rs.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		pst.close();
		return fusokulist;
	}

	/* 他店舗貸出可能一覧 */
	public MediaListBean TenpoKasidasi(String pId, String pKubun, int stid) throws SQLException {
		Connection con = DBConnectUtil.connect();
		PreparedStatement ps = null;
		ResultSet rs = null;
		MediaListBean Medialist = new MediaListBean();
		MediaTenpoRecordBean mtRecordbean = new MediaTenpoRecordBean();
		try {
			String sql = "SELECT SHOP.SH_ID,SHOP.SH_NAME, MEDIA.M_CATEGORY, COUNT(*)  AS msu"
					+ " FROM PRODUCT,SHOP,MEDIA "
					+ " WHERE MEDIA.P_ID=PRODUCT.P_ID AND MEDIA.SH_ID=SHOP.SH_ID AND M_FLAG = 2"
					+ " AND MEDIA.P_ID=? AND MEDIA.M_CATEGORY=?" + " GROUP BY SHOP.SH_ID,SHOP.SH_NAME,MEDIA.M_CATEGORY";
			ps = con.prepareStatement(sql);
			ps.setString(1, pId);
			ps.setString(2, pKubun);
			rs = ps.executeQuery();
			while (rs.next())
				;
			mtRecordbean.setShId(rs.getInt("sh_id"));
			mtRecordbean.setShName(rs.getString("SH_NAME"));
			mtRecordbean.setSu(rs.getInt("msu"));
			mtRecordbean.setMgory(rs.getString("M_CATEGORY"));
			mtRecordbean.setPid(rs.getString("p_id"));
			mtRecordbean.setStatus("1");
			mtRecordbean.setZitenpoID(mddao.tenpoidPull(con, stid));

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			dbc.disconnect(con);
		}

		rs.close();
		return Medialist;
	}

	/* 他店舗貸出申請 */
	/*
	 * public Bean TenpoSinsei() throws SQLException { Connection con =
	 * DBConnectUtil.connect(); PreparedStatement ps = null; Bean = new Bean();
	 *
	 * try { String sql = ""; ps = con.prepareStatement(sql);
	 * ps.executeUpdate(); } catch (Exception e) { e.printStackTrace(); }
	 * finally { doc.disconnect(con); } return; }
	 */
}