package db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import bean.ProductBean;

public class TitleTuikaDAO {

	PreparedStatement ps= null;
	DBConnectUtil dbc = new DBConnectUtil();

	/* タイトル追加 */
	public void addTitleInfo(ProductBean pr)throws SQLException {
		Connection con = DBConnectUtil.connect();

		try{
			String sql = "INSERT INTO product VALUES(?,?,?,?,TO_DATE(?,'yyyy/MM/dd HH24:mi:ss'),?,?,?,?,?)";
			ps = con.prepareStatement(sql);

			ps.setString(1, pr.getpId());
			ps.setString(2, pr.getpName());
			ps.setString(3, pr.getpNameKana());
			ps.setString(4, pr.getRecordingTime());
			ps.setString(5, pr.getpStartTime());
			ps.setString(6, pr.getnId());
			ps.setString(7, pr.getStarring());
			ps.setString(8, pr.getStarringKana());
			ps.setString(9, pr.getSuperVision());
			ps.setString(10, pr.getSupervisionKana());

			ps.executeUpdate();
			dbc.commit(con);
			ps.close();
			con.close();
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			dbc.disconnect(con);
		}
		return;
	}


	/*media追加*/
	public void addMedia(String mid,String category,int shid,String pid)
									throws SQLException {
		Connection con = DBConnectUtil.connect();

		String sql = "INSERT INTO media VALUES('"+ mid +"','" + category + "','" + shid +"','" + pid + "',2)";
		ps = con.prepareStatement(sql);
		ps.executeUpdate();
		dbc.commit(con);
		ps.close();
		con.close();
		dbc.disconnect(con);
	}

}