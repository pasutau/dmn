package db;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class ReadKey {
	public String readString() {
	InputStreamReader inStream = new InputStreamReader(System.in);
	BufferedReader	  inBuf    = new BufferedReader(inStream);
	String			  inStr	   = "";

	try {
		inStr = inBuf.readLine();
	} catch (IOException e) {
		System.out.println(e);
		}
	return inStr;
	}


	String readString(String msg) {
		System.out.println(msg);
		return readString();
	}


		public int readInt() {
			int intVal = 0;
			String inStr = "";

			while (true) {
				inStr = readString();
				try {
					intVal = Integer.parseInt(inStr);
					break;
				} catch (NumberFormatException e) {
					System.out.println("error" + inStr);
				}
			}
			return intVal;
		}

		int readInt(String msg) {
			System.out.println(msg);
			return readInt();
		}

		double readDouble() {
			double dblVal = 0;
			String inStr = "";

			while (true) {
				inStr = readString();
				try {
					dblVal = Double.parseDouble(inStr);
					break;
				} catch (NumberFormatException e) {
					System.out.println("���͒l�Ɍ�肪����܂�:" + inStr);
					System.out.print("�ē���>");
				}
		}
		return dblVal;
	}

	double readDouble(String msg) {
		System.out.print(msg);
		return readDouble();
	}
}
