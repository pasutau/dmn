package db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;

import bean.DayprocessingBean;
import bean.EntaiBean;
import bean.RegisterBean;
import bean.RegisterInfo;
import bean.TighteningBean;


//種銭入力//
public class RegisterDAO {
	PreparedStatement ps= null;
	DBConnectUtil dbc = new DBConnectUtil();
	private Statement stmt;

	public void addRegister(RegisterBean reg) {

		Connection con = DBConnectUtil.connect();

		try{
			String sql = "UPDATE REGISTER SET  Man= ?,Gosen= ?,Nisen= ?,Sen= ?,Gohyaku= ?,Hyaku= ?,Gozyu= ?,Zyu= ?,Go= ?,Iti= ?,Sougoukei=? WHERE REGI_ID=?";
			ps = con.prepareStatement(sql);
			ps.setInt(1,reg.getMan());
			ps.setInt(2,reg.getGosen());
			ps.setInt(3,reg.getNisen());
			ps.setInt(4,reg.getSen());
			ps.setInt(5,reg.getGohyaku());
			ps.setInt(6,reg.getHyaku());
			ps.setInt(7,reg.getGozyu());
			ps.setInt(8,reg.getZyu());
			ps.setInt(9,reg.getGo());
			ps.setInt(10,reg.getIti());
			ps.setInt(11,reg.getSougoukei());
			ps.setInt(12,reg.getRegiId());

			ps.executeUpdate();
			ps.close();
		}catch(SQLException e){
			e.printStackTrace();
		}finally{
			dbc.disconnect(con);
		}
		return;
	}

	// 金種入力 //


	public void addTightening(TighteningBean tight){
		PreparedStatement ps= null;
		Connection con = DBConnectUtil.connect();

		try{
			String sql = "INSERT INTO R_TIGHTENING (Regi_Id=?,Rt_Day=?,Rt_Time=?,Rt_Sales=?,Rt_Amount=?,Rt_LateFee=?,Rt_RefundAmount=?,Rt_Admission=?,Rt_Difference=?)"
					+ "SELECT RtSales"
					+ "FROM LEND"
					+"WHERE R_TIGHTENING.Regi_Id=LEND.Regi_Id"
					+ "AND LEND.LEND_TIME BETWEEN RtDay AND RtTime"
					+ "AND LEND.LEND_FEE=R_Tightening.RtSales";
					ps = con.prepareStatement(sql);
			ps.setInt(1,tight.getRegiId());
			ps.setString(2,tight.getRtDay());
			ps.setString(3,tight.getRtTime());
			ps.setInt(4,tight.getRtSales());
			ps.setInt(5,tight.getRtAmount());
			ps.setInt(6,tight.getRtLateFee());
			ps.setInt(7,tight.getRtRefundAmount());
			ps.setInt(8,tight.getRtAdmission());
			ps.setInt(9,tight.getRtDifference());

			ps.executeUpdate();
			ps.close();
			}catch(SQLException e){
			e.printStackTrace();
		}finally{
			dbc.disconnect(con);

		}
		return;
	}





 //日時処理 //


	public void addDayprocessing(DayprocessingBean dyp) {

		Connection con = DBConnectUtil.connect();

		try{
			String sql = "INSERT INTO DAY_PROCESSING VALUES(?,TO_DATE(?,'yyyy/MM/dd HH24:mi:ss'),?,?,?,?,?,?)";
			ps = con.prepareStatement(sql);
			ps.setInt(1,dyp.getShId());
			ps.setString(2,dyp.getDpTime());
			ps.setInt(3,dyp.getDpSales());
			ps.setInt(4,dyp.getDpAmount());
			ps.setInt(5,dyp.getDpLateFee());
			ps.setInt(6,dyp.getDpRefundAmount());
			ps.setInt(7,dyp.getDpAdmission());
			ps.setInt(8,dyp.getProfitAndLoss());




			ps.executeUpdate();
			ps.close();
		}catch(SQLException e){
			e.printStackTrace();
		}finally{
			dbc.disconnect(con);
		}
		return;
	}

	public int stIdShutoku(String rId) {
		// TODO 自動生成されたメソッド・スタブ
		Connection con = DBConnectUtil.connect();
		int stId = 0;
		int regiId = Integer.parseInt(rId);
		ResultSet rs = null;
		PreparedStatement pst = null;
		try{
			pst = con.prepareStatement("SELECT sh_id FROM register"
									+ " WHERE regi_id = " + regiId);
			rs = pst.executeQuery();
			while(rs.next()){
				stId = rs.getInt("sh_id");
			}
			pst.close();
			rs.close();
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			dbc.disconnect(con);
		}
		return stId;
	}

	public RegisterInfo getSales(int regiId, String rtTime) {
		// TODO 自動生成されたメソッド・スタブ
		Connection con = DBConnectUtil.connect();
		ResultSet rs = null;
		RegisterInfo ri = new RegisterInfo();
		String sql = "SELECT SUM(LEND_FEE), SUM(DISCOUNT) FROM LEND l, R_TIGHTENING r "
				  + " WHERE l.REGI_ID=r.REGI_ID AND (LEND_TIME <= TO_DATE('"+rtTime+"','yyyy/MM/dd HH24:mi:ss') "
				  + " AND r.RT_DAY <= LEND_TIME) AND l.REGI_ID = '"+regiId+"'";
		try{
			stmt = con.createStatement();
			rs = stmt.executeQuery(sql);
			while(rs.next()){
				RegisterBean eb = new RegisterBean();
				eb.setRsales(rs.getInt(1));
				eb.setRcount(rs.getInt(2));
				ri.addRegiRecord(eb);
			}
		}catch(SQLException e){
			e.printStackTrace();
		}finally{
			dbc.disconnect(con);
		}
		return ri;
	}

	public int getAdmision(String rtTime,int regiId) {
		// TODO 自動生成されたメソッド・スタブ
		Connection con = DBConnectUtil.connect();
		ResultSet rs = null;
		int admission=0;
		String sql = " SELECT COUNT(U_ID) FROM USERS u, R_TIGHTENING r"
				   + " WHERE u.REGI_ID = r.REGI_ID AND r.REGI_ID = '"+regiId+"'"
				   + " AND (u.U_START_TIME <= TO_DATE('"+rtTime+"','yyyy/MM/dd HH24:mi:ss') AND r.RT_DAY <= u.U_START_TIME) ";
		try{
			stmt = con.createStatement();
			rs = stmt.executeQuery(sql);
			while(rs.next()){
				admission = rs.getInt(1);
			}
		}catch(SQLException e){
			e.printStackTrace();
		}finally{
			dbc.disconnect(con);
		}
		return admission;
	}

	public int getLateFee(String rtTime, int regiId, Date date) {
		// TODO 自動生成されたメソッド・スタブ
		Connection con = DBConnectUtil.connect();
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		ResultSet rs = null;
		int latefee=0;


		try{
			String sql = " SELECT LI_PLAN_TIME FROM LEND le, L_INFORMATION l,  R_TIGHTENING r "
					   + " WHERE le.REGI_ID = r.REGI_ID AND le.L_ID = l.L_ID AND r.REGI_ID = '"+regiId+"'"
					   + " AND LI_TIME IS NOT NULL AND (l.LI_TIME <= TO_DATE('"+rtTime+"','yyyy/MM/dd HH24:mi:ss') AND r.RT_DAY <= l.LI_TIME)  ";
			stmt = con.createStatement();
			rs = stmt.executeQuery(sql);
			while(rs.next()){
				String od = rs.getString(1).substring(0, 10);
				Date date1 = format.parse(od);
				long dateq = date1.getTime();

				String str2 = format.format(date);
				Date date2 = format.parse(str2);
				long datew = date2.getTime();

				long dayDiff = ( datew - dateq  ) / (1000 * 60 * 60 * 24 );
				latefee = latefee + (int)dayDiff;
			}
		}catch (Exception e) {
			e.printStackTrace();
		}finally{
			dbc.commit(con);
			dbc.disconnect(con);
		}
		return latefee;
	}

	public int addHenkin(String rtTime, int regiId, String mid) {
		// TODO 自動生成されたメソッド・スタブ
		Connection con = DBConnectUtil.connect();
		ResultSet rs = null;
		PreparedStatement pst = null;
		int amount=0;
		int num = 0;
		int lid = 0;
		try{
			String sql = " SELECT  l.LEND_FEE, MAX(li.L_ID) FROM LEND l, L_INFORMATION li"
					   + " WHERE l.L_ID = li.L_ID AND li.M_ID = '"+mid+"' ";
			pst = con.prepareStatement(sql);
			rs = pst.executeQuery();
			while (rs.next()) {
				amount = rs.getInt(1);
				lid = rs.getInt(2);
			}
		}catch (Exception e) {
			e.printStackTrace();
		}finally{
			dbc.commit(con);
		}
		try {
			Statement stmt = con.createStatement();
			String sql = "INSERT INTO REFUND VALUES(" + lid + ", TO_DATE('" + rtTime+ "','yyyy/MM/dd HH24:mi:ss')," + amount + ",'"+regiId+"')";
			num = stmt.executeUpdate(sql);

			stmt.close();
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			dbc.commit(con);
			dbc.disconnect(con);
		}
		return amount;
	}

	public int getRefund(String rtTime, int regiId) {
		// TODO 自動生成されたメソッド・スタブ
		Connection con = DBConnectUtil.connect();
		ResultSet rs = null;
		int refund=0;
		String sql = " SELECT SUM(REFUND_AMOUNT) FROM refund re, R_TIGHTENING r"
				   + " WHERE re.REGI_ID = r.REGI_ID AND r.REGI_ID = '"+regiId+"'"
				   + " AND (re.R_TIME <= TO_DATE('"+rtTime+"','yyyy/MM/dd HH24:mi:ss') AND r.RT_DAY <= re.R_TIME) ";
		try{
			stmt = con.createStatement();
			rs = stmt.executeQuery(sql);
			while(rs.next()){
				refund = rs.getInt(1);
			}
		}catch(SQLException e){
			e.printStackTrace();
		}finally{
			dbc.disconnect(con);
		}
		return refund;
	}

	public void addRezisime(int rid, String rttime, int sales, int amount, int fee, int rtRefundAmount, int admission, int difference, int rcnt) {
		// TODO 自動生成されたメソッド・スタブ
		Connection con = DBConnectUtil.connect();

		try{
			String sql = "INSERT INTO R_TIGHTENING VALUES(?,TO_DATE(?,'yyyy/MM/dd HH24:mi:ss'),?,?,?,?,?,?,?)";
			ps = con.prepareStatement(sql);
			ps.setInt(1,rid);
			ps.setString(2,rttime);
			ps.setInt(3,sales);
			ps.setInt(4,amount);
			ps.setInt(5,fee);
			ps.setInt(6,rtRefundAmount);
			ps.setInt(7,admission);
			ps.setInt(8,difference);
			ps.setInt(9, rcnt);
			ps.executeUpdate();
			ps.close();
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			dbc.commit(con);
			dbc.disconnect(con);
		}
		return;
	}

	public void addSyusen(String rtTime, int sougoukei, int rid) {
		// TODO 自動生成されたメソッド・スタブ
		Connection con = DBConnectUtil.connect();
		int num = 0;
		try {
			Statement stmt = con.createStatement();
			String sql = "INSERT INTO R_TIGHTENING VALUES("+rid+",TO_DATE('" + rtTime+ "','yyyy/MM/dd HH24:mi:ss'),'',"+sougoukei+" ,'','','','','1')";
			num = stmt.executeUpdate(sql);

			stmt.close();
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			dbc.commit(con);
			dbc.disconnect(con);
		}
		return;
	}

	public int OldDate(int regiId,int cnt, String rtime) {
		// TODO 自動生成されたメソッド・スタブ
		Connection con = DBConnectUtil.connect();
		ResultSet rs = null;
		int amo =0;
		String sql = "SELECT RT_AMOUNT  FROM R_TIGHTENING WHERE REGI_ID = '"+regiId+"' AND RT_COUNT = "+cnt+" AND  RT_DAY = TO_DATE('"+rtime+"','yyyy/MM/dd HH24:mi:ss')";
		try{
			stmt = con.createStatement();
			rs = stmt.executeQuery(sql);
			while(rs.next()){
				amo = rs.getInt(1);
			}
		}catch(SQLException e){
			e.printStackTrace();
		}finally{
			dbc.disconnect(con);
		}
		return amo;
	}

	public int getCont(String rtime, int regiId) {
		// TODO 自動生成されたメソッド・スタブ
		Connection con = DBConnectUtil.connect();
		ResultSet rs = null;
		int cnt=0;
		String sql = "SELECT RT_COUNT FROM R_TIGHTENING WHERE REGI_ID = '"+regiId+"' AND  RT_DAY = TO_DATE('"+rtime+"','yyyy/MM/dd HH24:mi:ss')";
		try{
			stmt = con.createStatement();
			rs = stmt.executeQuery(sql);
			while(rs.next()){
				cnt = rs.getInt(1);
			}
		}catch(SQLException e){
			e.printStackTrace();
		}finally{
			dbc.disconnect(con);
		}
		return cnt;
	}

	public String getSeleTime(int rid) {
		// TODO 自動生成されたメソッド・スタブ
		String day="";
		Connection con = DBConnectUtil.connect();
		ResultSet rs = null;
		int cnt=0;
		String sql = "SELECT MAX(RT_DAY)  FROM R_TIGHTENING WHERE REGI_ID = '"+rid+"'";
		try{
			stmt = con.createStatement();
			rs = stmt.executeQuery(sql);
			while(rs.next()){
				day = rs.getString(1);
			}
		}catch(SQLException e){
			e.printStackTrace();
		}finally{
			dbc.disconnect(con);
		}

		return day;
	}
}





