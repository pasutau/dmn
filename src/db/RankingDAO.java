package db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import bean.RankingListBean;
import bean.RankingRecordBean;

public class RankingDAO {
	PreparedStatement pst = null;
	DBConnectUtil dbc = new DBConnectUtil();

	//ランキング取得
	public RankingListBean ranking(Connection con)throws SQLException{


		//リストのインポート
		RankingListBean Ranking = new RankingListBean();


		ResultSet rs = null;
		try{
			pst = con.prepareStatement("SELECT p_name,COUNT(*) FROM product,media,l_information"
									+ " WHERE product.p_id = media.p_id"
									+ " AND media.m_id = l_information.m_id"
									+ " GROUP BY p_name"
									+ " ORDER BY 2 DESC ");
			rs = pst.executeQuery();
			while(rs.next()){
				RankingRecordBean record = new RankingRecordBean();
				record.setTitle(rs.getString("p_name"));
				record.setKensu(rs.getInt(2));
				Ranking.addRanking(record);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		pst.close();
		rs.close();

		System.out.println(Ranking);
		return Ranking;
	}

//	//料金取得
//	public int ryokinShutoku(Connection con,String mediaId,String hi)throws SQLException{
//		int ryokin = 0;
//		System.out.println("かしだしＤＡＯ料金のmediaId=" + mediaId);
//		System.out.println("かしだしＤＡＯ料金のhi=" + hi);
//		ResultSet rs = null;
//		try{
//			//ここでSyntaxErrorはいてるくさい
//			pst = con.prepareStatement("SELECT rf_amount"
//									+ " FROM product,media,newness,rental_fee"
//									+ " WHERE product.p_id = media.p_id"
//									+ " AND product.n_id = newness.n_id"
//									+ " AND newness.n_id = rental_fee.n_id"
//									+ " AND media.m_id = " + "'" + mediaId + "'"
//									+ " AND rf_days = " + "'" + hi + "'");
//
//			rs = pst.executeQuery();
//			while(rs.next()){
//				ryokin = rs.getInt("rf_amount");
//				System.out.println("syntaxの部分のryokin="+ryokin);
//			}
//		}catch(Exception e){
//			e.printStackTrace();
//		}
//
//		System.out.println(mediaId + "のryokin=" +ryokin);
//		return ryokin;
//	}

}
