package db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;

import bean.BurakkurisutoBean;
import bean.BurakkurisutoInfoBean;

public class BurakkuDAO {
	DBConnectUtil dbc = new DBConnectUtil();
	private Statement stmt;
	PreparedStatement ps= null;
	public BurakkurisutoInfoBean getBurakkurisutoInfoBean() {
		// TODO 自動生成されたメソッド・スタブ
		Connection con = DBConnectUtil.connect();
		BurakkurisutoInfoBean burakkuinfobean = new BurakkurisutoInfoBean();
		ResultSet rs = null;
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		String sql = "SELECT B_USER.U_ID,USERS.U_NAME,B_USER.U_TIME,STAFF.ST_NAME,B_USER.REASON,B_USER.B_STATUS "
				+ "FROM B_USER,USERS,STAFF "
				+ "WHERE B_USER.U_ID = USERS.U_ID AND B_USER.ST_ID = STAFF.ST_ID  AND B_STATUS <> 1 "
				+ "ORDER BY 6,3,1";
		try{
			stmt = con.createStatement();
			rs = stmt.executeQuery(sql);
			while(rs.next()){
				BurakkurisutoBean rc = new BurakkurisutoBean();
				rc.setUid(rs.getInt(1));
				rc.setUname(rs.getString(2));
				rc.setUtime(format.format(rs.getDate(3)));
				rc.setStname(rs.getString(4));
				rc.setReason(rs.getString(5));
				rc.setBstatus(rs.getString(6));
				burakkuinfobean.addBurakkuRecord(rc);
			}
		}catch(SQLException e){
			e.printStackTrace();
		}finally{
			dbc.disconnect(con);
		}
		return burakkuinfobean;
	}
	public void addBurakkuInfo(BurakkurisutoBean burabean) {
		// TODO 自動生成されたメソッド・スタブ
		Connection con = DBConnectUtil.connect();

		try{
			String sql = "INSERT INTO B_USER VALUES(?,TO_DATE(?,'yyyy/MM/dd HH24:mi:ss'),?,?,?)";
			ps = con.prepareStatement(sql);
			ps.setInt(1,burabean.getUid());
			ps.setString(2,burabean.getUtime());
			ps.setInt(3,burabean.getStid());
			ps.setString(4,burabean.getReason());
			ps.setString(5,burabean.getBstatus());
			ps.executeUpdate();
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			dbc.commit(con);
			dbc.disconnect(con);
		}
		return;
	}
	public void BurakkurisutoUpdate(int i, String parameter) {
		// TODO 自動生成されたメソッド・スタブ
		Connection con = DBConnectUtil.connect();
		try{
			String sql = "UPDATE B_USER SET b_status = ? WHERE u_id = ? ";
			ps = con.prepareStatement(sql);
			ps.setString(1,parameter);
			ps.setInt(2,i);
			ps.executeUpdate();
	} catch (SQLException e) {
		e.printStackTrace();
	} finally {
		dbc.commit(con);
		dbc.disconnect(con);
	}
	return ;
	}

	//わじまんこきたないBIG MAGNUM
}
