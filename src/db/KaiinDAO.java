package db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import bean.MemberBean;
import bean.MemberInfoBean;

public class KaiinDAO {

	PreparedStatement ps= null;
	DBConnectUtil dbc = new DBConnectUtil();
	private Statement stmt;

	/* 会員登録 */
	public void addMemberInfo(MemberBean mem,String rid)throws SQLException {
		// TODO 自動生成されたメソッド・スタブ
		Connection con = DBConnectUtil.connect();

		try{
			String sql = "INSERT INTO USERS VALUES((SELECT NVL(MAX(u_id),0)+1 FROM USERS),?,?,?,?,?,?,?,?,?,TO_DATE(?,'yyyy/MM/dd HH24:mi:ss'),?,?)";
			ps = con.prepareStatement(sql);
			ps.setString(1,mem.getuBirthday());
			ps.setString(2, mem.getuName());
			ps.setString(3, mem.getuNameKana());
			ps.setString(4,mem.getuGender());
			ps.setString(5,mem.getuPostal());
			ps.setString(6,mem.getuAddress());
			ps.setString(7,mem.getuTel());
			ps.setString(8,mem.getuMail());
			ps.setString(9,mem.getuJob());
			ps.setString(10,mem.getuStartTime());
			ps.setString(11,mem.getuStatus());
			ps.setString(12,rid);
			ps.executeUpdate();
			ps.close();
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			dbc.commit(con);
			dbc.disconnect(con);
		}
		return;
	}

	/*会員検索*/
	public MemberInfoBean getMemberInfoBean(String uname, String utel, String ubirth, String uid) {
		// TODO 自動生成されたメソッド・スタブ
		MemberInfoBean memberinfobean = new MemberInfoBean();
		Connection con = DBConnectUtil.connect();
		ResultSet rs = null;
		String sql = "SELECT * FROM USERS";
		int i = 0;
		if( !(uid == null || uid.length() == 0)){
			sql = sql + " WHERE U_ID = '" + uid + "'";
			i++;
		}
		if( !(uname == null || uname.length() == 0)){
			if(i>0){
				sql = sql + " AND U_NAME_KANA LIKE  '%" + uname + "%'";
			}else{
				sql = sql + " WHERE U_NAME_KANA LIKE '%" + uname + "%'";
				i++;
			}
		}
		if( !(ubirth == null || ubirth.length() == 0) ){
			if(i>0){
				sql = sql + "AND U_BIRTHDAY = '" + ubirth + "'";
			}else{
				sql = sql + " WHERE U_BIRTHDAY = '" + ubirth + "'";
				i++;
			}

		}
		if( !(utel == null || utel.length() == 0)){
			if(i>0){
				sql = sql + "AND U_TEL = " + utel;
			}else{
				sql = sql + " WHERE U_TEL = " + utel;
			}

		}
		sql = sql + " ORDER BY U_ID";

		try{
			stmt = con.createStatement();
			rs = stmt.executeQuery(sql);
			while(rs.next()){
				MemberBean rc = new MemberBean();
				rc.setuId(rs.getInt("U_ID"));
				rc.setuName(rs.getString("U_NAME"));
				rc.setuBirthday(rs.getString("U_BIRTHDAY"));
				rc.setuNameKana(rs.getString("U_NAME_KANA"));
				rc.setuGender(rs.getString("U_GENDER"));
				rc.setuPostal(rs.getString("U_POSTAL"));
				rc.setuAddress(rs.getString("U_ADDRESS"));
				rc.setuTel(rs.getString("U_TEL"));
				rc.setuMail(rs.getString("U_MAIL"));
				rc.setuJob(rs.getString("U_JOB"));
				rc.setuStatus(rs.getString("U_STATUS"));
				memberinfobean.addMemberRecord(rc);
			}
		}catch(SQLException e){
			e.printStackTrace();
		}finally{
			dbc.commit(con);
			dbc.disconnect(con);
		}
		return memberinfobean;
	}

	/*会員検索（ID指定Ver）*/
	public MemberInfoBean getMemberKojinInfoBean(String id) {
		// TODO 自動生成されたメソッド・スタブ
		MemberInfoBean memberinfobean = new MemberInfoBean();
		Connection con = DBConnectUtil.connect();
		ResultSet rs = null;
		try{
			String sql = "SELECT * FROM USERS WHERE U_ID = ? ";
			ps = con.prepareStatement(sql);
			ps.setString(1, id);
			rs = ps.executeQuery();
			while(rs.next()){
				MemberBean rc = new MemberBean();
				rc.setuId(rs.getInt("U_ID"));
				rc.setuName(rs.getString("U_NAME"));
				rc.setuBirthday(rs.getString("U_BIRTHDAY"));
				rc.setuNameKana(rs.getString("U_NAME_KANA"));
				rc.setuGender(rs.getString("U_GENDER"));
				rc.setuPostal(rs.getString("U_POSTAL"));
				rc.setuAddress(rs.getString("U_ADDRESS"));
				rc.setuTel(rs.getString("U_TEL"));
				rc.setuMail(rs.getString("U_MAIL"));
				rc.setuJob(rs.getString("U_JOB"));
				rc.setuStatus(rs.getString("U_STATUS"));
				memberinfobean.addMemberRecord(rc);
			}
		}catch(SQLException e){
			e.printStackTrace();
		}finally{
			dbc.disconnect(con);
		}

		return memberinfobean;
	}

	/*会員更新*/
	public void updateMemberInfo(MemberBean mem) {
		// TODO 自動生成されたメソッド・スタブ
		Connection con = DBConnectUtil.connect();
		try{
		String sql = "UPDATE USERS SET u_birthday = ?, u_name = ?, u_name_kana = ?, u_gender = ?, u_job = ?, u_postal = ?, "
				+ "u_address = ?, u_tel = ?, u_mail = ? WHERE u_id = ? ";
		ps = con.prepareStatement(sql);
		ps.setString(1,mem.getuBirthday());
		ps.setString(2,mem.getuName());
		ps.setString(3,mem.getuNameKana());
		ps.setString(4,mem.getuGender());
		ps.setString(5,mem.getuJob());
		ps.setString(6,mem.getuPostal());
		ps.setString(7,mem.getuAddress());
		ps.setString(8,mem.getuTel());
		ps.setString(9,mem.getuMail());
		ps.setInt(10,mem.getuId());
		ps.executeUpdate();

	} catch (SQLException e) {
		e.printStackTrace();
	} finally {
		dbc.commit(con);
		dbc.disconnect(con);
	}
	return ;
	}

	public MemberInfoBean MemberZyoutaiUpdate()throws SQLException  {
				// TODO 自動生成されたメソッド・スタブ
				Connection con = DBConnectUtil.connect();
				MemberInfoBean memberinfobean = new MemberInfoBean();
				ResultSet rs = null;
				String sql = "SELECT * FROM USERS WHERE U_STATUS != 3";
				try{
					stmt = con.createStatement();
					rs = stmt.executeQuery(sql);
					while(rs.next()){
						MemberBean rc = new MemberBean();
						rc.setuId(rs.getInt("U_ID"));
						rc.setuStartTime(rs.getString("U_START_TIME"));
						rc.setuStatus(rs.getString("U_STATUS"));
						memberinfobean.addMemberRecord(rc);
					}
				} catch (SQLException e) {
					e.printStackTrace();
				} finally {
					dbc.disconnect(con);
					}


				return memberinfobean;
				}

			public void MemberZyoutaiUpdate(int id,String st) {
				// TODO 自動生成されたメソッド・スタブ
				Connection con = DBConnectUtil.connect();
				try{
				String sql = "UPDATE USERS SET U_STATUS = ? WHERE U_ID = ?";
				ps = con.prepareStatement(sql);
				ps.setString(1,st);
				ps.setInt(2, id);
				ps.executeUpdate();
				ps.close();
				} catch (SQLException e) {
					e.printStackTrace();
				} finally {
					dbc.commit(con);
					dbc.disconnect(con);
				}
				return ;
			}

			public void addNewMember(MemberBean mem, String sttime,String rid) {
				// TODO 自動生成されたメソッド・スタブ
				Connection con = DBConnectUtil.connect();

				try{
					String sql = "INSERT INTO USERS VALUES((SELECT NVL(MAX(u_id),0)+1 FROM USERS),?,?,?,?,?,?,?,?,?,TO_DATE(?,'yyyy/MM/dd HH24:mi:ss'),?,?)";
					ps = con.prepareStatement(sql);
					ps.setString(1,mem.getuBirthday());
					ps.setString(2, mem.getuName());
					ps.setString(3, mem.getuNameKana());
					ps.setString(4,mem.getuGender());
					ps.setString(5,mem.getuPostal());
					ps.setString(6,mem.getuAddress());
					ps.setString(7,mem.getuTel());
					ps.setString(8,mem.getuMail());
					ps.setString(9,mem.getuJob());
					ps.setString(10,sttime);
					ps.setString(11,"1");
					ps.setString(12,rid);
					ps.executeUpdate();
					ps.close();
				}catch(Exception e){
					e.printStackTrace();
				}finally{
					dbc.commit(con);
					dbc.disconnect(con);
				}
				return;
			}

			public void addUpdateNewMember(int uId, String sttime) {
				// TODO 自動生成されたメソッド・スタブ
				Connection con = DBConnectUtil.connect();
				ResultSet rs = null;
				PreparedStatement pst = null;
				int uid =0;
				int num = 0;
				try {
					pst = con.prepareStatement("SELECT MAX(u_id) FROM USERS");
					rs = pst.executeQuery();
					while (rs.next()) {
						uid = rs.getInt(1);
					}
				} catch (Exception e) {
					e.printStackTrace();
				}finally{
					dbc.commit(con);
				}

				try {
					Statement stmt = con.createStatement();
					String sql = "INSERT INTO updated_history VALUES(" + uid + ",TO_DATE('" + sttime+ "','yyyy/MM/dd HH24:mi:ss')," + uId + ")";
					num = stmt.executeUpdate(sql);

					stmt.close();
				} catch (Exception e) {
					e.printStackTrace();
				}finally{
					dbc.commit(con);
					dbc.disconnect(con);
				}
			}
}
