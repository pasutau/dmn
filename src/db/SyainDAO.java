package db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import bean.SyainBean;
import bean.SyainInfoBean;

public class SyainDAO {

	PreparedStatement ps = null;
	DBConnectUtil dbc = new DBConnectUtil();

	/* 社員登録 */
	public void addSyainInfo(SyainBean sem) {
		Connection con = DBConnectUtil.connect();

		try{
			String sql = "INSERT INTO staff VALUES((SELECT NVL(MAX(st_id),0)+1 FROM STAFF),?,?,?,?,?,?,TO_DATE(?,'yyyy/MM/dd HH24:mi:ss'),?)";
			ps = con.prepareStatement(sql);
			ps.setString(1,sem.getsName());
			ps.setString(2,sem.getsNameKana());
			ps.setString(3,sem.getsTenpo());
			ps.setString(4,sem.getsTel());
			ps.setString(5,sem.getsAddress());
			ps.setString(6,sem.getsPossion());
			ps.setString(7,sem.getsStartTime());
			ps.setString(8,null);
			ps.executeUpdate();
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			dbc.disconnect(con);
		}
		return;
	}


	/*社員検索（ID指定Ver）*/
	public SyainInfoBean getSyainKojinInfoBean(String id) throws SQLException {
		// TODO 自動生成されたメソッド・スタブ
		SyainInfoBean syaininfobean = new SyainInfoBean();
		Connection con = DBConnectUtil.connect();
		ResultSet rs = null;
		try{
			String sql = "SELECT ST.ST_ID,ST.ST_NAME,ST.ST_NAME_KANA,ST.ST_TEL,ST.ST_ADDRESS,ST.POSITION,SH.SH_NAME"
					 + " FROM STAFF ST,SHOP SH  WHERE ST.ST_ID = ? AND ST.SH_ID = SH.SH_ID";
			ps = con.prepareStatement(sql);
			ps.setString(1, id);
			rs = ps.executeQuery();

//			//デバッグ用
//			while(rs.next()){
//				System.out.println(rs.getInt("st_id"));
//				System.out.println(rs.getString("st_name"));
//				System.out.println(rs.getString("st_name_kana"));
//				System.out.println(rs.getString("st_tel"));
//				System.out.println(rs.getString("st_address"));
//				System.out.println(rs.getString("position"));
//			}

			while(rs.next()){
				SyainBean rc = new SyainBean();
				rc.setsId(rs.getInt("ST_ID"));
				rc.setsName(rs.getString("ST_NAME"));
				rc.setsNameKana(rs.getString("ST_NAME_KANA"));
				rc.setsTel(rs.getString("ST_TEL"));
				rc.setsAddress(rs.getString("ST_ADDRESS"));
				rc.setsPossion(rs.getString("POSITION"));
				rc.setShName(rs.getString("SH_NAME"));
				syaininfobean.addSyainRecord(rc);
			}
		}catch(SQLException e){
			e.printStackTrace();
		}finally{
			dbc.disconnect(con);
			rs.close();

		}

		return syaininfobean;
	}

	/*社員更新*/
	public void updateSyainInfo(SyainBean sem) {
		// TODO 自動生成されたメソッド・スタブ
		Connection con = DBConnectUtil.connect();
		try{
		String sql = "UPDATE STAFF SET st_name = ?, st_name_kana = ?, st_tel = ?, st_address = ?, position = ?, sh_id = ?"
				+ " WHERE st_id = ? ";
		ps = con.prepareStatement(sql);
		ps.setString(1,sem.getsName());
		ps.setString(2,sem.getsNameKana());
		ps.setString(3,sem.getsTel());
		ps.setString(4,sem.getsAddress());
		ps.setString(5,sem.getsPossion());
		ps.setString(6,sem.getsTenpo());
		ps.setInt(7,sem.getsId());
		ps.executeUpdate();

	} catch (SQLException e) {
		e.printStackTrace();
	} finally {
		dbc.disconnect(con);
	}
	return;
	}

	/*社員権限登録*/
	public void Syainkengen(String sid, String no, String sPrTime) {
		// TODO 自動生成されたメソッド・スタブ
		Connection con = DBConnectUtil.connect();
		try{
			String sql = "INSERT INTO PERMISSION_ROLL VALUES(?,?,TO_DATE(?,'yyyy/MM/dd HH24:mi:ss'))";
			ps = con.prepareStatement(sql);
			ps.setString(1,sid);
			ps.setString(2,no);
			ps.setString(3,sPrTime);
			ps.executeUpdate();
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			dbc.disconnect(con);
		}
		return;
	}

	/* 社員権限削除 */
	public void Syainkengensakujyo(String sid, String no, String sPrTime) {
		Connection con = DBConnectUtil.connect();
		try{
			String sql = "DELETE FROM PERMISSION_ROLL WHERE st_id = ? AND roll_id = ?";
			ps = con.prepareStatement(sql);
			ps.setString(1,sid);
			ps.setString(2,no);
			ps.executeUpdate();
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			dbc.disconnect(con);
		}
		return;
	}
}
