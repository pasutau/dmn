package db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import bean.ProductBean;
public class ProductDAO {
	PreparedStatement ps= null;
	DBConnectUtil dbc = new DBConnectUtil();

	public ProductBean getProductBean(String bangou) {
		// TODO 自動生成されたメソッド・スタブ
		ProductBean pb = new ProductBean();
		Connection con = DBConnectUtil.connect();
		ResultSet rs = null;
		try{
			String sql = "SELECT p_id,p_name,n_id FROM PRODUCT WHERE p_id = ? ";
			ps = con.prepareStatement(sql);
			ps.setString(1, bangou);
			rs = ps.executeQuery();
			while(rs.next()){
				pb.setpId(rs.getString(1));
				pb.setpName(rs.getString(2));
				pb.setnId(rs.getString(3));
			}
		}catch(SQLException e){
			e.printStackTrace();
		}finally{
			dbc.disconnect(con);
		}

		return pb;
	}
//てえてて
	public void updateProductInfo(String pid, String nid) {
		// TODO 自動生成されたメソッド・スタブ
		Connection con = DBConnectUtil.connect();
		try{
			String sql = "UPDATE PRODUCT SET n_id = ? WHERE p_id = ?";
			ps = con.prepareStatement(sql);
			ps.setString(1,nid);
			ps.setString(2,pid);
			ps.executeUpdate();
	} catch (SQLException e) {
		e.printStackTrace();
	} finally {
		dbc.disconnect(con);
	}
	return ;
	}

	public ProductBean getProductId(String id) {
		// TODO 自動生成されたメソッド・スタブ
		ProductBean pb = new ProductBean();
		Connection con = DBConnectUtil.connect();
		ResultSet rs = null;
		try{
			String sql = "SELECT p_id,p_name,n_id FROM NEWNESS WHERE p_id = ? ";
			ps = con.prepareStatement(sql);
			ps.setString(1, id);
			rs = ps.executeQuery();
			while(rs.next()){
				pb.setpId(rs.getString(1));
				pb.setpName(rs.getString(2));
				pb.setnId(rs.getString(3));
			}
		}catch(SQLException e){
			e.printStackTrace();
		}finally{
			dbc.disconnect(con);
		}

		return pb;
	}
}
