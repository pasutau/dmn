package db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;


public class DBLogic {
	/*認証(Login)*/
	public String login(Connection con, String stId) throws SQLException{

		String stName = "haitteyonaiyo";
		ResultSet rs = null;
		PreparedStatement pst = null;
		try{
			pst = con.prepareStatement("SELECT st_name FROM STAFF WHERE st_id =" + stId);
			rs = pst.executeQuery();
			while(rs.next()){
			stName = rs.getString("ST_NAME");
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		pst.close();
		rs.close();
	return stName;
	}

	/*権限判定*/
	public boolean authority(Connection con,String stId, int kId) throws SQLException {

		boolean ret = false;//許可しない状態
		ResultSet rs = null;
		PreparedStatement pst = null;

		try{
			pst = con.prepareStatement("SELECT * FROM permission_roll"
									+ " WHERE st_id = " + stId
									+ " AND roll_id = " + kId);
			rs = pst.executeQuery();
			//カウントが0ならデータが無い...はず
			int cnt = 0;
			while(rs.next()){
				cnt++;
			}
			if(cnt > 0){
				ret = true;
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		pst.close();
		rs.close();
		System.out.println("DBLogicのret=" +ret);
		return ret;
	}


	/*レジIDを元に店舗IDを取得*/
	public int stIdShutoku(Connection con,String rId) throws SQLException{
		int stId = 0;
		int regiId = Integer.parseInt(rId);
		ResultSet rs = null;
		PreparedStatement pst = null;
		try{
			pst = con.prepareStatement("SELECT sh_id FROM register"
									+ " WHERE regi_id = " + regiId);
			rs = pst.executeQuery();
			while(rs.next()){
				stId = rs.getInt("sh_id");
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		pst.close();
		rs.close();
		return stId;
	}

	/*AC除去して新しい番号発行(max+1)表名(product)入力されるとmax+1されたID帰ってくる*/
	public String newAcId(Connection con,String hyomei) throws SQLException{
		String newId = null;
		int sagyoid = 0;
		String sagyoid2 = "";
		String id = null;
		int mojisu = 0;

		if(hyomei.equals("media")){
			id = "m_id";
			mojisu = 9;
		}else{
			id = "p_id";
			mojisu = 8;
		}

		ResultSet rs = null;
		PreparedStatement pst = null;
		try{
			pst = con.prepareStatement("SELECT MAX(SUBSTR(" + id + ",3," + mojisu + ")) FROM " + hyomei);
			rs = pst.executeQuery();
			while(rs.next()){
				sagyoid = Integer.parseInt(rs.getString(1));
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		pst.close();
		rs.close();

		sagyoid+=1;
		sagyoid2 = String.valueOf(sagyoid);
		while(sagyoid2.length() < mojisu){
			sagyoid2 = "0" + sagyoid2;
		}
		newId = "AC"+sagyoid2;

		return newId;
	}


	/*AC除去して新しい番号発行max+1されたmID帰ってくる*/
	public String newMid(Connection con,String pid) throws SQLException{
		String newmId = null;
		String sagyomid = null;
		String sagyomid2 = "";
		String mid = null;
		int sagyo = 0;

		ResultSet rs = null;
		PreparedStatement pst = null;
		try{
			pst = con.prepareStatement("SELECT p_id,MAX(TO_NUMBER(SUBSTR(m_id,11)))"
									+ " FROM media"
									+ " WHERE media.p_id = '" + pid + "'"
									+ " GROUP BY p_id");
			rs = pst.executeQuery();
			while(rs.next()){
				sagyomid = rs.getString(2);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		pst.close();
		rs.close();


		if(sagyomid == null){
			newmId = pid+"0";
		}else{
			sagyo = Integer.parseInt(sagyomid);
			sagyo+=1;
			sagyomid2 = String.valueOf(sagyo);
			newmId = pid+sagyomid2;
		}
		return newmId;
	}
}

