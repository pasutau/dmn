﻿package db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBConnectUtil {

	/**
	 * DBに接続する。
	 *
	 * @return con Connection
	 */
	public static Connection connect() {


		String drvName = "oracle.jdbc.driver.OracleDriver";
		String url = "jdbc:oracle:thin:@";
		String port = "1521";

		// ローカルDB用
		// String servername = "localhost";
		// String sid = "xe";
		// String user = "root";// 各自の設定値
		// String pass = "1234";// 各自の設定値

		// 学生環境DB用
		String servername = "10.11.39.214";
		String sid = "HCS1";
		String user = "h20153007";// 各自の設定値
		String pass = "oraclemaster";

		Connection con = null;

		try {
			Class.forName(drvName);
			con = DriverManager.getConnection(url + servername + ":" + port + ":" + sid, user, pass);
			con.setAutoCommit(false);
			System.out.println("接続しました");
			//Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
			//Properties prop = new Properties(); // 追記
			//prop.put("charSet", "SJIS"); // 追記
			//con = DriverManager.getConnection("jdbc:odbc:GakuseiMng", prop); // 修正
		} catch (SQLException e) {
			while (e != null) {
				System.err.println("@Err@SQLException DBConnectUtil connect");
				System.err.println("[" + e.getErrorCode() + "]");
				System.err.println(e.getMessage());
				System.err.println(e.getSQLState());
				System.err.println();
				e = e.getNextException();
			}
		} catch (Exception e) {
			System.err.println("@Err@Exception DBConnectUtil connect");
			e.printStackTrace();
		}

		return con;
	}

	/**
	 * DBを切断する。
	 *
	 * @param con Connection
	 */
	public void disconnect(Connection con) {
		try {
			if (con != null) {
				con.close();
				con = null;
			}
		} catch (Exception e) {
			System.err.println("@Err@Exception DBConnectUtil disconnect");
		}
		System.out.println("切断しました");
	}

	/**
	 * DBをコミットする。
	 *
	 * @param con Connection
	 */
	public void commit(Connection con) {
		if (con != null) {
			try {
				con.commit();
				System.out.println("＊コミットしました");
			} catch (SQLException e) {
				while (e != null) {
					System.err.println("@Err@SQLException DBConnectUtil commit");
					System.err.println("[" + e.getErrorCode() + "]");
					System.err.println(e.getMessage());
					System.err.println(e.getSQLState());
					System.err.println();
					e = e.getNextException();
				}
			} catch (Exception e) {
				System.err.println("@Err@Exception DBConnectUtil commit");
				e.printStackTrace();
			}
		}
	}

	/**
	 * DBをロールバックする。
	 *
	 * @param con Connection
	 */
	public void rollback(Connection con) {
		if (con != null) {
			try {
				con.rollback();
			} catch (SQLException e) {
				while (e != null) {
					System.err.println("@Err@SQLException DBConnectUtil rollback");
					System.err.println("[" + e.getErrorCode() + "]");
					System.err.println(e.getMessage());
					System.err.println(e.getSQLState());
					System.err.println();
					e = e.getNextException();
				}
			} catch (Exception e) {
				System.err.println("@Err@Exception DBConnectUtil rollback");
				e.printStackTrace();
			}
		}
	}
}
