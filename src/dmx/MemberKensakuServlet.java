package dmx;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import bean.MemberInfoBean;
import db.KaiinDAO;

/**
 * Servlet implementation class MemberKensakuServlet
 */
@WebServlet("/MemberKensakuServlet")
public class MemberKensakuServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private MemberInfoBean memInfoBean;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public MemberKensakuServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String uid = request.getParameter("uid");
		String uname = request.getParameter("uname");
		String utel = request.getParameter("utel");
		String ubirth = request.getParameter("ubirth");
		try{
			KaiinDAO dao = new KaiinDAO();
			memInfoBean = dao.getMemberInfoBean(uname,utel,ubirth,uid);
			HttpSession session = request.getSession();
			session.setAttribute("memInfoBean", memInfoBean);
			getServletContext().getRequestDispatcher("/kaiinkensaku2.jsp").forward(request, response);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

}
