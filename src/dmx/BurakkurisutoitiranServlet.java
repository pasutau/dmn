package dmx;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import bean.BurakkurisutoInfoBean;
import db.BurakkuDAO;

/**
 * Servlet implementation class BurakkurisutoitiranServlet
 */
@WebServlet("/BurakkurisutoitiranServlet")
public class BurakkurisutoitiranServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private BurakkurisutoInfoBean burakkuBean;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public BurakkurisutoitiranServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		try{
			BurakkuDAO dao = new BurakkuDAO();
			burakkuBean = dao.getBurakkurisutoInfoBean();
			HttpSession session = request.getSession();
			session.setAttribute("buraInfoBean", burakkuBean);
			getServletContext().getRequestDispatcher("/burakkurisutokousin.jsp").forward(request, response);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

}
