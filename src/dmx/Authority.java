package dmx;

import java.sql.Connection;
import java.sql.SQLException;

import db.DBConnectUtil;
import db.DBLogic;

public class Authority{
	public boolean AuthorityCheck(String stId,int kId) throws SQLException{

		boolean ret = true;
		DBConnectUtil dbc = new DBConnectUtil();
		DBLogic dbl = new DBLogic();
		Connection con = DBConnectUtil.connect();

		ret = dbl.authority(con,stId,kId);

		dbc.disconnect(con);
		return ret;
	}
}
