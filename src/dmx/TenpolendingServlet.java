package dmx;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import db.MediaDAO;
import bean.LoanBean;

/**
 * Servlet implementation class TenpolendingServlet
 */
@WebServlet("/TenpolendingServlet")
public class TenpolendingServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	String mediaid = "";
	int slid = 0;
	String pid = "";
	int shid = 0;
	int mytenid;// 自店舗ID
	String shopname;// 店舗名
	String shouhinname;// 商品名
	String lenstartday;// 貸出開始日
	// 店舗間貸出表に追加する

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// データベースに登録する情報をjspからとってくる

		Date date = new Date();
		SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		lenstartday = format.format(date);

		slid = Integer.parseInt(request.getParameter("slid"));// 変換が必要なものを数字に
		shid = Integer.parseInt(request.getParameter("shid"));
		mytenid = Integer.parseInt(request.getParameter("mytenid"));

		// dbに渡すために全部beanにぶっこんでる
		LoanBean loanbean = new LoanBean();
		loanbean.setLenstartday(lenstartday = format.format(date));// 貸出開始日をセット

		// 貸出終了予定日（２週間）を作るよ
		Date newTime = null;

		try {
			// 日付計算

			try {
				Calendar now = Calendar.getInstance();
				// Calendarクラスのインスタンスを生成

				now.setTime(date);
				// Date型からカレンダー型に変換

				now.add(Calendar.DATE, 14);
				// 当日の日付に２週間
				// 当日→1 7泊8日→7

				newTime = now.getTime();
				// カレンダー型からDate型に変換

			} catch (Exception e) {
				e.printStackTrace();
			}
			java.sql.Date sqlDate = new java.sql.Date(newTime.getTime());
			loanbean.setPlanTime(format.format(sqlDate));// 貸出終了予定日セット(文字型）
		} catch (Exception e) {
			e.printStackTrace();
		}

		loanbean.setMediaid(request.getParameter("mediaid"));
		loanbean.setSlid(slid);
		loanbean.setPid(request.getParameter("pid"));
		loanbean.setShid(shid);
		loanbean.setMytenpoid(mytenid);
		loanbean.setShopname(request.getParameter("shopname"));
		loanbean.setProductname(request.getParameter("shouhinname"));
		loanbean.setStatus("4");

		try {
			MediaDAO mddao = new MediaDAO();
			mddao.slendingUpdate(loanbean);
		} catch (Exception e) {
			e.printStackTrace();
		}
		RequestDispatcher dispatch = request.getRequestDispatcher("kasidasijouhoukousinkanryo.jsp");;
		dispatch.forward(request, response);
	}
}