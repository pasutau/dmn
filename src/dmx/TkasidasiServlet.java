package dmx;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import bean.LoanBean;
import bean.LoanapplicationListBean;
import db.DBConnectUtil;
import db.MediaDAO;

/**
 * Servlet implementation class TkasidasiServlet
 */
@WebServlet("/TkasidasiServlet")
public class TkasidasiServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	DBConnectUtil dbc = new DBConnectUtil();

	// 他店舗に貸出しているメディア一覧//

	int mysyainid;// 自分の社員ＩＤ
	int mytenpoid;// 自分の店舗ＩＤ
	String myTenponame;// 自分の店舗名
	String productID;// 商品ＩＤ
	String product;// 商品名
	int loanID;// 店舗貸出ＩＤ

	LoanBean lbean = new LoanBean();
	MediaDAO mddao = new MediaDAO();
	Connection con = DBConnectUtil.connect();
	LoanapplicationListBean lListbean = new LoanapplicationListBean();

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		Connection con = DBConnectUtil.connect();

		// クッキーからレジID取得する
		Cookie[] cookies = request.getCookies();
		String stid = "";
		// クッキーから社員ＩＤをもってくる
		if (cookies != null) {
			for (Cookie cook : cookies) {
				if (cook.getName().equals("StId")) {
					stid = cook.getValue();
				}
			}
		}
		int Istid = Integer.parseInt(stid);// 社員ＩＤをＩＮＴ型に変換

		try {
			lbean.setMytenpoid(mddao.tenpoidPull(con, Istid)); // 自分の店舗ＩＤをbeanにセット
		} catch (SQLException e) {
			e.printStackTrace();
		}

		// 自店舗ＩＤ ＝ 借入先店舗ＩＤ AND ステータス=4をセレクト
		lListbean = mddao.zitenpoLoanpull(con, lbean.getMytenpoid());// 自店舗ＩＤをとばして自店舗貸出中一覧をひっぱってきてる

		HttpSession session = request.getSession();
		session.setAttribute("lListbean", lListbean);
		getServletContext().getRequestDispatcher("/zitenpokasidasi.jsp").forward(request, response);
	}

}
