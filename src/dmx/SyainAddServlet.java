package dmx;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import bean.SyainBean;
import db.DBConnectUtil;
import db.SyainDAO;

/**
 * Servlet implementation class MemberAddServlet
 */
@WebServlet("/SyainAddServlet")
public class SyainAddServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	DBConnectUtil dbc = new DBConnectUtil();
	SyainDAO  kasi = new SyainDAO();
	Date date  = new Date();

    /**
     * @see HttpServlet#HttpServlet()
     */
    public SyainAddServlet() {
        super();
        // TODO Auto-generated constructor stub
    }


	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
    @Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
    	SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");

		String  sName = null;
		String 	sNameKana = null;
		String 	sTenpo = null;
		String 	sTel = null;
		String	sAddress = null;
		String	sPossion = null;
		String	sStartTime = null;


		try {
			sName = request.getParameter("sName");
			sNameKana = request.getParameter("sNameKana");
			sTenpo = request.getParameter("sTenpo");
			sTel = request.getParameter("sTel");
			sAddress = request.getParameter("sAddress");
			sPossion = request.getParameter("sPossion");
			sStartTime = format.format(date);
		} catch (Exception e) {
			e.printStackTrace();
		}

		SyainBean sem = new SyainBean();
		sem.setsName(sName);
		sem.setsNameKana(sNameKana);
		sem.setsTenpo(sTenpo);
		sem.setsTel(sTel);
		sem.setsAddress(sAddress);
		sem.setsPossion(sPossion);
		sem.setsStartTime(sStartTime);



		try {
			SyainDAO dao = new SyainDAO();
			dao.addSyainInfo(sem);

		}catch (Exception e) {
			e.printStackTrace();
		}
		RequestDispatcher dispatch = request.getRequestDispatcher("syainzyouhoutorokukanryo.jsp");
		dispatch.forward(request, response);
	}
}
