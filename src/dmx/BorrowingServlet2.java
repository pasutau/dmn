package dmx;

import java.io.IOException;
import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import bean.LoanBean;
import bean.LoanapplicationListBean;
import db.DBConnectUtil;
import db.MediaDAO;

/**
 * Servlet implementation class LoandetailsServlet
 */
@WebServlet("/BorrowingServlet2")
public class BorrowingServlet2 extends HttpServlet {
	private static final long serialVersionUID = 1L;

	Connection con = DBConnectUtil.connect();
	MediaDAO md = new MediaDAO();
	LoanBean loanbean = new LoanBean();
	LoanapplicationListBean lListbean = new LoanapplicationListBean();
	String pid = "";
	int shid = 0;
	int mytenid = 0;
	String shopname = "";
	String shouhinname = "";
	String mid = "";
	String lastdate = "";

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		mid = request.getParameter("mediaid");//返すメディアＩＤ
		pid = request.getParameter("pid");// 返す商品ＩＤ
		shid = Integer.parseInt(request.getParameter("shid"));// 貸し出している店舗ID
		mytenid = Integer.parseInt(request.getParameter("mytenid"));// 自分の店舗ID
		shopname = request.getParameter("shopname");// 貸し出している店舗名
		shouhinname = request.getParameter("shouhinname");// 貸し出している商品名
		// 表示項目 → 店舗名、タイトル、メディアＩＤ、（入力）をとってくるよおお

		loanbean.setMediaid(mid);
		loanbean.setPid(pid);
		loanbean.setShid(mytenid);
		loanbean.setMytenpoid(mytenid);
		loanbean.setShopname(shopname);
		loanbean.setProductname(shouhinname);
		loanbean.setStatus("5");
		try {
			MediaDAO mddao = new MediaDAO();
			mddao.s_borrowingUpdate(loanbean);
		} catch (Exception e) {
			e.printStackTrace();
		}

		getServletContext().getRequestDispatcher("/zitenpokariirehenkyakukan.jsp").forward(request, response);
	}

}
