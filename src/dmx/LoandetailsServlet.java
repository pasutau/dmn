package dmx;

import java.io.IOException;
import java.sql.Connection;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import bean.LoanBean;
import bean.LoanapplicationListBean;
import db.DBConnectUtil;
import db.MediaDAO;

/**
 * Servlet implementation class LoandetailsServlet
 */
@WebServlet("/LoandetailsServlet")
public class LoandetailsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	Connection con = DBConnectUtil.connect();
	MediaDAO md = new MediaDAO();
	LoanBean loanbean = new LoanBean();
	LoanapplicationListBean lListbean = new LoanapplicationListBean();
	String pid = "";
	int shid = 0;
	int mytenid = 0;
	String shopname = "";
	String shouhinname = "";
	String mid = "";
	int slid = 0;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		pid = request.getParameter("pid");// 貸し出す商品ＩＤ
		shid = Integer.parseInt(request.getParameter("shid"));// 貸し出す店舗ID
		mytenid = Integer.parseInt(request.getParameter("mytenid"));// 自分の店舗ID
		shopname = request.getParameter("shopname");// 店舗名
		shouhinname = request.getParameter("shouhinname");// 商品名
		slid = Integer.parseInt(request.getParameter("slid"));//店舗貸出ＩＤ
		// 表示項目 → 店舗名、タイトル、メディアＩＤ（入力）をとってくるよおお

		loanbean.setSlid(slid);
		loanbean.setPid(pid);
		loanbean.setShid(mytenid);
		loanbean.setMytenpoid(mytenid);
		loanbean.setShopname(shopname);
		loanbean.setProductname(shouhinname);

		lListbean.addMediainfo(loanbean);
		HttpSession session = request.getSession();
		session.setAttribute("lListbean", lListbean);

		getServletContext().getRequestDispatcher("/kasidasisyousai.jsp").forward(request, response);
	}

}
