package dmx;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import bean.BurakkurisutoBean;
import db.BurakkuDAO;

/**
 * Servlet implementation class BurakkurisutoAddServlet
 */
@WebServlet("/BurakkurisutoAddServlet")
public class BurakkurisutoAddServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public BurakkurisutoAddServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		Date date = new Date();
		SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");


		int uid =0;
		String utime = format.format(date);
		int stid =0 ;
		String reason = null;
		String ustatus = "2";
		try{
			uid = Integer.parseInt(request.getParameter("uid"));
			stid = Integer.parseInt(request.getParameter("stid"));
			reason = request.getParameter("riyuu");
		}catch (Exception e) {
			e.printStackTrace();
		}

		BurakkurisutoBean burabean = new BurakkurisutoBean();
		burabean.setUid(uid);
		burabean.setUtime(utime);
		burabean.setStid(stid);
		burabean.setReason(reason);
		burabean.setBstatus(ustatus);

		try{
			BurakkuDAO dao = new BurakkuDAO();
			dao.addBurakkuInfo(burabean);
		}catch(Exception e){
			e.printStackTrace();
		}
		RequestDispatcher dispatch = request.getRequestDispatcher("burakkurisutotourokukanryou.jsp");
		dispatch.forward(request, response);

	}

}
