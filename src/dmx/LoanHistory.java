package dmx;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import bean.KasiRirekiListBean;
import db.DBConnectUtil;
import db.KasidasiDAO;


/*メディア貸出履歴検索
 * mediaId打たれたら誰が借りたか表示する*/
@WebServlet("/LoanHistory")
public class LoanHistory extends HttpServlet {
	private static final long serialVersionUID = 1L;

	DBConnectUtil dbc = new DBConnectUtil();
	KasidasiDAO	  kasi = new KasidasiDAO();
	KasiRirekiListBean kasilist = new KasiRirekiListBean();

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		Connection con = DBConnectUtil.connect();
		String  mediaId = request.getParameter("mediaId");//リクエスト送ってくる奴

		try {
			 kasilist = kasi.mediaKasiRireki(con, mediaId);
		} catch (SQLException e) {
			e.printStackTrace();
		}

		HttpSession session = request.getSession();
		session.setAttribute("kasilist", kasilist);

		dbc.disconnect(con);

		getServletContext().getRequestDispatcher("/mediakasidasirirekiitiran.jsp").forward(request, response);
	}

}
