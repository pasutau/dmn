package dmx;

import java.io.IOException;
import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import bean.RegisterBean;
import db.DBConnectUtil;
import db.DBLogic;
import db.RegisterDAO;


/**
 * Servlet implementation class RegisterKeisan
 */
@WebServlet("/RegisterSyusen")
public class RegisterSyusen extends HttpServlet {
	private static final long serialVersionUID = 1L;

	DBLogic dbl = new DBLogic();
    /**
     * @see HttpServlet#HttpServlet()
     */
    public RegisterSyusen() {
        super();

    }

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int RegiId=0;
		int ShId=0;
		int Man=0;
		int Gosen=0;
		int Nisen=0;
		int Sen=0;
		int Gohyaku=0;
		int Hyaku=0;
		int Gozyu=0;
		int Zyu=0;
		int Go=0;
		int Iti=0;
		int sougoukei=0;
		RegisterDAO dao = new RegisterDAO();
		Date date = new Date();
		SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		String RtTime= format.format(date);

	try{
		RegiId = Integer.parseInt(request.getParameter("rid"));
		ShId = dao.stIdShutoku(request.getParameter("rid"));//店舗IDとってる
		Man = Integer.parseInt(request.getParameter("man")) * 10000;
		Gosen = Integer.parseInt(request.getParameter("gosen"))* 5000;
		Nisen = Integer.parseInt(request.getParameter("nisen")) * 2000;
		Sen = Integer.parseInt(request.getParameter("sen"))* 1000;
		Gohyaku = Integer.parseInt(request.getParameter("gohyaku"))* 500;
		Hyaku = Integer.parseInt(request.getParameter("hyaku"))* 100;
		Gozyu = Integer.parseInt(request.getParameter("gozyu"))* 50;
		Zyu = Integer.parseInt(request.getParameter("zyu"))* 10;
		Go = Integer.parseInt(request.getParameter("go"))* 5;
		Iti = Integer.parseInt(request.getParameter("iti"))* 1;
		sougoukei = Man+Gosen+Nisen+Sen+Gohyaku+Hyaku+Gozyu+Zyu+Go+Iti;

	} catch (Exception e) {
		e.printStackTrace();
	}

	RegisterBean reg = new RegisterBean();
	reg.setRegiId(RegiId);
	reg.setShId(ShId);
	reg.setMan(Man);
	reg.setGosen(Gosen);
	reg.setNisen(Nisen);
	reg.setSen(Sen);
	reg.setGohyaku(Gohyaku);
	reg.setHyaku(Hyaku);
	reg.setGozyu(Gozyu);
	reg.setZyu(Zyu);
	reg.setGo(Go);
	reg.setIti(Iti);
	reg.setSougoukei(sougoukei);
	dao.addRegister(reg);
	dao.addSyusen(RtTime,sougoukei,RegiId);

	RequestDispatcher dispatch = request.getRequestDispatcher("syusentoroku.jsp");;
	dispatch.forward(request, response);
	}
}

