package dmx;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import bean.BurakkurisutoBean;
import db.BurakkuDAO;

/**
 * Servlet implementation class BurakkurisutoUpdateServlet
 */
@WebServlet("/BurakkurisutoUpdateServlet")
public class BurakkurisutoUpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public BurakkurisutoUpdateServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		int count = 1;
		HttpSession session = request.getSession();
		@SuppressWarnings("unchecked")
		ArrayList<BurakkurisutoBean> buraInfoArray = (ArrayList<BurakkurisutoBean>) session.getAttribute("buraInfoArray");
		try{
			for(BurakkurisutoBean rc : buraInfoArray) {
				if(!(request.getParameter("status"+count).equals("2"))){
					BurakkuDAO dao = new BurakkuDAO();
					dao.BurakkurisutoUpdate(rc.getUid(),request.getParameter("status"+count));
				}
				count++;
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		RequestDispatcher dispatch = request.getRequestDispatcher("burakkukousinkanryou.jsp");
		dispatch.forward(request, response);
	}
}
