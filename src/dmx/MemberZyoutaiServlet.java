package dmx;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import bean.MemberBean;
import bean.MemberInfoBean;
import db.KaiinDAO;

/**
 * Servlet implementation class MemberZyoutaiServlet
 */
@WebServlet("/MemberZyoutaiServlet")
public class MemberZyoutaiServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private MemberInfoBean memInfoBean;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public MemberZyoutaiServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		Date date = new Date();

		try{
			String str2 = format.format(date);
			Date date2 = format.parse(str2);
			long datew = date2.getTime();

			KaiinDAO dao = new KaiinDAO();
			memInfoBean = dao.MemberZyoutaiUpdate();
			ArrayList<MemberBean> memInfoArray = memInfoBean.getMemberArray();
			for(MemberBean rc : memInfoArray) {

				String str1 = rc.getuStartTime().substring(0, 10);
				Date date1 = format.parse(str1);
				long dateq = date1.getTime();

				long dayDiff = ( datew - dateq  ) / (1000 * 60 * 60 * 24 );
				if(dayDiff >= 732){
					String st = "3";
					int id = rc.getuId();
					dao.MemberZyoutaiUpdate(id,st);
				} else if(dayDiff >= 366){
					String st = "2";
					int id = rc.getuId();
					dao.MemberZyoutaiUpdate(id,st);
				}

				}

		}catch(Exception e){
			e.printStackTrace();
		}
		RequestDispatcher dispatch = request.getRequestDispatcher("kaiinkanri.jsp");
		dispatch.forward(request, response);
	}

}
