package dmx;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import bean.SyainInfoBean;
import db.SyainDAO;

/**
 * Servlet implementation class SyainkensakuServlet
 */
@WebServlet("/SyainkensakuServlet3")
public class SyainkensakuServlet3 extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private SyainInfoBean SyainInfoBean;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SyainkensakuServlet3() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String sid = request.getParameter("sid");
		try{
			SyainDAO dao = new SyainDAO();
			SyainInfoBean = dao.getSyainKojinInfoBean(sid);
			HttpSession session = request.getSession();
			session.setAttribute("SyainInfoBean", SyainInfoBean);
			getServletContext().getRequestDispatcher("/syainkengensakujyo.jsp").forward(request, response);
		}catch(Exception e){
			e.printStackTrace();
		}
	}
}

