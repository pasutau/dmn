package dmx;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import db.DBConnectUtil;
import db.DBLogic;
import db.TitleTuikaDAO;

/**
 * Servlet implementation class MemberAddServlet
 */
@WebServlet("/MediaTuika")
public class MediaTuika extends HttpServlet {
	private static final long serialVersionUID = 1L;
	DBConnectUtil dbc = new DBConnectUtil();
    /**
     * @see HttpServlet#HttpServlet()
     */
    public MediaTuika() {
        super();
    }

    @Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Connection con = DBConnectUtil.connect();
		DBLogic dbl = new DBLogic();
		String mid = null;
		int su = Integer.parseInt(request.getParameter("su"));
		String mcategory = request.getParameter("category");
		String pid = request.getParameter("pid");

		while(su > 0){
			try {
				mid = dbl.newMid(con, pid);
			} catch (SQLException e) {
				e.printStackTrace();
			}
			int shid =  Integer.parseInt(request.getParameter("sTenpo"));

			TitleTuikaDAO dao = new TitleTuikaDAO();
			try {
				dao.addMedia(mid, mcategory, shid, pid);
			} catch (SQLException e) {
				e.printStackTrace();
			}
			su--;
		}
		RequestDispatcher dispatch = request.getRequestDispatcher("mediatuikakanryou.jsp");
		//次こことID発行メソッド
		dispatch.forward(request, response);
    }
}
