package dmx;

import java.io.IOException;
import java.net.URLEncoder;
import java.sql.Connection;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import db.DBConnectUtil;
import db.DBLogic;


/*ログイン用クラス*/
@WebServlet("/Authentication")
public class Authentication extends HttpServlet {
	private static final long serialVersionUID = 1L;

	DBConnectUtil dbc = new DBConnectUtil();
	DBLogic dbl = new DBLogic();

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		System.out.println(1);
		Connection con = DBConnectUtil.connect();
		String stName = null;//データベースから拾ってくる社員名
		String stId = request.getParameter("stid");//リクエスト送ってくる奴
		String regiId = request.getParameter("rid");//リクエスト送ってくるやつ
		try {
			stName = dbl.login(con, stId);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		dbc.disconnect(con);



		Cookie cookStId = new Cookie("StId",stId);
		Cookie cookStName = new Cookie("StName",URLEncoder.encode(stName, "UTF-8"));
		Cookie cookRegiId = new Cookie("RegiId",regiId);
		cookStId.setMaxAge(86400);
		cookStName.setMaxAge(86400);
		cookRegiId.setMaxAge(86400);
		response.addCookie(cookStId);
		response.addCookie(cookStName);
		response.addCookie(cookRegiId);

		String url = "./Topmenu.jsp";
	    response.sendRedirect(url);

	}

}
