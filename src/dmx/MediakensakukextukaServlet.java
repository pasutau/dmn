package dmx;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import bean.ProductBean;
import db.ProductDAO;

/**
 * Servlet implementation class MediakensakukextukaServlet
 */
@WebServlet("/MediakensakukextukaServlet")
public class MediakensakukextukaServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public MediakensakukextukaServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		ProductBean productbean = new ProductBean();
		String bangou = request.getParameter("bangou");

		try{
			ProductDAO dao = new ProductDAO();
			productbean = dao.getProductBean(bangou);
			System.out.println(productbean.getpName());
			HttpSession session = request.getSession();
			session.setAttribute("productBean", productbean);
			getServletContext().getRequestDispatcher("/mediaSerchResult.jsp").forward(request, response);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	}
