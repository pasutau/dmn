package dmx;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import bean.MemberBean;
import db.KaiinDAO;

/**
 * Servlet implementation class MemberUpdateServlet
 */
@WebServlet("/MemberUpdateServlet")
public class MemberUpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public MemberUpdateServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		int  uId = 0;
		String	uName = null;
		String 	uNameKana = null;
		String 	uBirthday = null;
		String 	uBirthday1 = null;
		String 	uBirthday2 = null;
		String 	uBirthday3 = null;
		String	uGender = null;
		String  uJob = null;
		String	uPostal = null;
		String 	uAddress = null;
		String  uTel = null;
		String  uMail = null;


		try {
			uId = Integer.parseInt(request.getParameter("uid"));
			uName = request.getParameter("name");
			uNameKana = request.getParameter("oname");
			uBirthday1 = request.getParameter("room");
			uBirthday2 = request.getParameter("tuki");
			if(Integer.parseInt(uBirthday2)>0 && Integer.parseInt(uBirthday2)<10){
				uBirthday2 = "0"+uBirthday2;
			}
			uBirthday3 = request.getParameter("hi");
			if(Integer.parseInt(uBirthday3)>0 && Integer.parseInt(uBirthday3)<10){
				uBirthday3 = "0"+uBirthday3;
			}
			uBirthday = uBirthday1+"-" +uBirthday2+"-"+ uBirthday3;
			uGender = request.getParameter("radio1");
			uJob = request.getParameter("radio2");
			uPostal = request.getParameter("example2");
			uAddress = request.getParameter("tosi");
			uTel = request.getParameter("denwa");
			uMail = request.getParameter("mail");
		} catch (Exception e) {
			e.printStackTrace();
		}

		MemberBean mem = new MemberBean();
		mem.setuId(uId);
		mem.setuName(uName);
		mem.setuNameKana(uNameKana);
		mem.setuBirthday(uBirthday);
		mem.setuGender(uGender);
		mem.setuJob(uJob);
		mem.setuPostal(uPostal);
		mem.setuAddress(uAddress);
		mem.setuTel(uTel);
		mem.setuMail(uMail);
		int flag = Integer.parseInt(request.getParameter("flag"));
		try{
			KaiinDAO dao = new KaiinDAO();
			dao.updateMemberInfo(mem);
			if(flag==2){
				String st = "3";
				dao.MemberZyoutaiUpdate(uId,st);
			}else if(flag==3){
				Date date = new Date();
				SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
				String sttime = format.format(date);
				String st = "3";
				dao.MemberZyoutaiUpdate(uId,st);
				dao.addNewMember(mem,sttime,request.getParameter("rid"));
				dao.addUpdateNewMember(uId,sttime);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		RequestDispatcher dispatch = request.getRequestDispatcher("kaiinkousinkanryou.jsp");;
		dispatch.forward(request, response);
	}

}
