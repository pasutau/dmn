package dmx;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import bean.MediaListBean;
import db.DBConnectUtil;
import db.FusokuDAO;

/**
 * Servlet implementation class MediaTenpoServlet
 */
@WebServlet("/MediaTenpoServlet")
public class MediaTenpoServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	DBConnectUtil dbc = new DBConnectUtil();
	private MediaListBean MediaListBean;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public MediaTenpoServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub

		String pid = null;
		String pkubn = null;

		try {
			pid = request.getParameter("pid");
			pkubn = request.getParameter("pkubn");
		} catch (Exception e) {
			e.printStackTrace();
		}

		// クッキーからレジID取得する
		Cookie[] cookies = request.getCookies();
		String stid = "";
		// クッキーから社員ＩＤをもってくる
		if (cookies != null) {
			for (Cookie cook : cookies) {
				if (cook.getName().equals("StId")) {
					stid = cook.getValue();
				}
			}
		}
		int Istid = Integer.parseInt(stid);// 社員ＩＤをＩＮＴ型に変換

		FusokuDAO dao = new FusokuDAO();
		try {
			MediaListBean = dao.TenpoKasidasi(pid, pkubn, Istid);
		} catch (SQLException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}
		HttpSession session = request.getSession();
		session.setAttribute("MediaListBean", MediaListBean);

		RequestDispatcher dispatch = request.getRequestDispatcher("tatenpokasidasi.jsp");
		dispatch.forward(request, response);
	}
}
