package dmx;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import bean.MemberBean;
import db.DBConnectUtil;
import db.KaiinDAO;

/**
 * Servlet implementation class MemberAddServlet
 */
@WebServlet("/MemberAddServlet")
public class MemberAddServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	DBConnectUtil dbc = new DBConnectUtil();
    /**
     * @see HttpServlet#HttpServlet()
     */
    public MemberAddServlet() {
        super();
        // TODO Auto-generated constructor stub
    }


	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
    @Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		Date date = new Date();
		SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");


		String 	uBirthday = null;
		String 	uBirthday1 = null;
		String 	uBirthday2 = null;
		String 	uBirthday3 = null;
		String	uName = null;
		String 	uNameKana = null;
		String	uGender = null;
		String  uJob = null;
		String	uPostal = null;
		String 	uAddress = null;
		String 	uAddress1 = null;
		String 	uAddress2 = null;
		String  uTel = null;
		String  uMail = null;
		String  uStartDay = null;
		String  uStatus = "1";





		try {
			uName = request.getParameter("name");
			uNameKana = request.getParameter("oname");
			uBirthday1 = request.getParameter("room");
			uBirthday2 = request.getParameter("tuki");
			if(Integer.parseInt(uBirthday2)>0 && Integer.parseInt(uBirthday2)<10){
				uBirthday2 = "0"+uBirthday2;
			}
			uBirthday3 = request.getParameter("hi");
			if(Integer.parseInt(uBirthday3)>0 && Integer.parseInt(uBirthday3)<10){
				uBirthday3 = "0"+uBirthday3;
			}
			uBirthday = uBirthday1+"-" +uBirthday2+"-"+ uBirthday3;
			uGender = request.getParameter("radio1");
			uJob = request.getParameter("radio2");
			uPostal = request.getParameter("yuubin");
			uAddress1 = request.getParameter("ken");
			uAddress2 = request.getParameter("tosi");
			uAddress = uAddress1 + uAddress2;
	        uStartDay = format.format(date);
			uTel = request.getParameter("denwa");
			uMail = request.getParameter("mail");
		} catch (Exception e) {
			e.printStackTrace();
		}

		MemberBean mem = new MemberBean();
		mem.setuName(uName);
		mem.setuNameKana(uNameKana);
		mem.setuBirthday(uBirthday);
		mem.setuGender(uGender);
		mem.setuJob(uJob);
		mem.setuPostal(uPostal);
		mem.setuAddress(uAddress);
		mem.setuTel(uTel);
		mem.setuMail(uMail);
		mem.setuStartTime(uStartDay);
		mem.setuStatus(uStatus);


		try{
			KaiinDAO dao = new KaiinDAO();
			dao.addMemberInfo(mem,request.getParameter("rid"));
		}catch(Exception e){
			e.printStackTrace();
		}
		RequestDispatcher dispatch = request.getRequestDispatcher("kaiinkanrikanryou.jsp");
		dispatch.forward(request, response);
    }
}
