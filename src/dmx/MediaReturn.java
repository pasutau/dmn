package dmx;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import db.DBConnectUtil;
import db.KasidasiDAO;

/**
 * Servlet implementation class MediaReturn
 */
@WebServlet("/MediaReturn")
public class MediaReturn extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		Connection con = DBConnectUtil.connect();


		//外BOXなら日付1足す
		SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date date = new Date();
		String sqlDate = format.format(date);

		int box = Integer.parseInt(request.getParameter("box"));
		if(box == 2){
			Calendar now = Calendar.getInstance();
			now.setTime(date);
			//1日足す
			now.add(Calendar.DATE, 1);
			date = now.getTime();
			// 今日の日付に1足す

		}
		System.out.println("box:1=内BOXレジ 2=外BOX");
		System.out.println("box=" + box);
		System.out.println("date=" + date);

		DBConnectUtil dbc = new DBConnectUtil();
		KasidasiDAO kasi = new KasidasiDAO();

		int soeji = 1;
		String mediaId[] = new String[20];
		String mediaMei[] = new String[20];

		// ＩＤ受け取り用配列初期化
		for (int j = 0; j < 20; j++) {
			mediaId[j] = "";
			mediaMei[j] = "";
		}

		// ＩＤ格納
		while (soeji < 21) {
			mediaId[soeji - 1] = request.getParameter("mediaid" + soeji);
			soeji += 1;
		}

		// メディア名取得
		soeji = 0;
		while (soeji < 20) {
			try {
				mediaMei[soeji] = kasi.mediaMeiShutoku(con, mediaId[soeji]);
			} catch (SQLException e) {
				e.printStackTrace();
			}
			soeji++;
		}

		soeji = 0;

		int entaisu = 0;//ｷﾞﾙﾃｨｶｳﾝﾄ
		// 返却処理呼び出し
		while (soeji < 20) {
			int entai = 0;//延滞日数
			if (mediaId[soeji] == null || mediaId[soeji].length() == 0) {
				System.out.println("mediaId[" + soeji + "]は空欄です");
			} else {
				System.out.println("mediaId[" + soeji + "]の返却処理をします " + mediaId[soeji]);
				try {
					//合計延滞日数が返ってくる
					entai = kasi.henkyaku(con, mediaId[soeji], sqlDate);
					entaisu+=entai;
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			soeji++;
		}
		dbc.commit(con);
		dbc.disconnect(con);

		HttpSession session = request.getSession();
		session.setAttribute("entaisu", entaisu);
		getServletContext().getRequestDispatcher("/mediahenkyakukanryo.jsp").forward(request, response);
	}

}