package dmx;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import bean.SyainBean;
import db.SyainDAO;

/**
 * Servlet implementation class SyainUpdateServlet
 */
@WebServlet("/SyainUpdateServlet")
public class SyainUpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public SyainUpdateServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		int  sId = 0;
		String	sName = null;
		String 	sNameKana = null;
		String	sTel = null;
		String  sAddress = null;
		String	sPosition = null;
		String	sTenpo = null;
		try {
			sId = Integer.parseInt(request.getParameter("sId"));
			sName = request.getParameter("sName");
			sNameKana = request.getParameter("sNameKana");
			sTel = request.getParameter("sTel");
			sAddress = request.getParameter("sAddress");
			sPosition = request.getParameter("sPossion");
			sTenpo = request.getParameter("sTenpo");
		} catch (Exception e) {
			e.printStackTrace();
		}

		SyainBean sem = new SyainBean();
		sem.setsId(sId);
		sem.setsName(sName);
		sem.setsNameKana(sNameKana);
		sem.setsTel(sTel);
		sem.setsAddress(sAddress);
		sem.setsPossion(sPosition);
		sem.setsTenpo(sTenpo);

		System.out.println(sId);
		try{
			SyainDAO dao = new SyainDAO();
			dao.updateSyainInfo(sem);
		}catch(Exception e){
			e.printStackTrace();
		}
		RequestDispatcher dispatch = request.getRequestDispatcher("syainzyouhoukosinkanryo.jsp");;
		dispatch.forward(request, response);
	}

}
