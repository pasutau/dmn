package dmx;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import bean.EntaiBean;
import bean.EntaiInfoBean;
import db.EntaiDAO;

/**
 * Servlet implementation class EntaiitiranServlet
 */
@WebServlet("/EntaiitiranServlet")
public class EntaiitiranServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private EntaiInfoBean entaiBean;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public EntaiitiranServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		Date date = new Date();
		String strNextDate = new SimpleDateFormat("yyyy-MM-dd").format(date);

		try{
			EntaiDAO dao = new EntaiDAO();
			entaiBean = dao.getNewEntaiHuman(strNextDate); //LIDをひろう
			ArrayList<EntaiBean> enInfoArray = entaiBean.getEntaiArray();
			for(EntaiBean rc : enInfoArray) {
				dao.EntaiAdd(rc.getLid(),strNextDate); //延滞者を表につっこむ
			}
			entaiBean = dao.getEntaiUpdateHuman();
			ArrayList<EntaiBean> en2InfoArray = entaiBean.getEntaiArray();
			for(EntaiBean rd : en2InfoArray) {
				System.out.println(rd.getLid());
				dao.EntaiEndUpdate(rd.getLid()); //返却した延滞者を画面から消す
			}
			/*------------------------------------------*/

			entaiBean = dao.getEntaiInfoBean();  //延滞者を拾う
			HttpSession session = request.getSession();
			session.setAttribute("enInfoBean", entaiBean);
			getServletContext().getRequestDispatcher("/entaiitiran.jsp").forward(request, response);
		}catch(Exception e){
			e.printStackTrace();
		}
	}
}
