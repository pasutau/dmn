package dmx;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import bean.UserRirekiListBean;
import db.DBConnectUtil;
import db.KasidasiDAO;

/*貸出履歴検索(ユーザが何を借りたかのクラス)*/
@WebServlet("/UserHistory")
public class UserHistory extends HttpServlet {
	private static final long serialVersionUID = 1L;

	DBConnectUtil dbc = new DBConnectUtil();
	KasidasiDAO  kasi = new KasidasiDAO();
	UserRirekiListBean uslist = new UserRirekiListBean();

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		Connection con = DBConnectUtil.connect();
		String  uId = request.getParameter("uid");//リクエスト送ってくる奴

		int userId = Integer.parseInt(uId);
		try {
			 uslist = kasi.userKasiRireki(con, userId);
		} catch (SQLException e) {
			e.printStackTrace();
		}

		HttpSession session = request.getSession();
		session.setAttribute("uslist", uslist);

		dbc.disconnect(con);

		getServletContext().getRequestDispatcher("/kasidasirireki.jsp").forward(request, response);
	}


}
