package dmx;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import db.DBConnectUtil;
import db.SyainDAO;

/**
 * Servlet implementation class SyainkengensakujyoServlet
 */
@WebServlet("/SyainkengensakujyoServlet")
public class SyainkengensakujyoServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	DBConnectUtil dbc = new DBConnectUtil();
	SyainDAO  kasi = new SyainDAO();
	Date date  = new Date();

    /**
     * @see HttpServlet#HttpServlet()
     */
    public SyainkengensakujyoServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
    	SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");

    	String	sid = request.getParameter("sId");
		String	sPrTime = null;
		String	hobby[] = request.getParameterValues("kanri");

		try {
			sPrTime = format.format(date);
			for(String no : hobby){
				SyainDAO dao = new SyainDAO();
				dao.Syainkengensakujyo(sid,no,sPrTime);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		RequestDispatcher dispatch = request.getRequestDispatcher("syainkengensakujyokanryo.jsp");
		dispatch.forward(request, response);
	}

}
