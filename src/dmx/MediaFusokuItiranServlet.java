package dmx;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import bean.FusokuListBean;
import db.DBConnectUtil;
import db.DBLogic;
import db.FusokuDAO;

//店舗ごとの不足メディアを取得する DVDごと ブルーレイごとに
@WebServlet("/MediaFusokuItiranServlet")
public class MediaFusokuItiranServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	DBConnectUtil dbc = new DBConnectUtil();
	FusokuDAO fd = new FusokuDAO();
	DBLogic dbl = new DBLogic();
	int tenpoId = 0;
	FusokuListBean fusokulist = new FusokuListBean();

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		Connection con = DBConnectUtil.connect();
		//クッキーからレジID取得する
		Cookie[] cookies = request.getCookies();
		String regiId = "";
		if(cookies != null  ){
			for(Cookie cook : cookies){
				if(cook.getName().equals("RegiId")){
					regiId = cook.getValue();
				}
			}
		}
		//店舗IDの取得
		try {
			 tenpoId = dbl.stIdShutoku(con, regiId);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		//店舗IDからどれ足りてないか持ってくる
		try {
			fusokulist = fd.FusokuList(con, tenpoId);
		}catch(Exception e){
			e.printStackTrace();
		}

		HttpSession session = request.getSession();
		session.setAttribute("fusokulist", fusokulist);
		session.setAttribute("tenpoId", tenpoId);

		dbc.disconnect(con);

		getServletContext().getRequestDispatcher("/fusokuItiran.jsp").forward(request, response);
	}

}
