package dmx;

import java.io.IOException;
import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import bean.LoanBean;
import bean.LoanapplicationListBean;
import db.DBConnectUtil;
import db.MediaDAO;

/**
 * Servlet implementation class LoandetailsServlet
 */
@WebServlet("/TTkasidasiServlet")
public class TTkasidasiServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	Connection con = DBConnectUtil.connect();
	MediaDAO md = new MediaDAO();
	LoanBean loanbean = new LoanBean();
	LoanapplicationListBean lListbean = new LoanapplicationListBean();
	String pid = "";
	int shid = 0;
	int mytenid = 0;
	String shopname = "";
	String shouhinname = "";
	String mid = "";
	String lastdate = "";

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		Date date = new Date();
		SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");// 日付の処理するためのやつ
		mid = request.getParameter("mediaid");//受け取るメディアＩＤ
		pid = request.getParameter("pid");// 貸し出している商品ＩＤ
		shid = Integer.parseInt(request.getParameter("shid"));// 貸し出している店舗ID
		mytenid = Integer.parseInt(request.getParameter("mytenid"));// 自分の店舗ID
		shopname = request.getParameter("shopname");// 貸し出している店舗名
		shouhinname = request.getParameter("shouhinname");// 貸し出している商品名
		// 表示項目 → 店舗名、タイトル、メディアＩＤ、（入力）をとってくるよおお

		loanbean.setLenendTime(format.format(date));
		loanbean.setMediaid(mid);
		loanbean.setPid(pid);
		loanbean.setShid(mytenid);
		loanbean.setMytenpoid(mytenid);
		loanbean.setShopname(shopname);
		loanbean.setProductname(shouhinname);
		loanbean.setStatus("7");
		try {
			MediaDAO mddao = new MediaDAO();
			mddao.slenendUpdate(loanbean);
		} catch (Exception e) {
			e.printStackTrace();
		}

		getServletContext().getRequestDispatcher("/kasidasimediaukekan.jsp").forward(request, response);
	}

}
