package dmx;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import bean.EntaiBean;
import bean.RegisterBean;
import bean.RegisterInfo;
import bean.TighteningBean;
import db.RegisterDAO;





/**
 * Servlet implementation class RegisterKinsyu
 */
@WebServlet("/RegisterKinsyu")
public class RegisterKinsyu extends HttpServlet {
	private static final long serialVersionUID = 1L;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public RegisterKinsyu() {
        super();
    }

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		Date date = new Date();
		SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		String RtTime= format.format(date);

	int RegiId=0;
	int ShId=0;

	int count=0;
	int RtSales=0;//売上金
	int RtAmount=0;//レジ内金
	int RtLateFee=0;//延滞金
	int RtRefundAmount=0;//返金
	int RtAdmission=0;//入会金
	int RtDifference=0;//差異
	int Man=0;
	int Gosen=0;
	int Nisen=0;
	int Sen=0;
	int Gohyaku=0;
	int Hyaku=0;
	int Gozyu=0;
	int Zyu=0;
	int Go=0;
	int Iti=0;
	int zenamo=0;
	int rcnt =0;
	RegisterDAO dao = new RegisterDAO();


	try{
		RegiId = Integer.parseInt(request.getParameter("rid"));
		ShId = dao.stIdShutoku(request.getParameter("rid"));//店舗IDとってる
		Man = Integer.parseInt(request.getParameter("man")) * 10000;
		Gosen = Integer.parseInt(request.getParameter("gosen"))* 5000;
		Nisen = Integer.parseInt(request.getParameter("nisen")) * 2000;
		Sen = Integer.parseInt(request.getParameter("sen"))* 1000;
		Gohyaku = Integer.parseInt(request.getParameter("gohyaku"))* 500;
		Hyaku = Integer.parseInt(request.getParameter("hyaku"))* 100;
		Gozyu = Integer.parseInt(request.getParameter("gozyu"))* 50;
		Zyu = Integer.parseInt(request.getParameter("zyu"))* 10;
		Go = Integer.parseInt(request.getParameter("go"))* 5;
		Iti = Integer.parseInt(request.getParameter("iti"))* 1;
		RtAmount = Man+Gosen+Nisen+Sen+Gohyaku+Hyaku+Gozyu+Zyu+Go+Iti;

		/* 前回のデータを引っ張る */
		String rtime  = dao.getSeleTime(RegiId);
		rtime = rtime.substring(0, 19);
		System.out.println(rtime);
		rcnt = dao.getCont(rtime,RegiId);
		zenamo = dao.OldDate(RegiId,rcnt,rtime);
		rcnt++;
		/* まずデータ上の売上金と割引回数をひっぱる */
		RegisterInfo ri = new RegisterInfo();
		ri = dao.getSales(RegiId,RtTime);
		ArrayList<RegisterBean> enInfoArray = ri.getRegiArray();
		for(RegisterBean rc : enInfoArray) {
			RtSales = rc.getRsales();
			count = rc.getRcount();
		}

		/* 入会金をひっぱる */
		RtAdmission = dao.getAdmision(RtTime,RegiId) * 500;

		/* 延滞金をひっぱる */
		RtLateFee = dao.getLateFee(RtTime,RegiId,date) * 300;

		/* 返金をひっぱる */
		RtRefundAmount = dao.getRefund(RtTime,RegiId);

		/* 差異の計算 */
		RtDifference = RtAmount - RtSales  -  RtAdmission - RtLateFee + RtRefundAmount - zenamo;

		dao.addRezisime(RegiId,RtTime,RtSales,RtAmount,RtLateFee,RtRefundAmount,RtAdmission,RtDifference,rcnt);

			} catch (Exception e) {
				e.printStackTrace();
			}
	RequestDispatcher dispatch = request.getRequestDispatcher("kinsyukanryou.jsp");;
			dispatch.forward(request, response);
			}
		}

