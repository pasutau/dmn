package dmx;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import bean.ProductBean;
import db.DBConnectUtil;
import db.DBLogic;
import db.TitleTuikaDAO;

/**
 * Servlet implementation class MemberAddServlet
 */
@WebServlet("/TitleTuika")
public class TitleTuika extends HttpServlet {
	private static final long serialVersionUID = 1L;
	DBConnectUtil dbc = new DBConnectUtil();
    /**
     * @see HttpServlet#HttpServlet()
     */
    public TitleTuika() {
        super();
    }

    @Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Connection con = DBConnectUtil.connect();
		DBLogic dbl = new DBLogic();

		String PId = null;
		try {
			PId = dbl.newAcId(con, "product");
		} catch (SQLException e2) {
			e2.printStackTrace();
		}
		String 	Name = null;
		String 	NameKana = null;
		String 	Time = null;

		String 	Start = null;
		String 	Start1 = null;
		String 	Start2 = null;
		String 	Start3 = null;

		String	kubun = null;
		String 	Shuen = null;
		String	ShuenKana = null;
		String  Kantoku = null;
		String	KantokuKana = null;


		try {
			Name = request.getParameter("pName");
			NameKana = request.getParameter("pNameKana");
			Time = request.getParameter("pTime");

			Start1 = request.getParameter("room");
			Start2 = request.getParameter("tuki");
			if(Integer.parseInt(Start2)>0 && Integer.parseInt(Start2)<10){
				Start2 = "0"+Start2;
			}
			Start3 = request.getParameter("hi");
			if(Integer.parseInt(Start3)>0 && Integer.parseInt(Start3)<10){
				Start3 = "0"+Start3;
			}
			Start = Start1+"-" +Start2+"-"+ Start3;

			kubun = request.getParameter("kubun");
			System.out.println("kubun="  + kubun);
			Shuen = request.getParameter("pShuen");
			ShuenKana = request.getParameter("pShuenKana");
			Kantoku = request.getParameter("pKantoku");
			KantokuKana = request.getParameter("pKantokuKana");
		} catch (Exception e) {
			e.printStackTrace();
		}

		ProductBean pr = new ProductBean();

		pr.setpId(PId);
		pr.setpName(Name);
		pr.setpNameKana(NameKana);
		pr.setRecordingTime(Time);
		pr.setpStartTime(Start);
		pr.setnId(kubun);
		pr.setStarring(Shuen);
		pr.setStarringKana(ShuenKana);
		pr.setSuperVision(Kantoku);
		pr.setSupervisionKana(KantokuKana);
		try{
			TitleTuikaDAO dao = new TitleTuikaDAO();
			dao.addTitleInfo(pr);
		}catch(Exception e){
			e.printStackTrace();
		}
		RequestDispatcher dispatch = request.getRequestDispatcher("titletuikakanryou.jsp");
		//次こことID発行メソッド
		dispatch.forward(request, response);
    }
}
