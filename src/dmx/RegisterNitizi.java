package dmx;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import bean.DayprocessingBean;
import db.RegisterDAO;

/**
 * Servlet implementation class RegisterNitizi
 */
@WebServlet("/RegisterNitizi")
public class RegisterNitizi extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public RegisterNitizi() {
        super();
        // TODO Auto-generated constructor stub
    }

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		Date date = new Date();
		SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

	int ShId=0;//店舗ID
	String DpTime=format.format(date);
	int DpSales=0;//売上
	int DpAmount=0;//レジ内金額
	int DpLateFee=0;//延滞
	int DpRefundAmount=0;//返金
	int DpAdmission=0;//入会金
	int ProfitAndLoss=0;//雑役、雑損


	try{
		ShId = Integer.parseInt(request.getParameter("shid"));
		DpSales = Integer.parseInt(request.getParameter("dsales"));
		DpAmount = Integer.parseInt(request.getParameter("damount"));
		DpLateFee = Integer.parseInt(request.getParameter("dlatefee"));
		DpRefundAmount = Integer.parseInt(request.getParameter("dfund"));
		DpAdmission = Integer.parseInt(request.getParameter("dadmission"));
		ProfitAndLoss = Integer.parseInt(request.getParameter("profit"));

	} catch (Exception e) {
		e.printStackTrace();
	}

	DayprocessingBean dyp = new DayprocessingBean();
	dyp.setShId(ShId);
	dyp.setDpTime(DpTime);
	dyp.setDpSales(DpSales);
	dyp.setDpAmount(DpAmount);
	dyp.setDpLateFee(DpLateFee);
	dyp.setDpRefundAmount(DpRefundAmount);
	dyp.setDpAdmission(DpAdmission);
	dyp.setProfitAndLoss(ProfitAndLoss);
	RegisterDAO dao = new RegisterDAO();
	dao.addDayprocessing(dyp);

	RequestDispatcher dispatch = request.getRequestDispatcher("nitizikanryou.jsp");;
	dispatch.forward(request, response);
	}
}



