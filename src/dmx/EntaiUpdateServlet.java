package dmx;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import bean.EntaiBean;
import db.EntaiDAO;

/**
 * Servlet implementation class EntaiUpdateServlet
 */
@WebServlet("/EntaiUpdateServlet")
public class EntaiUpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public EntaiUpdateServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		int count = 1;
		Cookie[] cookies = request.getCookies();
		String stid = "";
		if(cookies != null){
			for(Cookie cook : cookies){
				if(cook.getName().equals("StId")){
					stid = cook.getValue();
				}

			}
		}
		HttpSession session = request.getSession();
		@SuppressWarnings("unchecked")
		ArrayList<EntaiBean> enInfoArray = (ArrayList<EntaiBean>) session.getAttribute("enInfoArray");
		try{
			for(EntaiBean rc : enInfoArray) {
				if(request.getParameter("status"+count).equals("2")){
					EntaiDAO dao = new EntaiDAO();
					dao.EntaiUpdate(rc.getLid(),request.getParameter("status"+count),stid,rc.getUid());
				}
				count++;
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		RequestDispatcher dispatch = request.getRequestDispatcher("entaikousinkanryou.jsp");
		dispatch.forward(request, response);
	}


}
