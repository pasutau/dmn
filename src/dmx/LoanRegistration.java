package dmx;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import db.BlackHanteiDAO;
import db.DBConnectUtil;
import db.KasidasiDAO;

/*貸出情報登録サーブレット*/
@WebServlet("/LoanRegistration")
public class LoanRegistration extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		DBConnectUtil dbc = new DBConnectUtil();
		KasidasiDAO kasi = new KasidasiDAO();
		Date date = new Date();
		java.sql.Date sqlDate = new java.sql.Date(date.getTime());
		SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		String nowdate = format.format(date);
		BlackHanteiDAO bh = new BlackHanteiDAO();


		int gokei = 0;
		int uId = 0;
		int newLId = 0;
		int soeji = 1;
		int USiyousu = 0;

		String mediaId[] = new String[20];
		String hakusu[] = new String[20];
		String mediaMei[] = new String[20];
		int ryokin[] = new int[20];

		String url = "";


		Connection con = DBConnectUtil.connect();

		// 初期化
		for (int j = 0; j < 20; j++) {
			mediaId[j] = "";
			hakusu[j] = "";
			mediaMei[j] = "";
			ryokin[j] = 0;
		}

		// 受け取りId
		uId = Integer.parseInt(request.getParameter("uid"));
		System.out.println("uid="+uId);

		// 受け取りスタンプ使用数
		USiyousu = Integer.parseInt(request.getParameter("siyousu"));
		System.out.println("スタンプ使用数=" + USiyousu);

		int cnt = 0;
		// 受け取りmediaId
		soeji = 1;
		while (soeji < 21) {
			mediaId[soeji - 1] = request.getParameter("mediaid" + soeji);
			hakusu[soeji - 1] = request.getParameter("hi" + soeji);
			System.out.println("受け取りmediaId mediaId[" + soeji + "-1] = " + mediaId[soeji-1]);

			if (mediaId[soeji - 1] != "") {
				cnt++;
			}
			System.out.println("cnt(何件あるか)= " + cnt);
			soeji += 1;
		}

		// ブラック判定
		boolean ret = bh.hantei(uId);
		if (ret == false && cnt > 1) {
			System.out.println("blackByeByeの処理入った");
			url = "/BlackByeBye.jsp";
		} else {
			System.out.println("Blackじゃないほう入った");
			// 名前selectしマース
			soeji = 0;
			while (soeji < 20) {
				if (mediaId[soeji] != "") {
					try {
						mediaMei[soeji] = kasi.mediaMeiShutoku(con, mediaId[soeji]);
						System.out.println("mediaMei[" + soeji + "]=" + mediaMei[soeji]);
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
				soeji += 1;
			}
			System.out.println("料金計算直前");
			// 料金selectしマース
			soeji = 0;
			while (soeji < 20) {
				if (mediaId[soeji] != "") {
					try {
						ryokin[soeji] = kasi.ryokinShutoku(con, mediaId[soeji], hakusu[soeji]);
						gokei += ryokin[soeji];
						System.out.println("gokei="  + gokei);
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
				soeji += 1;
			}

			System.out.println("店員ＩＤほしい");
			// 店員ＩＤ拾ってくる
			Cookie[] cookies = request.getCookies();
			String stid = "";
			String rid = "";
			if (cookies != null) {
				for (Cookie cook : cookies) {
					if (cook.getName().equals("StId")) {
						stid = cook.getValue();
					}
					if (cook.getName().equals("RegiId")) {
						rid = cook.getValue();
					}
				}
			}
			System.out.println("stid=" + stid);
			System.out.println("rid=" + rid);

			// いよいよInsert(lend)
			// その前に型あわせよう
			int StId = Integer.parseInt(stid);
			int RId = Integer.parseInt(rid);

			// 割引処理
			gokei = gokei - USiyousu * 50;
			System.out.println("kasi.lendTouroku(" + uId + "," + date + "," + StId + "," + RId + "," + gokei + ","
					+ USiyousu + ")");

			kasi.lendTouroku(con, uId, nowdate, StId, RId, gokei, USiyousu);

			// 次明細
			soeji = 0;
			while (soeji < 20) {
				Date newTime = null;
				if (mediaId[soeji] != "") {
					try {
						// 日付計算

						try {
							Calendar now = Calendar.getInstance();
							// Calendarクラスのインスタンスを生成

							now.setTime(date);
							// Date型からカレンダー型に変換

							int newHakusu = Integer.parseInt(hakusu[soeji]);
							now.add(Calendar.DATE, newHakusu);
							// 指定された日時に、指定された日にちを足す(手元にあろうバーコードﾋﾟｯってやった数字が泊数)
							// rf_daysにまんま数値入ってる前提
							// 当日→1 7泊8日→7

							newTime = now.getTime();
							// カレンダー型からDate型に変換

						} catch (Exception e) {
							e.printStackTrace();
						}
						// java.sql.Date sqlDate = new
						// java.sql.Date(newTime.getTime());
						String sqld = format.format(newTime);
						kasi.lendInfTouroku(con, mediaId[soeji], sqld);
					} catch (SQLException e) {
						e.printStackTrace();
					}
				} else {
					System.out.println("mediaId[" + soeji + "]は空欄です");
				}
				soeji += 1;
			}

			url = "/mediakasidasikanryo.jsp";
			dbc.commit(con);

			dbc.disconnect(con);

			HttpSession session = request.getSession();
			session.setAttribute("mediaMei[]", mediaMei);
			session.setAttribute("ryokin[]", ryokin);
			session.setAttribute("siyousu", USiyousu);
			session.setAttribute("gokei", gokei);
		}
		getServletContext().getRequestDispatcher(url).forward(request, response);

	}
}