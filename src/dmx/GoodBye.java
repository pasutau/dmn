package dmx;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/*ログアウト用クラス*/
@WebServlet("/GoodBye")
public class GoodBye extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		Cookie cookStId = new Cookie("StId","");
		Cookie cookStName = new Cookie("StName","");
		cookStId.setMaxAge(0);
		cookStName.setMaxAge(0);
		response.addCookie(cookStId);
		response.addCookie(cookStName);


		/*タイトルに戻しちゃうよ☆*/
		String url = "./index.jsp";
	    response.sendRedirect(url);

	}

}
