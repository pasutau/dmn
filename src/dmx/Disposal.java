
package dmx;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import bean.MediaBean;
import bean.MediadisposalinfoBean;
import db.DBConnectUtil;
import db.MediaDAO;

/**
 * Servlet implementation class Disposal
 */
@WebServlet("/Disposal")
public class Disposal extends HttpServlet {
	private static final long serialVersionUID = 1L;


	MediadisposalinfoBean mdinfoBean = new MediadisposalinfoBean();

	MediaDAO md = new MediaDAO();
	java.util.Date d1 = new java.util.Date();
	java.sql.Date sqlDate = new java.sql.Date(d1.getTime());

	String mediaId="";
	int soeji = 1;

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		Connection con = DBConnectUtil.connect();
		mediaId = request.getParameter("mediaid");

		////////////////////////////////////// レコードに廃棄するDVDの情報をレコードにぶっこみまくるよ//////////////////////////////////////////////
			try {
				MediaBean mdBean = new MediaBean();
				mdBean.setmId(mediaId);// メディアＩＤをセットしてる
				mdBean.setMedianame(md.haikimediaPull(con, mediaId));// メディアのタイトルをセットしてる
				mdBean.setHaikiday(sqlDate);// 廃棄日をセットしてる
				mdinfoBean.addMediainfo(mdBean);// レコードにいろんな情報をまとめてセットしてる
			} catch (SQLException e) {
					e.printStackTrace();
			}
		///////////////////////////////////////////////////////////// レコードに廃棄するDVDの情報をデータベースに登録する////////////////////////////////////////////////
		ArrayList<MediaBean> mdRecordArray = mdinfoBean.getMediaRecordArray();

		for (MediaBean rcd : mdRecordArray) {
			md.haikiInsert(con, sqlDate, rcd.getmId());
		}

		HttpSession session = request.getSession();
		session.setAttribute("mdinfoBean", mdinfoBean);
		getServletContext().getRequestDispatcher("/haikitourokukanryo.jsp").forward(request, response);
	}

}
