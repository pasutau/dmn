package dmx;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import db.RegisterDAO;

/**
 * Servlet implementation class Akaden
 */
@WebServlet("/Akaden")
public class Akaden extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Akaden() {
        super();
        // TODO Auto-generated constructor stub
    }


	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		Date date = new Date();
		SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		String RtTime= format.format(date);
		int amount =0;
		if(request.getParameter("flag").equals("1")){
			try{
				RegisterDAO dao = new RegisterDAO();
				int RegiId = Integer.parseInt(request.getParameter("rid"));
				String mid = request.getParameter("mid");
				amount = dao.addHenkin(RtTime,RegiId,mid);
			}catch(Exception e){
				e.printStackTrace();
			}
			HttpSession session = request.getSession();
			  session.setAttribute("amount", amount);
			  ServletContext context = this.getServletContext();
			  RequestDispatcher dispatcher = context.getRequestDispatcher("/akadenkanryou.jsp");
			  dispatcher.forward(request,response);

		}
	}

}
