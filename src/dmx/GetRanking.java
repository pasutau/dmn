package dmx;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import bean.RankingListBean;
import bean.UserRirekiListBean;
import db.DBConnectUtil;
import db.KasidasiDAO;
import db.RankingDAO;

@WebServlet("/GetRanking")
public class GetRanking extends HttpServlet {
	private static final long serialVersionUID = 1L;

	DBConnectUtil dbc = new DBConnectUtil();
	RankingDAO rank = new RankingDAO();
	RankingListBean ranklist = new RankingListBean();

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		Connection con = DBConnectUtil.connect();

		try {
			 ranklist = rank.ranking(con);
		} catch (SQLException e) {
			e.printStackTrace();
		}

		HttpSession session = request.getSession();
		session.setAttribute("ranklist", ranklist);

		dbc.disconnect(con);

		getServletContext().getRequestDispatcher("/ranking.jsp").forward(request, response);
	}


}
