package bean;

import java.io.Serializable;

public class FusokuRecordBean implements Serializable {
	private static final long serialVersion = 1L;

	private String pid;
	private String pName;
	private String mCategory;
	private int su;
	private int kubun;

	public String getpName() {
		return pName;
	}
	public void setpName(String pName) {
		this.pName = pName;
	}
	public int getSu() {
		return su;
	}
	public void setSu(int su) {
		this.su = su;
	}
	public String getmCategory() {
		return mCategory;
	}
	public void setmCategory(String mCategory) {
		this.mCategory = mCategory;
	}
	public void setKubun(int kubun) {
		this.kubun = kubun;
	}
	public int getKubun(){
		return kubun;
	}
	public void setpId(String pid) {
		this.pid = pid;
	}
	public String getpId(){
		return pid;
	}
}
