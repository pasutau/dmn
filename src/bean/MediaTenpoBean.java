package bean;

import java.io.Serializable;

public class MediaTenpoBean implements Serializable {
	private static final long serialVersionUID = 1L;


	private String pId;//メディア番号
	private String pName; //メディア名
	private String mCategory;//DVD/BR区分
	private int shId; //店舗ID
	private String shName;//店舗名
	private int sCount; //商品の本数


	//Constructor
    public MediaTenpoBean() {
    	super();
    }

	public String getpId() {
		return pId;
	}

	public void setpId(String pid2) {
		this.pId = pid2;
	}

	public String getpName() {
		return pName;
	}

	public void setpName(String pName) {
		this.pName = pName;
	}

	public String getmCategory() {
		return mCategory;
	}

	public void setmCategory(String mCategory) {
		this.mCategory = mCategory;
	}

	public String getShName() {
		return shName;
	}

	public void setShName(String shName) {
		this.shName = shName;
	}

	public int getsCount() {
		return sCount;
	}

	public void setsCount(int sCount) {
		this.sCount = sCount;
	}


	public int getShId() {
		return shId;
	}

	public void setshId(int sid) {
		this.setshId(sid);
	}

}
