package bean;

import java.io.Serializable;
import java.util.Date;

public class LendBean implements Serializable {
	private static final long serialVersionUID = 1L;

	private int lId;
	private int uId;
	private Date lTime;
	private int stId;
	private int registerId;
	private int lendFee;
	private int discount;


	public LendBean() {
		// TODO 自動生成されたコンストラクター・スタブ
	}

	public int getlId() {
		return lId;
	}

	public void setlId(int lId) {
		this.lId = lId;
	}

	public int getuId() {
		return uId;
	}

	public void setuId(int uId) {
		this.uId = uId;
	}

	public Date getlTime() {
		return lTime;
	}

	public void setlTime(Date lTime) {
		this.lTime = lTime;
	}

	public int getStId() {
		return stId;
	}

	public void setStId(int stId) {
		this.stId = stId;
	}

	public int getRegisterId() {
		return registerId;
	}

	public void setRegisterId(int registerId) {
		this.registerId = registerId;
	}

	public int getLendFee() {
		return lendFee;
	}

	public void setLendFee(int lendFee) {
		this.lendFee = lendFee;
	}

	public int getDiscount() {
		return discount;
	}

	public void setDiscount(int discount) {
		this.discount = discount;
	}

}
