package bean;

import java.io.Serializable;

public class SyainBean implements Serializable {
	private static final long serialVersionUID = 1L;
	// 社員情報 //
	private int sId;		// 社員ID
	private String	sName;	//名前
	private String 	sNameKana; //かな
	private String	sTenpo; // 店舗
	private String	sTel; // 電話
	private String	sAddress; // 住所
	private String	sPossion; // 職位
	private String	sStartTime; // 入社日
	private String	sEndTime;	// 退社日
	private String	shName;	//店舗名


	public String getShName() {
		return shName;
	}


	public void setShName(String shName) {
		this.shName = shName;
	}


	//Constructor
    public SyainBean() {
    	super();
    }


	public int getsId() {
		return sId;
	}


	public void setsId(int sId) {
		this.sId = sId;
	}


	public String getsName() {
		return sName;
	}


	public void setsName(String sName) {
		this.sName = sName;
	}


	public String getsNameKana() {
		return sNameKana;
	}


	public void setsNameKana(String sNameKana) {
		this.sNameKana = sNameKana;
	}


	public String getsTenpo() {
		return sTenpo;
	}


	public void setsTenpo(String sTenpo) {
		this.sTenpo = sTenpo;
	}


	public String getsTel() {
		return sTel;
	}


	public void setsTel(String sTel) {
		this.sTel = sTel;
	}


	public String getsAddress() {
		return sAddress;
	}


	public void setsAddress(String sAddress) {
		this.sAddress = sAddress;
	}


	public String getsPossion() {
		return sPossion;
	}


	public void setsPossion(String sPossion) {
		this.sPossion = sPossion;
	}


	public String getsStartTime() {
		return sStartTime;
	}


	public void setsStartTime(String sStartTime) {
		this.sStartTime = sStartTime;
	}


	public String getsEndTime() {
		return sEndTime;
	}


	public void setsEndTime(String sEndTime) {
		this.sEndTime = sEndTime;
	}
}
