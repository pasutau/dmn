package bean;

import java.io.Serializable;
import java.util.ArrayList;

public class EntaiInfoBean implements Serializable{
	private static final long serialVersionUID = 1L;
	private ArrayList<EntaiBean> enaray;

	public EntaiInfoBean() {
		enaray= new ArrayList<EntaiBean>();
	}

	public void addEntaiRecord(EntaiBean obj) {
		enaray.add(obj);
	}

	public int getArraySize() {
		return enaray.size();
	}

	public ArrayList<EntaiBean> getEntaiArray() {
		return enaray;
	}

	public void setEntaiRecordArray(ArrayList<EntaiBean> enaray) {
		this.enaray = enaray;
	}
}
