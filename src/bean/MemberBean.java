package bean;

import java.io.Serializable;

public class MemberBean implements Serializable {
	private static final long serialVersionUID = 1L;
///////会員情報//////////////////
	private int 	uId;
	private String 	uBirthday;
	private String	uName;
	private String 	uNameKana;
	private String	uGender;
	private String	uPostal;//郵便番号
	private String 	uAddress;//住所
	private String	uTel;
	private String	uMail;
	private String  uJob;
	private String	uStartTime;//入会年月日
	private String	uStatus;//会員のステータス(1:有効 2:失効 3:退会)


	//Constructor
    public MemberBean() {
    	super();
    }

	public int getuId() {
		return uId;
	}

	public void setuId(int uId) {
		this.uId = uId;
	}

	public String getuBirthday() {
		return uBirthday;
	}

	public void setuBirthday(String uBirthday) {
		this.uBirthday = uBirthday;
	}

	public String getuName() {
		return uName;
	}

	public void setuName(String uName) {
		this.uName = uName;
	}

	public String getuNameKana() {
		return uNameKana;
	}

	public void setuNameKana(String uNameKana) {
		this.uNameKana = uNameKana;
	}

	public String getuGender() {
		return uGender;
	}

	public void setuGender(String uGender) {
		this.uGender = uGender;
	}

	public String getuPostal() {
		return uPostal;
	}

	public void setuPostal(String uPostal) {
		this.uPostal = uPostal;
	}

	public String getuAddress() {
		return uAddress;
	}

	public void setuAddress(String uAddress) {
		this.uAddress = uAddress;
	}

	public String getuTel() {
		return uTel;
	}

	public void setuTel(String uTel) {
		this.uTel = uTel;
	}

	public String getuMail() {
		return uMail;
	}

	public void setuMail(String uMail) {
		this.uMail = uMail;
	}

	public String getuJob() {
		return uJob;
	}

	public void setuJob(String uJob) {
		this.uJob = uJob;
	}

	public String getuStartTime() {
		return uStartTime;
	}

	public void setuStartTime(String uStartTime) {
		this.uStartTime = uStartTime;
	}

	public String getuStatus() {
		return uStatus;
	}

	public void setuStatus(String uStatus) {
		this.uStatus = uStatus;
	}

}
