package bean;

import java.io.Serializable;
import java.util.ArrayList;

public class BurakkurisutoInfoBean implements Serializable{
	private static final long serialVersionUID = 1L;
	private ArrayList<BurakkurisutoBean> blaray;

	public BurakkurisutoInfoBean() {
		blaray= new ArrayList<BurakkurisutoBean>();
	}

	public void addBurakkuRecord(BurakkurisutoBean obj) {
		blaray.add(obj);
	}

	public int getArraySize() {
		return blaray.size();
	}

	public ArrayList<BurakkurisutoBean> getBurakkuArray() {
		return blaray;
	}

	public void setBurakkuRecordArray(ArrayList<BurakkurisutoBean> blaray) {
		this.blaray = blaray;
	}
}
