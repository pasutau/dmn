package bean;

import java.io.Serializable;
import java.util.Date;

public class UserRirekiRecordBean implements Serializable {
	private static final long serialVersion = 1L;
	private Date kasidasi;
	private Date henkyaku;
	private String title;

	public UserRirekiRecordBean() {
		super();
	}

	public Date getKasidasi() {
		return kasidasi;
	}

	public void setKasidasi(Date kasidasi) {
		this.kasidasi = kasidasi;
	}

	public Date getHenkyaku() {
		return henkyaku;
	}

	public void setHenkyaku(Date henkyaku) {
		this.henkyaku = henkyaku;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}


}
