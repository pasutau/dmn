package bean;

import java.sql.Date;

public class LoanBean {
	private static final long serialVersionUID = 1L;

	/////////////////////////// 申請一覧にのっける情報をもってる///////////////////////
	private String lenstartday;// 貸出開始日
	private int slid;// 店舗貸出ＩＤ
	private String pid;// 商品番号
	private String mediaid;// メディア番号
	private int slshid;// 借入先店舗ＩＤ
	private int shid;// 貸してって言ってきてる店舗ＩＤ
	private Date firstTime;// 貸出開始日
	private String planTime;// 貸出終了予定日
	private Date endTime;// 貸出終了日
	private String lenendTime;// 貸出終了日（文字）
	private String status;// ステータス
	private String statusname;//ステータス名称
	private int mytenpoid;// 自店舗ＩＤ
	private String mytenponame;// 自店舗名
	private String productname;// 商品名
	private String shopname;// 店舗名

	////////////////////////////////////////////////////////////////////////////

	public String getMediaid() {
		return mediaid;
	}

	public void setMediaid(String mediaid) {
		this.mediaid = mediaid;
	}

	public String getProductname() {
		return productname;
	}

	public void setProductname(String productname) {
		this.productname = productname;
	}

	public String getShopname() {
		return shopname;
	}

	public void setShopname(String shopname) {
		this.shopname = shopname;
	}

	public int getShid() {
		return shid;
	}

	public void setShid(int shid) {
		this.shid = shid;
	}

	public String getMytenponame() {
		return mytenponame;
	}

	public void setMytenponame(String mytenponame) {
		this.mytenponame = mytenponame;
	}

	public int getMytenpoid() {
		return mytenpoid;
	}

	public void setMytenpoid(int myid) {
		this.mytenpoid = myid;
	}

	public int getSlid() {
		return slid;
	}

	public void setSlid(int slid) {
		this.slid = slid;
	}

	public String getPid() {
		return pid;
	}

	public void setPid(String pid) {
		this.pid = pid;
	}

	public int getSlshid() {
		return slshid;
	}

	public void setSlshid(int slshid) {
		this.slshid = slshid;
	}

	public Date getFirstTime() {
		return firstTime;
	}

	public void setFirstTime(Date firstTime) {
		this.firstTime = firstTime;
	}

	public String getLenendTime() {
		return lenendTime;
	}

	public void setLenendTime(String lenendTime) {
		this.lenendTime = lenendTime;
	}

	public String getPlanTime() {
		return planTime;
	}

	public void setPlanTime(String planTime) {
		this.planTime = planTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getStatusname() {
		return statusname;
	}

	public void setStatusname(String statusname) {
		this.statusname = statusname;
	}

	public String getLenstartday() {
		return lenstartday;
	}

	public void setLenstartday(String lenstartday) {
		this.lenstartday = lenstartday;
	}
}
