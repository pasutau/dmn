package bean;

import java.io.Serializable;
import java.util.Date;

public class StaffBean implements Serializable {
	private static final long serialVersionUID = 1L;

	private int stId;
	private String stName;
	private String stNameKana;
	private int shId;
	private String shTel;
	private String shAddress;
	private String position;
	private Date stStartTime;
	private Date stEndTime;


	// Constructor
	public StaffBean() {
	}

	public int getStId() {
		return stId;
	}

	public void setStId(int stId) {
		this.stId = stId;
	}

	public String getStName() {
		return stName;
	}

	public void setStName(String stName) {
		this.stName = stName;
	}

	public String getStNameKana() {
		return stNameKana;
	}

	public void setStNameKana(String stNameKana) {
		this.stNameKana = stNameKana;
	}

	public int getShId() {
		return shId;
	}

	public void setShId(int shId) {
		this.shId = shId;
	}

	public String getShTel() {
		return shTel;
	}

	public void setShTel(String shTel) {
		this.shTel = shTel;
	}

	public String getShAddress() {
		return shAddress;
	}

	public void setShAddress(String shAddress) {
		this.shAddress = shAddress;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public Date getStStartTime() {
		return stStartTime;
	}

	public void setStStartTime(Date stStartTime) {
		this.stStartTime = stStartTime;
	}

	public Date getStEndTime() {
		return stEndTime;
	}

	public void setStEndTime(Date stEndTime) {
		this.stEndTime = stEndTime;
	}
}

