package bean;

public class EntaiBean {

	private int lid;
	private int uid;
	private String uname;
	private String	uTel;
	private int shid;
	private String shname;
	private String dcflag;

	public String getuTel() {
		return uTel;
	}
	public void setuTel(String uTel) {
		this.uTel = uTel;
	}
	public int getLid() {
		return lid;
	}
	public void setLid(int lid) {
		this.lid = lid;
	}
	public int getUid() {
		return uid;
	}
	public void setUid(int uid) {
		this.uid = uid;
	}
	public String getUname() {
		return uname;
	}
	public void setUname(String uname) {
		this.uname = uname;
	}
	public int getShid() {
		return shid;
	}
	public void setShid(int shid) {
		this.shid = shid;
	}
	public String getShname() {
		return shname;
	}
	public void setShname(String shname) {
		this.shname = shname;
	}
	public String getDcflag() {
		return dcflag;
	}
	public void setDcflag(String dcflag) {
		this.dcflag = dcflag;
	}
}
