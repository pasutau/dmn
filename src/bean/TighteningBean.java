package bean;

import java.io.Serializable;


public class TighteningBean implements Serializable {
	private static final long serialVersionUID = 1L;

	private int RegiId;	//レジ番号
	private String RtDay; //レジを閉めた日付
	private String RtTime; //レジを閉めた時間
	private int RtSales; //売り上げ金額
	private int RtAmount; //レジの中にある金額
	private int RtLateFee;//レジごとの延滞金
	private int RtRefundAmount;//レジごとの返金
	private int RtAdmission;;//レジごとの入会金
	private int RtDifference;//レジごとの差異a

	// Constructor
	public TighteningBean() {
	}

	public int getRegiId() {
		return RegiId;
	}

	public void setRegiId(int RegiId) {
		this.RegiId = RegiId;
	}

	public String getRtDay() {
		return RtDay;
	}

	public void setRtDay(String RtDay) {
		this.RtDay = RtDay;
	}

	public String getRtTime() {
		return RtTime;
	}

	public void setRtTime(String RtTime) {
		this.RtTime = RtTime;
	}

	public int getRtSales() {
		return RtSales;
	}

	public void setRtSales(int RtSales) {
		this.RtSales = RtSales;
	}

	public int getRtAmount() {
		return RtAmount;
	}

	public void setRtAmount(int RtAmount) {
		this.RtAmount = RtAmount;
	}

	public int getRtLateFee() {
		return RtLateFee;
	}

	public void setRtLateFee(int RtLateFee) {
		this.RtLateFee = RtLateFee;
	}

	public int getRtRefundAmount() {
		return RtRefundAmount;
	}

	public void setRtRefundAmount(int RtRefundAmount) {
		this.RtRefundAmount = RtRefundAmount;
	}

	public int getRtAdmission() {
		return RtAdmission;
	}

	public void setRtAdmission(int RtAdmission) {
		this.RtAdmission = RtAdmission;
	}

	public int getRtDifference() {
		return RtDifference;
	}

	public void setRtDifference(int RtDifference) {
		this.RtDifference = RtDifference;
	}

}