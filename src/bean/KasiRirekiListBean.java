package bean;

import java.io.Serializable;
import java.util.ArrayList;

public class KasiRirekiListBean implements Serializable {
	private static final long serialVersionUID = 1L;
	private ArrayList<KasiRirekiBean> kasiary;

	public KasiRirekiListBean() {
		kasiary = new ArrayList<KasiRirekiBean>();
	}

	public void addKasiRireki(KasiRirekiBean obj){
		kasiary.add(obj);
	}

	public int getArraySize(){
		return kasiary.size();
	}

	public ArrayList<KasiRirekiBean> getKasiRireki(){
		return kasiary;
	}

 	public void setKasiRireki(ArrayList<KasiRirekiBean> kasiary){
 		this.kasiary = kasiary;
 	}

}
