package bean;

import java.io.Serializable;

public class ProductBean implements Serializable {
	private static final long serialVersionUID = 1L;

	private String pId;
	private String pName;//タイトル
	private String pNameKana;//タイトルかな
	private String recordingTime;//収録時間
	private String pStartTime;//レンタル開始日
	private String nId;//新旧区分
	private String Starring;//主演
	private String starringKana;//主演かな
	private String superVision;//監督
	private String supervisionKana;//監督かな


	// Constructor
	public ProductBean() {
	}



	public String getpId() {
		return pId;
	}

	public void setpId(String pId) {
		this.pId = pId;
	}

	public String getpName() {
		return pName;
	}

	public void setpName(String pName) {
		this.pName = pName;
	}

	public String getpNameKana() {
		return pNameKana;
	}

	public void setpNameKana(String pNameKana) {
		this.pNameKana = pNameKana;
	}

	public String getRecordingTime() {
		return recordingTime;
	}

	public void setRecordingTime(String recordingTime) {
		this.recordingTime = recordingTime;
	}

	public String getpStartTime() {
		return pStartTime;
	}

	public void setpStartTime(String pStartTime) {
		this.pStartTime = pStartTime;
	}

	public String getnId() {
		return nId;
	}

	public void setnId(String nId) {
		this.nId = nId;
	}

	public String getStarring() {
		return Starring;
	}

	public void setStarring(String starring) {
		Starring = starring;
	}

	public String getStarringKana() {
		return starringKana;
	}

	public void setStarringKana(String starringKana) {
		this.starringKana = starringKana;
	}

	public String getSuperVision() {
		return superVision;
	}

	public void setSuperVision(String superVision) {
		this.superVision = superVision;
	}

	public String getSupervisionKana() {
		return supervisionKana;
	}

	public void setSupervisionKana(String supervisionKana) {
		this.supervisionKana = supervisionKana;
	}
}

