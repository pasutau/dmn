package bean;

import java.io.Serializable;
import java.util.ArrayList;

public class MemberInfoBean implements Serializable{
	private static final long serialVersionUID = 1L;
	private ArrayList<MemberBean> memaray;

	public MemberInfoBean() {
		memaray= new ArrayList<MemberBean>();
	}

	public void addMemberRecord(MemberBean obj) {
		memaray.add(obj);
	}

	public int getArraySize() {
		return memaray.size();
	}

	public ArrayList<MemberBean> getMemberArray() {
		return memaray;
	}

	public void setMemberRecordArray(ArrayList<MemberBean> memaray) {
		this.memaray = memaray;
	}

}
