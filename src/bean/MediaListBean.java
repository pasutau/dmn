package bean;

import java.io.Serializable;
import java.util.ArrayList;

public class MediaListBean implements Serializable {
	private static final long serialVersionUID = 1L;
	private ArrayList<MediaTenpoRecordBean> Mediaary;

	public MediaListBean() {
		Mediaary = new ArrayList<MediaTenpoRecordBean>();
	}

	public void addMediaList(MediaTenpoRecordBean obj){
		Mediaary.add(obj);
	}

	public int getArraySize(){
		return Mediaary.size();
	}

	public ArrayList<MediaTenpoRecordBean> getMediaList(){
		return Mediaary;
	}

 	public void setMediaList(ArrayList<MediaTenpoRecordBean> Mediaary){
 		this.Mediaary = Mediaary;
 	}
}
