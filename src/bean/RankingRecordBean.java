package bean;

import java.io.Serializable;



public class RankingRecordBean implements Serializable {
	private static final long serialVersion = 1L;
	private int kensu;
	private String title;

	public int getKensu() {
		return kensu;
	}
	public void setKensu(int kensu) {
		this.kensu = kensu;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}




}
