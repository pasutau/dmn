package bean;

import java.io.Serializable;

public class RegisterBean implements Serializable {
	private static final long serialVersionUID = 1L;

	private int RegiId;//レジ番号
	private int ShId;// 店舗ID
	private int Man;// 一万円
	private int Gosen;//五千円
	private int Nisen;//二千円
	private int Sen;//千円
	private int Gohyaku;//五百円
	private int Hyaku;//百円
	private int Gozyu;//五十円
	private int Zyu;//十円
	private int Go;//五円
	private int Iti;//一円
	private int sougoukei;
	private int rtime;
	private int rcount;
	private int rsales;
	private int zenamo;

	public int getZenamo() {
		return zenamo;
	}
	public void setZenamo(int zenamo) {
		this.zenamo = zenamo;
	}
	public int getRsales() {
		return rsales;
	}
	public void setRsales(int rsales) {
		this.rsales = rsales;
	}
	public int getRtime() {
		return rtime;
	}
	public void setRtime(int rtime) {
		this.rtime = rtime;
	}
	public int getRcount() {
		return rcount;
	}
	public void setRcount(int rcount) {
		this.rcount = rcount;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public RegisterBean() {
	}
	public int getSougoukei() {
		return sougoukei;
	}
	public void setSougoukei(int sougoukei) {
		this.sougoukei = sougoukei;
	}
	public int getRegiId() {
		return RegiId;
	}
	public void setRegiId(int RegiId){
		this.RegiId= RegiId;
	}
	public int getShId(){
		return ShId;
	}
	public void setShId(int ShId){
		this.ShId= ShId;
	}
	public int getMan(){
		return Man;
	}
	public void setMan(int Man){
		this.Man= Man;
	}
	public int getGosen(){
		return Gosen;
	}
	public void setGosen(int Gosen){
		this.Gosen= Gosen;
	}
	public int getNisen(){
		return Nisen;
	}
	public void setNisen(int Nisen){
		this.Nisen= Nisen;
	}

	public int getSen(){
		return Sen;
	}
	public void setSen(int Sen){
		this.Sen= Sen;
	}
	public int getGohyaku(){
		return Gohyaku;
	}
	public void setGohyaku(int Gohyaku){
		this.Gohyaku= Gohyaku;
	}
	public int getHyaku(){
		return Hyaku;
	}
	public void setHyaku(int Hyaku){
		this.Hyaku= Hyaku;
	}
	public int getGozyu(){
		return Gozyu;
	}
	public void setGozyu(int Gozyu){
		this.Gozyu= Gozyu;
	}
	public int getZyu(){
		return Zyu;
	}
	public void setZyu(int Zyu){
		this.Zyu= Zyu;
	}
	public int getGo(){
		return Go;
	}
	public void setGo(int Go){
		this.Go= Go;
	}
	public int getIti(){
		return Iti;
	}
	public void setIti(int Iti){
		this.Iti= Iti;
	}

}


