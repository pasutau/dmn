package bean;

import java.io.Serializable;
import java.util.Date;

public class KasiRirekiBean implements Serializable {
	private static final long serialVersion = 1L;
	private int uId;
	private String uName;
	private Date time;

	public KasiRirekiBean(){
		super();
	}

	public int getuId() {
		return uId;
	}
	public void setuId(int uId) {
		this.uId = uId;
	}

	public String getuName() {
		return uName;
	}
	public void setuName(String uName) {
		this.uName = uName;
	}

	public Date getTime() {
		return time;
	}
	public void setTime(Date time) {
		this.time = time;
	}

}
