package bean;

import java.io.Serializable;

public class MediaTenpoRecordBean implements Serializable {
	private static final long serialVersionUID = 1L;

	private int shId;
	private String shName;
	private int su;
	private String mgory;
	private String pid;
	private String status;
	private int zitenpoID;

	public String getShName() {
		return shName;
	}

	public void setShName(String shName) {
		this.shName = shName;
	}

	public int getShId() {
		return shId;
	}

	public void setShId(int shId) {
		this.shId = shId;
	}

	public int getSu() {
		return su;
	}

	public void setSu(int su) {
		this.su = su;
	}

	public String getMgory() {
		return mgory;
	}

	public void setMgory(String mgory) {
		this.mgory = mgory;
	}

	public String getPid() {
		return pid;
	}

	public void setPid(String pid) {
		this.pid = pid;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public int getZitenpoID() {
		return zitenpoID;
	}

	public void setZitenpoID(int zitenpoID) {
		this.zitenpoID = zitenpoID;
	}
}