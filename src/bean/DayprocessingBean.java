package bean;

import java.io.Serializable;

public class DayprocessingBean implements Serializable {
	private static final long serialVersionUID = 1L;

	private int ShId;//店舗ID
	private String DpTime;//日付
	private int DpSales;//売上
	private int DpAmount;//レジ内金額
	private int DpLateFee;//延滞
	private int DpRefundAmount;//返金
	private int DpAdmission;//入会、更新金額
	private int ProfitAndLoss;//雑役、雑損

	public DayprocessingBean() {
	}
	public int getShId(){
		return ShId;
	}
	public void setShId(int ShId){
		this.ShId= ShId;
	}

	public String getDpTime(){
		return DpTime;
	}
	public void setDpTime(String DpTime){
		this.DpTime= DpTime;
	}
	public int getDpSales(){
		return DpSales;
	}
	public void setDpSales(int DpSales){
		this.DpSales=DpSales;
	}
	public int getDpAmount(){
		return DpAmount;
	}
	public void setDpAmount(int DpAmount){
		this.DpAmount=DpAmount;
	}
	public int getDpLateFee(){
		return DpLateFee;
	}
	public void setDpLateFee(int DpLateFee){
		this.DpLateFee=DpLateFee;
	}
	public int getDpRefundAmount(){
		return DpRefundAmount;
	}
	public void setDpRefundAmount(int DpRefundAmount){
		this.DpRefundAmount=DpRefundAmount;
	}
	public int getDpAdmission(){
		return DpAdmission;
	}
	public void setDpAdmission(int DpAdmission){
		this.DpAdmission=DpAdmission;
	}
	public int getProfitAndLoss(){
		return ProfitAndLoss;
	}
	public void setProfitAndLoss(int ProfitAndLoss){
		this.ProfitAndLoss=ProfitAndLoss;
	}



}
