package bean;

import java.io.Serializable;

public class StoreLendingBean implements Serializable {
	private static final long serialVersionUID = 1L;

	private String slId;
	private String slshId;
	private int slTime;
	private String slPlanTime;
	private String slEndTime;
	private String slStatus;


	// Constructor
	public StoreLendingBean() {
	}

	public String getSlId() {
		return slId;
	}

	public void setSlId(String slId) {
		this.slId = slId;
	}

	public String getSlshId() {
		return slshId;
	}

	public void setSlshId(String slshId) {
		this.slshId = slshId;
	}

	public int getSlTime() {
		return slTime;
	}

	public void setSlTime(int slTime) {
		this.slTime = slTime;
	}

	public String getSlPlanTime() {
		return slPlanTime;
	}

	public void setSlPlanTime(String slPlanTime) {
		this.slPlanTime = slPlanTime;
	}

	public String getSlEndTime() {
		return slEndTime;
	}

	public void setSlEndTime(String slEndTime) {
		this.slEndTime = slEndTime;
	}

	public String getSlStatus() {
		return slStatus;
	}

	public void setSlStatus(String slStatus) {
		this.slStatus = slStatus;
	}

}