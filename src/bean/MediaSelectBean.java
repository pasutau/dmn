package bean;

import java.io.Serializable;
import java.util.Date;

public class MediaSelectBean implements Serializable {
	private static final long serialVersionUID = 1L;

		private int uId;
		private String uName;
		private Date loanDate;
		private String breakerFlg;

	public MediaSelectBean() {
		// TODO 自動生成されたコンストラクター・スタブ
	}




	public int getuId() {
		return uId;
	}




	public void setuId(int uId) {
		this.uId = uId;
	}




	public String getuName() {
		return uName;
	}




	public void setuName(String uName) {
		this.uName = uName;
	}




	public Date getLoanDate() {
		return loanDate;
	}




	public void setLoanDate(Date loanDate) {
		this.loanDate = loanDate;
	}




	public String getBreakerFlg() {
		return breakerFlg;
	}




	public void setBreakerFlg(String breakerFlg) {
		this.breakerFlg = breakerFlg;
	}

}
