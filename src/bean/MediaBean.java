package bean;

import java.io.Serializable;
import java.sql.Date;

public class MediaBean implements Serializable {
	private static final long serialVersionUID = 1L;


//////１件のメディアのいろんな情報をもってるよん//////////////////


	private String mId;//メディア番号
	private String mCategory;//DVD/BR区分
	private int shId;//店舗ＩＤ
	private String mFlag;//状態フラグ
	private Date haikiday;//廃棄日
	private String medianame;//メディア名


	public String getMedianame() {
		return medianame;
	}

	public void setMedianame(String medianame) {
		this.medianame = medianame;
	}

	public Date getHaikiday() {
		return haikiday;
	}

	public void setHaikiday(Date haikiday) {
		this.haikiday = haikiday;
	}

	// Constructor
	public MediaBean() {
	}

	public String getmId() {
		return mId;
	}

	public void setmId(String mId) {
		this.mId = mId;
	}

	public String getmCategory() {
		return mCategory;
	}

	public void setmCategory(String mCategory) {
		this.mCategory = mCategory;
	}

	public int getShId() {
		return shId;
	}

	public void setShId(int shId) {
		this.shId = shId;
	}

	public String getmFlag() {
		return mFlag;
	}

	public void setmFlag(String mFlag) {
		this.mFlag = mFlag;
	}
}
