package bean;

import java.io.Serializable;
import java.util.ArrayList;

public class SyainInfoBean implements Serializable{
	private static final long serialVersionUID = 1L;
	private ArrayList<SyainBean> semaray;

	public SyainInfoBean() {
		semaray= new ArrayList<SyainBean>();
	}

	public void addSyainRecord(SyainBean obj) {
		semaray.add(obj);
	}

	public int getArraySize() {
		return semaray.size();
	}

	public ArrayList<SyainBean> getSyainArray() {
		return semaray;
	}

	public void setSyainRecordArray(ArrayList<SyainBean> semaray) {
		this.semaray = semaray;
	}

}
