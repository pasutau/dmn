package bean;

import java.io.Serializable;
import java.util.ArrayList;

public class RankingListBean implements Serializable {
	private static final long serialVersionUID = 1L;

	private ArrayList<RankingRecordBean> Ranking;

	public RankingListBean() {
		Ranking = new ArrayList<RankingRecordBean>();
	}

		public void addRanking(RankingRecordBean obj){
			Ranking.add(obj);
		}

		public int ArraySize(){
			return Ranking.size();
		}

		public ArrayList<RankingRecordBean> getRanking(){
			return Ranking;
		}

	 	public void setRankingList (ArrayList<RankingRecordBean> Ranking){
	 		this.Ranking = Ranking;
	 	}

}