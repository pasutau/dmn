package bean;

import java.io.Serializable;
import java.util.ArrayList;

public class RegisterInfo implements Serializable{
	private static final long serialVersionUID = 1L;
	private ArrayList<RegisterBean> rb;

	public RegisterInfo() {
		rb = new ArrayList<RegisterBean>();
	}

	public void addRegiRecord(RegisterBean obj) {
		rb.add(obj);
	}

	public int getArraySize() {
		return rb.size();
	}

	public ArrayList<RegisterBean> getRegiArray() {
		return rb;
	}

	public void setRegiRecordArray(ArrayList<RegisterBean> rb) {
		this.rb = rb;
	}
}
