package bean;

import java.util.Date;

public class LInfomationBean {

	private int lId;
	private String mId;
	private Date liPlanTime;
	private Date liTime;
	public LInfomationBean() {
		// TODO 自動生成されたコンストラクター・スタブ
	}

	public int getlId() {
		return lId;
	}
	public void setlId(int lId) {
		this.lId = lId;
	}
	public String getmId() {
		return mId;
	}
	public void setmId(String mId) {
		this.mId = mId;
	}
	public Date getLiPlanTime() {
		return liPlanTime;
	}
	public void setLiPlanTime(Date liPlanTime) {
		this.liPlanTime = liPlanTime;
	}
	public Date getLiTime() {
		return liTime;
	}
	public void setLiTime(Date liTime) {
		this.liTime = liTime;
	}

}
