package bean;

import java.io.Serializable;
import java.util.ArrayList;

public class FusokuListBean implements Serializable {
	private static final long serialVersionUID = 1L;
	private ArrayList<FusokuRecordBean> fusokuary;

	public FusokuListBean() {
		fusokuary = new ArrayList<FusokuRecordBean>();
	}

	public void addFusokuList(FusokuRecordBean obj){
		fusokuary.add(obj);
	}

	public int getArraySize(){
		return fusokuary.size();
	}

	public ArrayList<FusokuRecordBean> getFusokuList(){
		return fusokuary;
	}

 	public void setFusokuList(ArrayList<FusokuRecordBean> fusokuary){
 		this.fusokuary = fusokuary;
 	}

}
