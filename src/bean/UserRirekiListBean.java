package bean;

import java.io.Serializable;
import java.util.ArrayList;

public class UserRirekiListBean implements Serializable {
	private static final long serialVersionUID = 1L;
	private ArrayList<UserRirekiRecordBean> uskasiary;

	public UserRirekiListBean() {
		uskasiary = new ArrayList<UserRirekiRecordBean>();
	}

	public void addUserRireki(UserRirekiRecordBean obj){
		uskasiary.add(obj);
	}

	public int getArraySize(){
		return uskasiary.size();
	}

	public ArrayList<UserRirekiRecordBean> getUserRireki(){
		return uskasiary;
	}

 	public void setKasiRireki(ArrayList<UserRirekiRecordBean> uskasiary){
 		this.uskasiary = uskasiary;
 	}
}
