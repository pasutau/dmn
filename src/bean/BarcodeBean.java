package bean;

import java.awt.Point;

public class BarcodeBean {

	public native void setCrop(int paramInt1, int paramInt2, int paramInt3, int paramInt4);

	private int rectLeft ;
    private int rectTop ;
    private int rectWidth ;
    private int rectHeight ;
    private Point previewSize ;
    @Override
    public void onPreviewFrame( byte[] data, Camera camera ) {


        BarcodeBean barcode;
		// 読み取り範囲の設定.
        barcode.setCrop( rectLeft,rectTop,rectWidth,rectHeight ) ;

    }
}
